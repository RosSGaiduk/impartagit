trigger TriggerOnRevenueItems on Revenue_Item__c (before insert,before update,after update) {
    
    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            RevenueItemTriggerHandler.handleAfterUpdate(Trigger.new);
        }    
    } else {
        RevenueItemTriggerHandler.updateCurrencyValue(trigger.new);
    }
    
}