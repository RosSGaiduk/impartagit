trigger ProjectCloudWorkTrigger on project_cloud__Work__c (before insert) {
    
    ProjectCloudWorkTriggerHandler.handlerBeforeInsert(Trigger.New);
}