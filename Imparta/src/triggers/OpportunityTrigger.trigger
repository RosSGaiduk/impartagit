trigger OpportunityTrigger on Opportunity (Before Insert, Before Update, After Update, Before Delete) {
    if(trigger.isBefore && trigger.isUpdate){
        OpportunityExchangeRateHandler.calculateExchangeRate(trigger.new);
    }
    if(trigger.isAfter && trigger.isupdate){
        opportunityTriggerHandler.updatingRevenueRec(trigger.old,trigger.newmap);
    }

    if(trigger.isBefore && trigger.isdelete){
        opportunityTriggerHandler.deleteRevenueRecords(trigger.old);
    }
}