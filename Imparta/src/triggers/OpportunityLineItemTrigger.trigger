trigger OpportunityLineItemTrigger on OpportunityLineItem (after insert, after update, after delete) {

    if (Trigger.isAfter && Trigger.isUpdate && !opportunityTriggerHandler.avoidProductlineItemTrigger && OppProductTriggerHandler.oppLineItemTrigger) {
        OppProductTriggerHandler.updatingRevenueRecords(Trigger.oldMap, Trigger.newMap);
        OppProductTriggerHandler.updateProjectTaskRecords(Trigger.old, Trigger.newMap);
        OppProductTriggerHandler.validateRecordsByActualActivityAfterUpdate(Trigger.oldMap, Trigger.newMap);
    }
    
    if (Trigger.isAfter && Trigger.isInsert) {
        OppProductTriggerHandler.creatingRevenueRecords(Trigger.new);
        OppProductTriggerHandler.validateRecordsByActualActivityAfterInsert(Trigger.new);
    }
    if (Trigger.isAfter && Trigger.isDelete) {
        OppProductTriggerHandler.deletingRevenueRecords(Trigger.old);
    }

}