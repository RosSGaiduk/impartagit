trigger LedgerItemTrigger on s2cor__Sage_ACC_Ledger_Item__c (after insert, after update, before delete) {

	if(Trigger.isInsert && Trigger.isAfter){

		LedgerItemTriggerHandler.insertRevItems(Trigger.newMap);

	}

	if(Trigger.isUpdate && Trigger.isAfter){

		LedgerItemTriggerHandler.updateRevItems(Trigger.newMap, Trigger.oldMap);

	}

	if(Trigger.isDelete && Trigger.isBefore){

		LedgerItemTriggerHandler.delRevItems(Trigger.oldMap);

	}

}