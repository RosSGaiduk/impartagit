trigger AllocationTrigger on project_cloud__Allocation__c (before insert, before update, after insert, after update, before delete) {

    if (Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)) {
        AllocationTriggerHandler.handleBeforeInsertOrUpdate(Trigger.new);
    } else if (Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)) {
        AllocationTriggerHandler.handleAfterInsertOrUpdate(Trigger.new);
    } else if (Trigger.isBefore && Trigger.isDelete){
        AllocationTriggerHandler.handleBeforeDelete(Trigger.Old);
    }

}