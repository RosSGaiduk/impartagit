trigger ExpenseReportTrigger on Expense_report__c (before update) {
    for (Expense_report__c oldEr : Trigger.old) {
        Expense_report__c newEr = Trigger.newMap.get(oldEr.Id);
        if (oldEr.Claim_status__c  != newEr.Claim_status__c) {
            System.debug('oldEr.Claim_status__c  != newEr.Claim_status__c: ' + oldEr.Claim_status__c  + ' - ' + newEr.Claim_status__c);
        }
    }
}