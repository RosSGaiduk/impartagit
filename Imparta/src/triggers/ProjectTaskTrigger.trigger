trigger ProjectTaskTrigger on  project_cloud__Project_Task__c (after insert, after update, before delete, after delete) {

	if (Trigger.isAfter) {
		if (Trigger.isInsert) {
			ProjectTaskTriggerHandler.handleAfterInsert(Trigger.new);
		} else if (Trigger.isUpdate) {
            System.debug('Is update');
			ProjectTaskTriggerHandler.handleAfterUpdate(Trigger.old);
            if (!ProjectTaskTriggerHandler.isProjectSync) {
            	ProjectTaskTriggerHandler.updateRevenueItemsIfSyncOptionIsChanged(Trigger.oldMap, Trigger.newMap);
            }
            System.debug('Is validate');
			//ProjectTaskTriggerHandler.validateTasksAfterInsertOrUpdate(Trigger.new);
        } else if (Trigger.isDelete) {
            //ProjectTaskTriggerHandler.handleAfterDelete(Trigger.old);
        }
	} else {
		if (Trigger.isDelete) {
			ProjectTaskTriggerHandler.handleBeforeDelete(Trigger.old);
		}
	}

}