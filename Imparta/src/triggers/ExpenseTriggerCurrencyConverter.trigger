trigger ExpenseTriggerCurrencyConverter on project_cloud__Expense2__c (before insert) {
    
    if (Trigger.isBefore) {
        if (Trigger.isInsert) {
            ExpensesCurrencyRateConverter.beforeInsert(Trigger.new);
        }
    }
}