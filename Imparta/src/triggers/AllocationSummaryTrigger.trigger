trigger AllocationSummaryTrigger on project_cloud__Allocation_Summary__c (after update, before delete) {

    if (Trigger.isAfter && Trigger.isUpdate) {
        AllocationSummaryTriggerHandler.handleAfterInsertOrUpdate(Trigger.new);
    } else if (Trigger.isBefore && Trigger.isDelete) {
        AllocationSummaryTriggerHandler.handleBeforeDelete(Trigger.old);
    }

}