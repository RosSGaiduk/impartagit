trigger trgDeleteProjectPhase on project_cloud__Project_Phase__c (before delete) {
    // This trigger deletes all of the Sub Tasks in a Phase before the Phase itself is deleted
    //Collect all of the Phase Ids in the Trigger
   List<Id> phaseIds = new List<Id>();

    for(project_cloud__Project_Phase__c ph:Trigger.old){
        phaseIds.add(ph.Id);
    }
    
    //Query all of the Sub Tasks in the Phase
    List<project_cloud__Project_Task__c> taskIds = [SELECT Id FROM project_cloud__Project_Task__c WHERE project_cloud__Project_Phase__c in:phaseIds];
   
    // Quaery all of the Sub Tasks in the Phase
    List<project_cloud__Project_Sub_Task__c> subTaskIds = [SELECT Id FROM project_cloud__Project_Sub_Task__c WHERE project_cloud__Project_Task__c in:taskIds];
      
    // Delete all the Sub Tasks
    if(subTaskIds != null && subTaskIds.size() > 0) delete subTaskIds;
}