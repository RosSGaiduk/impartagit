trigger AllocationGroupTrigger on project_cloud__Allocation_Group__c (after insert, after update) {

    if (Trigger.isAfter && Trigger.isUpdate) {
        AllocationGroupTriggerHandler.handleAfterUpdate(Trigger.new);
    }

}