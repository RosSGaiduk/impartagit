({
	fetchWorkTypes : function(component) {
		var action = component.get('c.getWorkTypesByProjectId');
        action.setParams({
            projectId : component.get('v.recordId')
        });
        
        action.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
            	console.log(response.getReturnValue());
            	component.set('v.workTypes', response.getReturnValue());
            }
        });
        
        $A.enqueueAction(action);
	}
})