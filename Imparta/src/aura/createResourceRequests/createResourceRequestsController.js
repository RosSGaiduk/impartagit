({
	initWorkTypes : function(component, event, helper) {
        component.set('v.columns', [
            { label: 'Name', fieldName: 'Name', type: 'text' },
            { label: 'Type', fieldName: 'project_cloud__Type__c', type: 'text' }
        ]);
        helper.fetchWorkTypes(component);
        component.set('v.workTypesInitialized', true);
	},
    
    onRender : function(component, event, helper) {	},
    
    updateSelectedRows : function(component, event, helper) {
        component.set('v.selectedRows', event.getParam('selectedRows'));
	},
    
    createResourceRequests : function(component, event, helper) {
    	var selectedRows = event.getParam('getSelectedRows');
    	alert(component.get('v.selectedRows'));
	}

})