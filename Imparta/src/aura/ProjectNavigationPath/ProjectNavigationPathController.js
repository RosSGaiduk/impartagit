({
    doInit: function (component) {
        var getProjectId = component.get('c.getProjectId');
        getProjectId.setParams({ recordId: component.get('v.recordId') });
        getProjectId.setCallback(this, function (r) {
            var state = r.getState();
            if (component.isValid() && state === 'SUCCESS') {
                component.set('v.relatedId', r.getReturnValue());
            } else {
                console.log('ERROR', r.getReturnValue());
            }
        });
        $A.enqueueAction(getProjectId);
    },
    handleNavigation: function (component, event) {
        var projectId = component.get('v.relatedId');
        var tabName = event.target.innerText;
        var urlEvt = $A.get('e.force:navigateToURL');
        if (projectId != null) {
            switch (tabName) {
                case 'Summary':
                    urlEvt.setParams({ 'url': '/apex/project_cloud__projects_app?id=' + projectId + '#/view/' + projectId + '/summary' });
                    urlEvt.fire();
                    break;
                case 'Timelines':
                    urlEvt.setParams({ 'url': '/apex/project_cloud__projects_app?id=' + projectId + '#/view/' + projectId + '/timelines' });
                    urlEvt.fire();
                    break;
                case 'Documents':
                    urlEvt.setParams({ 'url': '/apex/project_cloud__projects_app?id=' + projectId + '#/view/' + projectId + '/documents' });
                    urlEvt.fire();
                    break;
                case 'Controls':
                    urlEvt.setParams({ 'url': '/apex/project_cloud__projects_app?id=' + projectId + '#/view/' + projectId + '/controls' });
                    urlEvt.fire();
                    break;
                case 'Resources':
                    urlEvt.setParams({ 'url': '/apex/project_cloud__projects_app?id=' + projectId + '#/view/' + projectId + '/resources' });
                    urlEvt.fire();
                    break;
                case 'Financials':
                    urlEvt.setParams({ 'url': '/apex/project_cloud__projects_app?id=' + projectId + '#/view/' + projectId + '/financials' });
                    urlEvt.fire();
                    break;
            }
        } else {
            console.log(`There is no parent Id. Parent Id: ${projectId}`);
        }
    }
})