({
	clicked : function(component, event, helper) {        
        var allElements = document.querySelectorAll('*[id^="taskIdElement_"]');
        allElements.forEach(function(el) {
            try { el.classList.add("slds-hide"); } catch (e) {}          
        });
        
        helper.openCurrent(component);
	},
    
    assignValue : function(component, event, helper) {
        var clickedTarget = event.currentTarget.id;
        var task = component.get('v.task');
        
        task.listOptions.forEach(function(opt) {
            if (opt.id == clickedTarget) {
                task.projectTask.Actual_Activity__c = opt.value;
                return;
            }
        });
        component.set('v.task', task);
        helper.closeCurrent(component);
    },
    
    changed : function(component, event, helper) {
        helper.filterData(component);
    }
    
})