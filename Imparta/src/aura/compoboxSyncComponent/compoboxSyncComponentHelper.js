({
	openCurrent : function(component) {
        var element = document.getElementById('taskIdElement_' + component.get('v.task').projectTask.Id);
        try {
        	element.classList.remove("slds-hide");
        } catch (e) {}
        this.filterData(component);
	},
    
    filterData : function(component) {
        var task = component.get('v.task');
        console.log(task.projectTask.Actual_Activity__c);
        var countHide = 0;
        try {
            task.listOptions.forEach(function(opt) {
                var element = document.getElementById(opt.id);
                var actualActivity = (task.projectTask.Actual_Activity__c != null && task.projectTask.Actual_Activity__c != undefined) 
                    ? task.projectTask.Actual_Activity__c : '';
                    
                if (opt.value.indexOf(actualActivity) == -1) {
                    countHide++;
                    try { element.classList.add("slds-hide"); } catch (e) { console.log('exception: ' + e); }       
                } else {
                    try { element.classList.remove("slds-hide"); } catch (e) { console.log('exception: ' + e); }    
                }
            });
        } catch (e) {}
        
        console.log(task.listOptions.length);
        if (countHide == task.listOptions.length || task.listOptions.length == 0) {
            document.getElementById('taskIdElement_' + task.projectTask.Id).style.display = "none";
        } else {
            document.getElementById('taskIdElement_' + task.projectTask.Id).style.display = "";
        }
    },
    
    closeCurrent : function(component) {
        var element = document.getElementById('taskIdElement_' + component.get('v.task').projectTask.Id);
        try {
        	element.classList.add("slds-hide");
        } catch (e) {}
    }
})