({
    checkSyncStatus : function(component) {
        const action = component.get('c.getSyncStatus');
        action.setParams({ projectId : component.get('v.recordId') });
        action.setCallback(this, function(response) {
            component.set('v.isSynchedDown', response.getReturnValue());
            if (component.get('v.isSynchedDown')) {
                component.set('v.value','replace-opp-from-tasks');
                this.showSpinner(component);
                this.buildActivityWindow(component);
            } else {
                component.set('v.value','copy-pricing-from-opps');
                //this.showSpinner(component);
                //this.sync(component);
            }
        });
        $A.enqueueAction(action);
    },
    
    closeComponent: function () {
        const dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    },

    hideSpinner: function (component) {
        const spinner = component.find('spinner');
        $A.util.addClass(spinner, 'slds-hide');
    },

    showSpinner: function (component) {
        const spinner = component.find('spinner');
        $A.util.removeClass(spinner, 'slds-hide');
    },

    showSuccessToast : function() {
        const toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Success Message',
            message: 'The Project has been synced.',
            duration:' 5000',
            key: 'info_alt',
            type: 'success',
            mode: 'sticky'
        });
        toastEvent.fire();
    },
    
    initSync : function(component) {
        const action = component.get('c.initialize');
        action.setParams({ projectId : component.get('v.recordId') });
        action.setCallback(this, function(response) {
            this.hideSpinner(component);

            let message;

            if (response.getState() === 'SUCCESS') {
                message = response.getReturnValue();
                console.log('message: '+message);
                if (message) {
                    const proceedButton = component.find('proceedButton');
                    $A.util.removeClass(proceedButton, 'slds-hide');
                } else {
                    this.closeComponent();
                    this.showSuccessToast();
                }
            } else {
                var errors = response.getError();
                if (errors) {
                    message = errors[0].message;
                }
            }

            if (message) {
                component.set('v.message', message);
                const radioBlock = component.find('radioBlock');
                $A.util.addClass(radioBlock, 'slds-hide');
                const syncByChoice = component.find('syncByChoice');
                $A.util.removeClass(syncByChoice, 'slds-hide');
            }
        });
        $A.enqueueAction(action);
    },
    
    sync: function(component) {
        this.showSpinner(component);

        const action = component.get('c.handleSync');
        action.setParams({
            projectId : component.get('v.recordId'),
            option : component.get("v.value")
         });

        action.setCallback(this, function(response) {
            this.hideSpinner(component);

            let message;
            
            if (response.getState() === 'SUCCESS') {
                this.closeComponent();
                this.showSuccessToast();
            } else {
                var errors = response.getError();
                if (errors) {
                    message = errors[0].message;
                }
            }
            if (message) {
                component.set('v.message', message);
                const radioBlock = component.find('radioBlock');
                $A.util.addClass(radioBlock, 'slds-hide');
                const syncByChoice = component.find('syncByChoice');
                $A.util.removeClass(syncByChoice, 'slds-hide');
            }
        });
        $A.enqueueAction(action);
    },
    
    syncWithTasksProcess : function(component) {
        this.showSpinner(component);
        component.set('v.message', 'Waiting response from server');
        
        const action = component.get('c.syncWithTasksProcessAction');
        action.setParams({
            projectId : component.get('v.recordId'),
            projectTasksWrappersStr: JSON.stringify(component.get("v.tasks"))
         });

        action.setCallback(this, function(response) {
            this.hideSpinner(component);
            
            if (response.getState() === 'SUCCESS') {
                component.set('v.message', 'Tasks were successfully modified and synced');
            } else {
                var errors = response.getError();
                var message;
                if (errors) {
                    message = errors[0].message;
                }
                if (message) {
                    component.set('v.message', message);
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    
    buildActivityWindow : function(component) {
        const action = component.get('c.buildActivityWindowAction');
        action.setParams({
            projectId : component.get('v.recordId')
        });
        
        action.setCallback(this, function(response) {
            this.hideSpinner(component);
            
            if (response.getState() === 'SUCCESS') {
                component.set('v.tasks', response.getReturnValue());
                component.set('v.message', '');
                const radioBlock = component.find('radioBlock');
                $A.util.addClass(radioBlock, 'slds-hide');
                const notSynchedTasksWindow = component.find('notSynchedTasksWindow');
                $A.util.removeClass(notSynchedTasksWindow, 'slds-hide');
            }
        });
        $A.enqueueAction(action);
    }
    
})