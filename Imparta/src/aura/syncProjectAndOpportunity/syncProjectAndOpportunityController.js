({
    checkSyncStatus : function(component, event, helper) {
        helper.checkSyncStatus(component);
    },
    
    sync: function(component, event, helper) {
        helper.sync(component);
    },

    cancelBtn : function(component, event, helper) {
        helper.closeComponent();
    },
    
    nextClicked : function(component, event, helper) {
        var selectedOption = component.get("v.value");
        if (selectedOption === 'replace-opp-from-tasks') {
            console.log('second option');
            helper.showSpinner(component);
            helper.buildActivityWindow(component);
            //helper.initSync(component);
        } else {
            console.log('first option');
            helper.showSpinner(component);
            helper.sync(component);
        }
    },
    
    syncWithTasks : function(component, event, helper) {
        //console.log('tasks: ' + JSON.stringify(component.get('v.tasks')));
        helper.syncWithTasksProcess(component);
    },
    
    clicked : function(component, event, helper) {        
        var allElements = document.querySelectorAll('*[id^="taskIdElement_"]');
        allElements.forEach(function(el) {
            try { el.classList.add("slds-hide"); } catch (e) {}          
        });
	}
})