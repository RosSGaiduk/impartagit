public class CongaOrderFormController{
    
    private User currentUserDetails = [SELECT Id, FirstName, LastName, Email, Phone, Title FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
    private Organization orgData = [SELECT Id, Name, City, Country, Fax, Division, IsSandbox, OrganizationType, Phone, PostalCode, PrimaryContact, State, Street FROM Organization WHERE Id =: UserInfo.getOrganizationId() LIMIT 1];
    public Boolean isSubmitted {get; set;}
    public String orgNameInput  {get; set;}
    public String primaryContactInput  {get; set;}
    public String billingContactInput  {get; set;}
    public String orgStreetInput  {get; set;}
    public String orgCityInput  {get; set;}
    public String orgStateInput  {get; set;}
    public String orgZipInput  {get; set;}
    public String orgCountryInput  {get; set;}
    public String orgPhoneInput  {get; set;}
    public String userEmailInput  {get; set;}
    public String userTitleInput  {get; set;}  
    public String prodOrgIdInput  {get; set;}  
    public Integer licensesInput  {get; set;}  
    
    //default constructor
    public CongaOrderFormController(){    
        setInputVariables();
        system.debug('*********************** orgNameInput: ' + orgNameInput);
    }
    
    private void setInputVariables()
    {
        orgNameInput = orgData.Name;
        primaryContactInput = orgData.PrimaryContact;  
        orgStreetInput = orgData.Street;
        orgCityInput = orgData.City;
        orgStateInput = orgData.State;
        orgZipInput = orgData.PostalCode;
        orgCountryInput = orgData.Country;
        orgPhoneInput = orgData.Phone;
        //userEmailInput = currentUserDetails.Email;
        userTitleInput = currentUserDetails.Title;
    }
        
    //Returning the Prod Org Id, if this is the Prod Org
    public String getProdOrgId(){
        String prodOrgId = '';
        
        if(orgData.IsSandbox == FALSE)
        {
            prodOrgId = orgData.Id;  
        }
        return prodOrgId;
    }
    
    
    //Get the IsSandbox Boolean value
    public Boolean getIsSandboxValue(){
        return orgData.IsSandbox;
    }
        
    public void submitProcedure(){
        String prodOrgIdForEmail = '';
        if(orgData.IsSandbox == FALSE)
        {
            prodOrgIdForEmail = orgData.Id;            
        }
        else
        {
            prodOrgIdForEmail = prodOrgIdInput;        
        }
        
        system.debug('*********************** prodOrgIdForEmail: ' + prodOrgIdForEmail);
        system.debug('*********************** licensesInput: ' + licensesInput);
        
        if(prodOrgIdForEmail == '' || licensesInput == 0 || orgStreetInput == '' || orgCityInput == '' || orgStateInput == '' || orgZipInput == '' || orgCountryInput == '' || orgPhoneInput == '' || userEmailInput == '' || billingContactInput == '' || prodOrgIdForEmail == '')
        {
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Please fill in all required fields.')); 
        }
        else
        {
            Messaging.SingleEmailMessage generatePostInstallEmail = new Messaging.SingleEmailMessage();
                String[] toAddresses = new String[] {'SageOrders@getconga.com'};          
                generatePostInstallEmail.setSubject('Conga for SageLive Order'); 
                generatePostInstallEmail.setToAddresses(toAddresses);
                generatePostInstallEmail.setPlainTextBody('COMPANY INFORMATION' + '\n' +
                                                          '\t' + 'Company Name: ' + orgNameInput + '\n' + '\t' + 'Billing Contact Name: ' + billingContactInput + '\n' + '\t' + 'Billing Phone: ' + orgPhoneInput + '\n' + '\t' + 'Billing Email: ' + userEmailInput  + '\n\n' +
                                                          
                                                          'LICENSE & ORG INFORMATION' + '\n' +
                                                          '\t' + 'Production Org Id: ' + prodOrgIdForEmail + '\n' + 
                                                          '\t' + 'Submitting Org Id: ' + UserInfo.GetOrganizationId() + '\n' +
                                                          '\t\t' + 'Submitting Org is a Sandbox?: ' + orgData.IsSandbox + '\n' + 
                                                          '\t' + 'Licenses Requested: ' + licensesInput + '\n' + 
                                                          '\t' + 'Start Date: ' + System.Today()+ '\n' +
                                                          '\t' + 'Term: One year from Start Date' + '\n' +
                                                          '\t' + 'Conga Products: Composer, Conductor, Workflow' + '\n\n' +                                                                                               
                                                                                                                    
                                                          'BILLING ADDRESS' + '\n' +
                                                          '\t' +'Street: ' + orgStreetInput + '\n' + 
                                                          '\t' +'City: ' + orgCityInput + '\n' + 
                                                          '\t' +'State: ' + orgStateInput + '\n' +
                                                          '\t' +'Postal Code: ' + orgZipInput + '\n' +
                                                          '\t' +'Country: ' + orgCountryInput + '\n\n' +                                                            
                                                          
                                                          'SUBMITTER INFORMATION' + '\n' +                                                          
                                                          '\t' + 'Submitter Name: ' + primaryContactInput  + '\n' + '\t' + 'Submitter Title: ' + userTitleInput+ '\n' + '\t' + 'Submitter Email: ' + currentUserDetails.Email);
                                                          
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { generatePostInstallEmail });       
                        
            isSubmitted = TRUE;            
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Your information had been submitted. You will be contacted via E-mail soon. Thank you. -Conga Team'));
        }
    }

}