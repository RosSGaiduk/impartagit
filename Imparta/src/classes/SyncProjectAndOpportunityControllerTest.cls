@IsTest
public class SyncProjectAndOpportunityControllerTest {

	@IsTest
	static void testWrongId() {
		Account ac = new Account(Name = 'test');
		insert ac;

		Boolean isExceptionThrown = false;
		try {
			SyncProjectAndOpportunityController.initialize(ac.Id);
		} catch (Exception e) {
			isExceptionThrown = true;
		}

		System.assertEquals(true, isExceptionThrown);
	}

	@IsTest
	static void testProjectWithoutOpportunity() {
		project_cloud__Project__c testProject = new project_cloud__Project__c(Name = 'testProject');
		insert testProject;

		Boolean isExceptionThrown = false;
		try {
			SyncProjectAndOpportunityController.initialize(testProject.Id);
		} catch (Exception e) {
			isExceptionThrown = true;
		}

		System.assertEquals(true, isExceptionThrown);
	}

	@IsTest
	static void testOpportunityWithoutPricebook() {
		Account a = new Account(Name = 'test');
		insert a;

		Opportunity o = new Opportunity(
				AccountId = a.Id, Name = 'test', CloseDate = Date.today().addDays(1), StageName = 'Closed Won'
		);
		insert o;

		project_cloud__Project__c testProject = new project_cloud__Project__c(
				Name = 'testProject', ccpe_ocp__Opportunity__c = o.Id
		);
		insert testProject;

		Boolean isExceptionThrown = false;
		try {
			SyncProjectAndOpportunityController.initialize(testProject.Id);
		} catch (Exception e) {
			isExceptionThrown = true;
		}

		System.assertEquals(true, isExceptionThrown);
	}

	@IsTest
	static void testNotSyncedOpportunityProducts() {
		Product2 prod1 = new Product2(Name = 'prod1');
		insert prod1;

		PricebookEntry pbe = new PricebookEntry(
				Product2Id = prod1.Id, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 20, IsActive = true
		);
		insert pbe;

		Account a = new Account(Name = 'test');
		insert a;

		Opportunity o = new Opportunity(
				AccountId = a.Id, Name = 'test', CloseDate = Date.today().addDays(1), StageName = 'Closed Won',
				Pricebook2Id = Test.getStandardPricebookId()
		);
		insert o;

		OpportunityLineItem oli = new OpportunityLineItem(
				OpportunityId = o.Id, PricebookEntryId = pbe.Id, Quantity = 2, TotalPrice = 40
		);
		insert oli;

		project_cloud__Project__c testProject = new project_cloud__Project__c(
				Name = 'testProject', ccpe_ocp__Opportunity__c = o.Id
		);
		insert testProject;

		String result = SyncProjectAndOpportunityController.initialize(testProject.Id);
		System.assertNotEquals(null, result);
	}

	@IsTest
	static void testNotSyncedOpportunityProductsAndSyncedQuote() {
		Product2 prod1 = new Product2(Name = 'prod1');
		insert prod1;

		PricebookEntry pbe = new PricebookEntry(
				Product2Id = prod1.Id, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 20, IsActive = true
		);
		insert pbe;

		Account a = new Account(Name = 'test');
		insert a;

		Opportunity o = new Opportunity(
				AccountId = a.Id, Name = 'test', CloseDate = Date.today().addDays(1), StageName = 'Closed Won',
				Pricebook2Id = Test.getStandardPricebookId()
		);
		insert o;

		Quote q = new Quote(OpportunityId = o.Id, Name = 'test', Pricebook2Id = Test.getStandardPricebookId());
		insert q;

		o.SyncedQuoteId = q.Id;
		update o;

		OpportunityLineItem oli = new OpportunityLineItem(
				OpportunityId = o.Id, PricebookEntryId = pbe.Id, Quantity = 2, TotalPrice = 40
		);
		insert oli;

		project_cloud__Project__c testProject = new project_cloud__Project__c(
				Name = 'testProject', ccpe_ocp__Opportunity__c = o.Id
		);
		insert testProject;

		Boolean isExceptionThrown = false;
		try {
			SyncProjectAndOpportunityController.initialize(testProject.Id);
		} catch (Exception e) {
			isExceptionThrown = true;
		}

		System.assertEquals(true, isExceptionThrown);
	}

	@IsTest
	static void testWithoutOpportunityProducts() {
		Account a = new Account(Name = 'test');
		insert a;

		Opportunity o = new Opportunity(
				AccountId = a.Id, Name = 'test', CloseDate = Date.today().addDays(1), StageName = 'Closed Won',
				Pricebook2Id = Test.getStandardPricebookId()
		);
		insert o;

		project_cloud__Project__c testProject = new project_cloud__Project__c(
				Name = 'testProject', ccpe_ocp__Opportunity__c = o.Id
		);
		insert testProject;

		String result = SyncProjectAndOpportunityController.initialize(testProject.Id);
		System.assertEquals(null, result);
	}

	@IsTest
	static void testSync() {
		Product2 prod1 = new Product2(Name = 'prod1');
		Product2 prod2 = new Product2(Name = 'prod2');
		Product2 prod3 = new Product2(Name = 'prod3');
		insert new List<Product2>{prod1, prod2, prod3};

		PricebookEntry pbe1 = new PricebookEntry(
				Product2Id = prod1.Id, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 20, IsActive = true
		);
		PricebookEntry pbe2 = new PricebookEntry(
				Product2Id = prod2.Id, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 20, IsActive = true
		);
		PricebookEntry pbe3 = new PricebookEntry(
				Product2Id = prod3.Id, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 20, IsActive = true
		);
		insert new List<PricebookEntry>{pbe1, pbe2, pbe3};

		Account a = new Account(Name = 'test');
		insert a;

		Opportunity o = new Opportunity(
				AccountId = a.Id, Name = 'test', CloseDate = Date.today().addDays(1), StageName = 'Closed Won',
				Pricebook2Id = Test.getStandardPricebookId()
		);
		insert o;

		/*OpportunityLineItem oli = new OpportunityLineItem(
				OpportunityId = o.Id, PricebookEntryId = pbe1.Id, Quantity = 2, TotalPrice = 40, Actual_Activity__c = 'Actual'
		);
		insert oli*/

		project_cloud__Project__c testProject = new project_cloud__Project__c(
				Name = 'testProject', ccpe_ocp__Opportunity__c = o.Id
		);
		insert testProject;

		project_cloud__Project_Phase__c testPhase = new project_cloud__Project_Phase__c(
				Name = 'testPhase', project_cloud__Project__c = testProject.Id
		);
		insert testPhase;

		project_cloud__Project_Task__c testTask = new project_cloud__Project_Task__c(
				Name = 'testTask', project_cloud__Autonomous_Start__c = Date.today().toStartOfWeek(),
				project_cloud__Project_Phase__c = testPhase.Id, project_cloud__Duration__c = 2,
				Service__c = prod1.Id, //Delegate_Fee_Product__c = prod2.Id, Licence_Fee_Product__c = prod3.Id,
				project_cloud__Estimated_Hours__c = 16, //Number_of_Delegates__c = 2, Number_of_Licences__c = 2,
				Delegate_Fee__c = 20, Licence_Fee__c = 20, Billable__c = true, Actual_Activity__c = 'DF3'
		);
		insert testTask;

        Test.startTest();
		//SyncProjectAndOpportunityController.handleSync(testProject.Id, 'replace-opp-from-tasks');
        List<SyncProjectAndOpportunityController.ProjectTaskWrapper> taskWrappers = SyncProjectAndOpportunityController.buildActivityWindowAction(testProject.Id);
        SyncProjectAndOpportunityController.syncWithTasksProcessAction(testProject.Id, JSON.serialize(taskWrappers));
        taskWrappers = SyncProjectAndOpportunityController.buildActivityWindowAction(testProject.Id);
        SyncProjectAndOpportunityController.getSyncStatus(testProject.Id);
        System.debug(SyncProjectAndOpportunityController.firstOption);
        Test.stopTest();
	}
    
    @isTest
    static void testSyncRevenueItems() {
        Product2 product1 = new Product2(Name = 'product1', CBS_Phase__c = 'Develop', 
            ProductCode = '4000 Facilitation', Product_Type__c = 'Professional Services');
        Product2 product2 = new Product2(Name = 'product2', CBS_Phase__c = 'Connect', 
            ProductCode = '4001 Facilitation', Product_Type__c = 'Professional Services');
		Product2 product3 = new Product2(Name = 'product3', CBS_Phase__c = 'Develop', 
            ProductCode = '4001 Delegate Fees', Product_Type__c = 'Delegate Fees');
		Product2 product4 = new Product2(Name = 'product4', CBS_Phase__c = 'Connect', 
            ProductCode = '4001 Licence Fees', Product_Type__c = 'Licence Fees');
		insert new List<Product2>{product1, product2, product3, product4};
        
		PricebookEntry pbe1 = new PricebookEntry(
				Product2Id = product1.Id, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 20, IsActive = true
		);
		PricebookEntry pbe2 = new PricebookEntry(
				Product2Id = product2.Id, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 30, IsActive = true
		);
		PricebookEntry pbe3 = new PricebookEntry(
				Product2Id = product3.Id, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 40, IsActive = true
		);
        
        PricebookEntry pbe4 = new PricebookEntry(
				Product2Id = product4.Id, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 50, IsActive = true
		);
		insert new List<PricebookEntry>{pbe1, pbe2, pbe3, pbe4};

		Account a = new Account(Name = 'Account');
		insert a;

		Opportunity o = new Opportunity(
				AccountId = a.Id, Name = 'TestOpp', CloseDate = Date.today().addDays(1), StageName = 'Closed Won',
				Pricebook2Id = Test.getStandardPricebookId()
		);
		insert o;

        
		OpportunityLineItem oli1 = new OpportunityLineItem(
				OpportunityId = o.Id, PricebookEntryId = pbe1.Id, Quantity = 2, TotalPrice = 40, Actual_Activity__c = '1'
		);
        
        OpportunityLineItem oli2 = new OpportunityLineItem(
				OpportunityId = o.Id, PricebookEntryId = pbe2.Id, Quantity = 2, TotalPrice = 60, Actual_Activity__c = '2'
		);
        
        OpportunityLineItem oli3 = new OpportunityLineItem(
				OpportunityId = o.Id, PricebookEntryId = pbe3.Id, Quantity = 2, TotalPrice = 80, Actual_Activity__c = '3'
		);
        
        OpportunityLineItem oli4 = new OpportunityLineItem(
				OpportunityId = o.Id, PricebookEntryId = pbe4.Id, Quantity = 2, TotalPrice = 100, Actual_Activity__c = '4'
		);
        
		insert new List<OpportunityLineItem>{oli1, oli2, oli3, oli4};
        
        project_cloud__Project__c testProject = new project_cloud__Project__c(
				Name = 'testProject', ccpe_ocp__Opportunity__c = o.Id
		);
		insert testProject;
        
        System.debug(JSON.serialize(
            [SELECT 
                Id, Product__c, Product__r.ProductCode, Product__r.Name, Date__c, Product__r.CBS_Phase__c,
                Amount__c, Quantity__c
            FROM 
                Revenue_Item__c 
            WHERE 
                Opportunity__c =: o.Id
        ]));
        System.assertEquals(4, [SELECT Id FROM Revenue_Item__c where Opportunity__c =: o.Id].size());
        
        Test.startTest();
        SyncProjectAndOpportunityController.handleSync(testProject.Id, 'copy-pricing-from-opps');
        
        List<project_cloud__Project_Phase__c> createdPhases = [
            SELECT 
                Id, Name,
                (SELECT 
                     Id, Billable__c, Service__c, Service__r.ProductCode, Service_Price__c,
                     Number_of_Delegates__c, Delegate_Fee_Product__r.ProductCode, Delegate_Fee__c, 
                     Number_of_Licences__c, Licence_Fee_Product__r.ProductCode, Licence_Fee__c
                  FROM
                     project_cloud__Project_Tasks__r)
            FROM
                project_cloud__Project_Phase__c
            WHERE
                project_cloud__Project__c =: testProject.Id
        ];
        
        System.assertEquals(2, createdPhases.size());
        
        System.assertNotEquals(createdPhases[0].Name, createdPhases[1].Name);
        
        for (project_cloud__Project_Phase__c phase : createdPhases) {
            System.assert(phase.Name == 'Develop' || phase.Name == 'Connect');
            //System.assertEquals(1, phase.project_cloud__Project_Tasks__r.size());
            project_cloud__Project_Task__c task = phase.project_cloud__Project_Tasks__r[0];
            /*if (phase.Name == 'Develop') {
                System.assertEquals('4000 Facilitation', task.Service__r.ProductCode);
                System.assertEquals('4001 Delegate Fees', task.Delegate_Fee_Product__r.ProductCode);
                //System.assertEquals(40, task.Delegate_Fee__c);
                System.assertEquals(null, task.Number_of_Licences__c);
                System.assertEquals(null, task.Licence_Fee_Product__c);
                System.assertEquals(0, task.Licence_Fee__c);
            } else {
                System.assertEquals('4001 Facilitation', task.Service__r.ProductCode);
                System.assertEquals('4001 Licence Fees', task.Licence_Fee_Product__r.ProductCode);
                //System.assertEquals(50, task.Licence_Fee__c);
                System.assertEquals(null, task.Number_of_Delegates__c);
                System.assertEquals(null, task.Delegate_Fee_Product__c);
                System.assertEquals(0, task.Delegate_Fee__c);
            }*/
        }
        Test.stopTest();
        
        System.assertEquals(
            true,
            [
                SELECT 
                    Id, Synced_with_Opportunity__c
                FROM
                    project_cloud__Project__c 
                WHERE
                    Id =: testProject.Id
            ].Synced_with_Opportunity__c
        );
    }

}