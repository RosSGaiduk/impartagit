@isTest
private class TestOpportunityTriggerHandler {

    private static testmethod void oppProjectDateUpdation() {
        
        Account a = new account();
        a.name = 'Test1';
        a.CurrencyIsoCode = 'GBP';
        insert a;
        list < OpportunityLineItem > oppLineItemList = new list < OpportunityLineItem > ();
        list < OpportunityLineItem > oppLineItemUpdateList = new list < OpportunityLineItem > ();

        opportunity opp = new opportunity();
        opp.Name = 'test';
        opp.AccountId = a.Id;
        opp.NextStep = 'meet';
        opp.CurrencyIsoCode = 'GBP';
        opp.CloseDate = Date.newInstance(2019, 11, 10);
        opp.Project_Start_Date__c = Date.newInstance(2019, 11, 11);
        opp.StageName = 'Decision';
        opp.Probability = 0.8;
        insert opp;

        opportunity opp1 = new opportunity();
        opp1.Name = 'test1';
        opp1.AccountId = a.Id;
        opp1.NextStep = 'meet';
        opp1.CurrencyIsoCode = 'GBP';
        opp1.CloseDate = Date.newInstance(2019, 11, 10);
        opp1.Project_Start_Date__c = Date.newInstance(2019, 11, 11);
        opp1.StageName = 'Decision';
        insert opp1;

        Product2 pro = new Product2();
        pro.Name = 'Xenogenix Sample Product - Quantity';
        pro.ProductCode = 'X123';
        pro.CurrencyIsoCode = 'GBP';
        insert pro;

        PricebookEntry entry = new PricebookEntry();
        entry.Product2Id = pro.Id;
        entry.Pricebook2Id = Test.getStandardPricebookId();
        entry.UnitPrice = 100.00;
        entry.IsActive = true;
        insert entry;

        OpportunityLineItem oppProduct = new OpportunityLineItem();
        oppProduct.Product2Id = pro.Id;
        oppProduct.PricebookEntryId = entry.Id;
        oppProduct.OpportunityId = opp.Id;
        oppProduct.Quantity = 10;
        oppProduct.UnitPrice = 80.00;
        oppProduct.Cost__c = 180.00;
        oppLineItemList.add(oppProduct);

        OpportunityLineItem oppProduct1 = new OpportunityLineItem();
        oppProduct1.Product2Id = pro.Id;
        oppProduct1.PricebookEntryId = entry.Id;
        oppProduct1.OpportunityId = opp.Id;
        oppProduct1.Quantity = 7;
        oppProduct1.UnitPrice = 50.00;
        oppProduct1.Cost__c = 0;
        oppProduct1.Start_Date__c = Date.newInstance(2019, 10, 23);
        oppLineItemList.add(oppProduct1);

        OpportunityLineItem oppProduct2 = new OpportunityLineItem();
        oppProduct2.Product2Id = pro.Id;
        oppProduct2.PricebookEntryId = entry.Id;
        oppProduct2.OpportunityId = opp1.Id;
        oppProduct2.Quantity = 5;
        oppProduct2.UnitPrice = 10.00;
        oppProduct2.Cost__c = 10;
        oppLineItemList.add(oppProduct2);
        insert oppLineItemList;

        List < Revenue_Item__c > revenueInsertList = new List < Revenue_Item__c > ();
        Revenue_Item__c rev = new Revenue_Item__c();
        rev.Opportunity__c = opp.Id;
        rev.Product__c = pro.Id;
        rev.Opportunity_Product_Id__c = oppProduct.Id;
        rev.Quantity__c = 7;
        rev.Amount__c = 350;
        rev.Cost__c = 0;
        revenueInsertList.add(rev);
        Revenue_Item__c rev1 = new Revenue_Item__c();
        rev1.Opportunity__c = opp.Id;
        rev1.Product__c = pro.Id;
        rev1.Opportunity_Product_Id__c = oppProduct.Id;
        rev1.Quantity__c = 1;
        rev1.Amount__c = 50;
        rev1.Cost__c = 50;
        revenueInsertList.add(rev1);

        insert revenueInsertList;

        //Project start date Change check
        List < Opportunity > oppUpdateList = new List < Opportunity > ();
        List < Opportunity > oppList = null;
        oppList = [Select Id, Project_Start_Date__c, CreatedDate, StageName from Opportunity];
        for (Opportunity opportunity: oppList) {
            if (opportunity.id == opp.id) {
                opportunity.Project_Start_Date__c = Date.newInstance(2019, 11, 15);
                oppUpdateList.add(opportunity);
            } else if (opportunity.id == opp1.id) {
                opportunity.Project_Start_Date__c = Date.newInstance(2019, 11, 16);
                oppUpdateList.add(opportunity);
            }

        }
        test.startTest();
            update oppUpdateList;

            List < Revenue_Item__c > revenueList = null;
            revenueList = [select Id, Opportunity_Product_Id__c, Quantity__c, Amount__c, Cost__c, Date__c, Product__c from Revenue_Item__c];

            system.assertEquals(5, revenueList.size());
            /* if(revenueList.size()!=0){
                 for( Revenue_Item__c revenue:revenueList ){
                     if(revenue.Opportunity_Product_Id__c==oppProduct.id){
                         if(revenue.Date__c==Date.newInstance(2019, 10, 15))
                          system.assertEquals(revenue.Date__c, Date.newInstance(2019, 10, 15));
                         if(revenue.Date__c==Date.newInstance(2019, 11, 15))
                          system.assertEquals(revenue.Date__c, Date.newInstance(2019, 11, 15));
                         if(revenue.Date__c==Date.newInstance(2019, 12, 15))
                          system.assertEquals(revenue.Date__c, Date.newInstance(2019, 12, 15));  
                     }
                     else if(revenue.Opportunity_Product_Id__c==oppProduct1.id){
                         //system.assertEquals(revenue.Date__c, Date.newInstance(2019, 10, 15));   
                     }
                     else if(revenue.Opportunity_Product_Id__c==oppProduct2.id){
                         //system.assertEquals(revenue.Date__c, Date.newInstance(2019, 11, 16)); 
                     }
                  
             }
             }*/

            //Stage Change Check
            List < Opportunity > oppUpdateList1 = new List < Opportunity > ();
            List < Opportunity > oppList1 = null;
            oppList1 = [Select Id, StageName from Opportunity];
            for (Opportunity opportunity: oppList1) {
                if (opportunity.id == opp.id) {
                    opportunity.StageName = 'Closed Lost';
                    opportunity.Loss_Reason__c = 'Other';
                    oppUpdateList1.add(opportunity);
                }
            }

            update oppUpdateList1;
            List < Revenue_Item__c > revenueList1 = null;
            revenueList1 = [select Id from Revenue_Item__c where Opportunity__c in: oppUpdateList];
            system.assertEquals(1, revenueList1.size());
            
            delete oppUpdateList;
            revenueList1 = [select Id from Revenue_Item__c where Opportunity__c in: oppUpdateList];
            system.assertEquals(0, revenueList1.size());
        test.stopTest();
    }

}