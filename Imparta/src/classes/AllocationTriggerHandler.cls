public class AllocationTriggerHandler {

    public static Boolean isExecutingInFuture = false;

    public static void handleBeforeInsertOrUpdate(List<project_cloud__Allocation__c> newAllocations) {
        Set<Id> allocationGroupsIds = new Set<Id>();
        Set<Id> usersIds = new Set<Id>();
        for (project_cloud__Allocation__c allocation : newAllocations) {
            allocationGroupsIds.add(allocation.project_cloud__Allocation_Group__c);
            usersIds.add(allocation.project_cloud__User__c);
        }

        Map<Id, project_cloud__Allocation_Group__c> allocationGroupMap = new Map<Id, project_cloud__Allocation_Group__c>([
            SELECT
                project_cloud__Allocation_Summary__r.project_cloud__Project_Task__r.project_cloud__Work_Type__c,
                project_cloud__Allocation_Summary__r.project_cloud__Project_Task__r.project_cloud__Project_Phase__r.project_cloud__Project__c
            FROM
                project_cloud__Allocation_Group__c
            WHERE
                Id IN :allocationGroupsIds
        ]);

        Set<Id> workTypesIds = new Set<Id>();
        Set<Id> projectsIds = new Set<Id>();
        for (project_cloud__Allocation_Group__c allocationGroup : allocationGroupMap.values()) {
            workTypesIds.add(allocationGroup.project_cloud__Allocation_Summary__r.project_cloud__Project_Task__r.project_cloud__Work_Type__c);
            projectsIds.add(allocationGroup.project_cloud__Allocation_Summary__r.project_cloud__Project_Task__r.project_cloud__Project_Phase__r.project_cloud__Project__c);
        }

        List<Cost_Rate__c> costRates = [
            SELECT
                Work_Type__c, Project__c, Resource__c, Cost_Rate__c, CurrencyIsoCode
            FROM
                Cost_Rate__c
            WHERE
                Work_Type__c IN :workTypesIds
                AND Project__c IN :projectsIds
                AND Resource__c IN :usersIds
        ];

        Map<Id, List<project_cloud__Allocation__c>> userIdAllocationsMap = new Map<Id, List<project_cloud__Allocation__c>>();
        for (project_cloud__Allocation__c allocation : newAllocations) {
            project_cloud__Project_Task__c allocationTask =
                    allocationGroupMap.get(allocation.project_cloud__Allocation_Group__c)
                            .project_cloud__Allocation_Summary__r.project_cloud__Project_Task__r;
            Id allocationWorkTypeId =
                    allocationTask.project_cloud__Work_Type__c;
            Id allocationProjectId =
                    allocationTask.project_cloud__Project_Phase__r.project_cloud__Project__c;

            Boolean isCostRateFound = false;
            for (Cost_Rate__c rate : costRates) {
                if (rate.Project__c == allocationProjectId && rate.Work_Type__c == allocationWorkTypeId && rate.Resource__c == allocation.project_cloud__User__c) {
                    allocation.Cost_Rate__c = MultiCurrencyService.convertCurrency(rate.Cost_Rate__c / 8, rate.CurrencyIsoCode, allocation.CurrencyIsoCode);
                    isCostRateFound = true;
                    break;
                }
            }

            if (!isCostRateFound) {
                if (userIdAllocationsMap.get(allocation.project_cloud__User__c) == null) {
                    userIdAllocationsMap.put(allocation.project_cloud__User__c, new List<project_cloud__Allocation__c>());
                }
                userIdAllocationsMap.get(allocation.project_cloud__User__c).add(allocation);
            }
        }

        if (!userIdAllocationsMap.isEmpty()) {
            List<User> users = [
                SELECT
                    Id, Cost_Rate__c, Currency_Iso_Code__c
                FROM
                    User
                WHERE
                    Id IN :userIdAllocationsMap.keySet()
            ];

            for (User u : users) {
                for (project_cloud__Allocation__c allocation : userIdAllocationsMap.get(u.Id)) {
                    allocation.Cost_Rate__c = MultiCurrencyService.convertCurrency(u.Cost_Rate__c / 8, u.Currency_Iso_Code__c, allocation.CurrencyIsoCode);
                }
            }
        }
    }

    public static void handleAfterInsertOrUpdate(List<project_cloud__Allocation__c> newAllocations) {
        if (isExecutingInFuture || System.isBatch() || System.isFuture()) return;

        Set<Id> allocationGroupsIds = new Set<Id>();
        for (project_cloud__Allocation__c allocation : newAllocations) {
            allocationGroupsIds.add(allocation.project_cloud__Allocation_Group__c);
        }

        List<project_cloud__Allocation_Group__c> allocationGroupsOfNewAllocations = [
            SELECT
                project_cloud__Allocation_Summary__r.project_cloud__Project_Task__c
            FROM
                project_cloud__Allocation_Group__c
            WHERE
                Id IN :allocationGroupsIds
        ];

        Set<Id> tasksIds = new Set<Id>();
        for (project_cloud__Allocation_Group__c allocationGroup : allocationGroupsOfNewAllocations) {
            tasksIds.add(allocationGroup.project_cloud__Allocation_Summary__r.project_cloud__Project_Task__c);
        }

        populateTasksAllocationsHoursField(tasksIds);
    }

    public static void handleBeforeDelete(List<project_cloud__Allocation__c> deletedAllocations) {
        List<Booking__c> bookingsToDelete = [SELECT Id FROM Booking__c WHERE Allocation__c in:deletedAllocations];
        
        If(bookingsToDelete != null && bookingsToDelete.size() > 0){
            delete bookingsToDelete;
         }
    }

    @Future
    public static void populateTasksAllocationsHoursField(Set<Id> tasksIds) {
        List<project_cloud__Allocation__c> allocations = [
            SELECT
                Id, project_cloud__Hours__c
            FROM
                project_cloud__Allocation__c
            WHERE
                project_cloud__Allocation_Group__r.project_cloud__Allocation_Summary__r.project_cloud__Project_Task__c IN :tasksIds
            FOR UPDATE
        ];

        for (project_cloud__Allocation__c allocation : allocations) {
            allocation.Hours__c = allocation.project_cloud__Hours__c;
        }

        isExecutingInFuture = true;
        update allocations;
    }

}