public class AllocationGroupTriggerHandler {

	public static void handleAfterUpdate(List<project_cloud__Allocation_Group__c> newAllocationGroups) {
		List<project_cloud__Allocation_Group__c> allocationGroups = [
			SELECT
				project_cloud__Allocation_Summary__r.project_cloud__Project_Task__c
			FROM
				project_cloud__Allocation_Group__c
			WHERE
				Id IN :newAllocationGroups
		];

		Set<Id> tasksIds = new Set<Id>();
		for (project_cloud__Allocation_Group__c allocationGroup : allocationGroups) {
			tasksIds.add(allocationGroup.project_cloud__Allocation_Summary__r.project_cloud__Project_Task__c);
		}

		if (AllocationTriggerHandler.isExecutingInFuture || System.isBatch() || System.isFuture()) return;
		AllocationTriggerHandler.populateTasksAllocationsHoursField(tasksIds);
	}

}