public class RevenueItemTriggerHandler {

/*******************************************************************************************
**    This handler class copies the currency from the appropriate Parent object and puts  **
**    it into the Revenue Item                                                            **
**    Commented out areas are placeholders for Phase 2 objects                            **
********************************************************************************************/

    public static void updateCurrencyValue(List<Revenue_Item__c> TriggerNew) {
        Set<ID> OpportuniyIDs=new Set<ID>();

        Map<ID,Opportunity> MapOfOppIDToOpp=new Map<ID,Opportunity>();
        for(Revenue_Item__c rev:TriggerNew) 
        {
            if(rev.Opportunity__c!=null)
                OpportuniyIDs.add(rev.Opportunity__c);
   

        }

        for(Opportunity ord:[SELECT ID,CurrencyIsoCode FROM Opportunity WHERE ID IN: OpportuniyIDs]) 
        {
            MapOfOppIDToOpp.put(ord.ID,ord);    
        }

        for(Revenue_Item__c rev:TriggerNew) 
        {   
            if(rev.Opportunity__c!=null&&MapOfOppIDToOpp.containsKey(rev.Opportunity__c)) 
            {
                if(MapOfOppIDToOpp.get(rev.Opportunity__c).CurrencyIsoCode!=null) 
                {
                    rev.CurrencyIsoCode=MapOfOppIDToOpp.get(rev.Opportunity__c).CurrencyIsoCode;
                }   
            }
        }
    }
    
    public static void handleAfterUpdate(List<Revenue_Item__c> newRevenueItems) {
        List<Revenue_Item__c> revenueItemsToDelete = new List<Revenue_Item__c>();
        List<Id> revenueItemIds = new List<Id>();
/*        
        for (Revenue_Item__c revenueItem : newRevenueItems) {
            if (checkIfRevenueItemMustBeDeleted(revenueItem)) {
                revenueItemsToDelete.add(new Revenue_Item__c(Id = revenueItem.Id));
            }
            if(revenueItem.Project_Task__c != null ){
                revenueItemIds.add(revenueItem.Id);
            }
        }
        
        delete revenueItemsToDelete; */
    }
    
    
    private static Boolean checkIfRevenueItemMustBeDeleted(Revenue_Item__c revenueItem) {
        return (revenueItem.Project__c == null && revenueItem.Project_Task__c == null
            && revenueItem.Opportunity__c == null && revenueItem.Opportunity_Product_Id__c == null 
            && revenueItem.Account__c == null);
    }
    
}