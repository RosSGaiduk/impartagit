@isTest
public class ConductorRecordCreatorTEST 
{
    
    @isTest
    static void CreateDocumentsPackage()    
    {
        ConductorRecordCreator crc = new ConductorRecordCreator();

        List<APXTConga4__Conga_Merge_Query__c> newQueries = [SELECT Id FROM APXTConga4__Conga_Merge_Query__c 
                                                            WHERE APXTConga4__Name__c IN ('CCN – Invoices To Send', 'CCN – Customer Statements To Send', 'CCN – Checks To Print', 'CCN – Payment Remittances To Send')
                                                            AND CreatedDate = Today];
            
        List<APXT_BPM__Conductor__c> newConductorRecords = [SELECT Id FROM APXT_BPM__Conductor__c 
                                                           WHERE APXT_BPM__Title__c IN ('Email Sales Invoices', 'Send Customer Statements', 'Print Checks', 'Send Payment Remittances')
                                                           AND CreatedDate = Today];        

        System.assert(newQueries.size() == 4);
        System.assert(newConductorRecords.size() == 4);  
              
    }

    
}