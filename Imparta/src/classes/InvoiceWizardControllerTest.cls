@isTest
public class InvoiceWizardControllerTest {

    @testSetup
    static void setup() {
        Project_Invoice_Generation__c projectInvoiceGeneration = new Project_Invoice_Generation__c(
                Transaction_Should_Be_Created__c = true
        );
        insert projectInvoiceGeneration;

        project_cloud__Project__c testProject = new project_cloud__Project__c();
        insert testProject;

        project_cloud__Project_Phase__c testProjectPhase = new project_cloud__Project_Phase__c(
                project_cloud__Project__c = testProject.Id
        );
        insert testProjectPhase;

        Product2 prod = new Product2(Name = 'testProduct');
        insert prod;
    }

    @isTest
    static void checkTimeTasks() {
        project_cloud__Project_Phase__c projectPhase = getProjectPhase();

        Product2 prod = getProduct();
        createTimeProjectTask(projectPhase.Id, prod.Id);

        setCurrentPage(projectPhase);

        InvoiceWizardController invoiceController = new InvoiceWizardController();
        invoiceController.selectedPONumber = 'None';
        invoiceController.initData();

        System.assertEquals(1, invoiceController.timeTaskWrappers.size());
        System.assertEquals(0, invoiceController.delegateFeeTaskWrappers.size());
        System.assertEquals(0, invoiceController.licenceTaskWrappers.size());
        System.assertEquals(0, invoiceController.expenseWrappers.size());

        invoiceController.createInvoice();

        System.assertEquals(1, invoiceController.timeTaskInvoiceItems.size());
        System.assertEquals(1, invoiceController.timeTaskInvoiceItems[0].tasks.size());

        System.assertEquals(0, invoiceController.delegateFeeTaskInvoiceItems.size());

        System.assertEquals(0, invoiceController.licenceTaskInvoiceItems.size());

        System.assertEquals(0, invoiceController.expenseInvoiceItems.size());
    }

    @isTest
    static void checkDelegateFeeTasks() {
        project_cloud__Project_Phase__c projectPhase = getProjectPhase();

        Product2 prod = getProduct();

        createDelegateFeeTask(projectPhase.Id, prod.Id);

        setCurrentPage(projectPhase);

        InvoiceWizardController invoiceController = new InvoiceWizardController();
        invoiceController.selectedPONumber = 'None';
        invoiceController.initData();

        System.assertEquals(1, invoiceController.timeTaskWrappers.size());
        System.assertEquals(1, invoiceController.delegateFeeTaskWrappers.size());
        System.assertEquals(0, invoiceController.licenceTaskWrappers.size());
        System.assertEquals(0, invoiceController.expenseWrappers.size());

        invoiceController.createInvoice();

        System.assertEquals(1, invoiceController.timeTaskInvoiceItems.size());
        System.assertEquals(1, invoiceController.timeTaskInvoiceItems[0].tasks.size());

        System.assertEquals(1, invoiceController.delegateFeeTaskInvoiceItems.size());
        System.assertEquals(1, invoiceController.delegateFeeTaskInvoiceItems[0].tasks.size());

        System.assertEquals(0, invoiceController.licenceTaskInvoiceItems.size());

        System.assertEquals(0, invoiceController.expenseInvoiceItems.size());
    }

    @isTest
    static void checkLicenceTasks() {
        project_cloud__Project_Phase__c projectPhase = getProjectPhase();

        Product2 prod = getProduct();

        createLicenceTask(projectPhase.Id, prod.Id);

        setCurrentPage(projectPhase);

        InvoiceWizardController invoiceController = new InvoiceWizardController();
        invoiceController.selectedPONumber = 'None';
        invoiceController.initData();

        System.assertEquals(1, invoiceController.timeTaskWrappers.size());
        System.assertEquals(0, invoiceController.delegateFeeTaskWrappers.size());
        System.assertEquals(1, invoiceController.licenceTaskWrappers.size());
        System.assertEquals(0, invoiceController.expenseWrappers.size());

        invoiceController.createInvoice();

        System.assertEquals(1, invoiceController.timeTaskInvoiceItems.size());
        System.assertEquals(1, invoiceController.timeTaskInvoiceItems[0].tasks.size());

        System.assertEquals(0, invoiceController.delegateFeeTaskInvoiceItems.size());

        System.assertEquals(1, invoiceController.licenceTaskInvoiceItems.size());
        System.assertEquals(1, invoiceController.licenceTaskInvoiceItems[0].tasks.size());

        System.assertEquals(0, invoiceController.expenseInvoiceItems.size());
    }

    @isTest
    static void checkExpenses() {
        project_cloud__Project_Phase__c projectPhase = getProjectPhase();

        createExpense(projectPhase);

        setCurrentPage(projectPhase);

        InvoiceWizardController invoiceController = new InvoiceWizardController();
        invoiceController.selectedPONumber = 'None';
        invoiceController.initData();

        System.assertEquals(0, invoiceController.timeTaskWrappers.size());
        System.assertEquals(0, invoiceController.delegateFeeTaskWrappers.size());
        System.assertEquals(0, invoiceController.licenceTaskWrappers.size());
        System.assertEquals(1, invoiceController.expenseWrappers.size());

        invoiceController.createInvoice();

        System.assertEquals(0, invoiceController.timeTaskInvoiceItems.size());

        System.assertEquals(0, invoiceController.delegateFeeTaskInvoiceItems.size());

        System.assertEquals(0, invoiceController.licenceTaskInvoiceItems.size());

        System.assertEquals(1, invoiceController.expenseInvoiceItems.size());
        System.assertEquals(1, invoiceController.expenseInvoiceItems[0].expenses.size());
    }

    @isTest
    static void checkAllTasks() {
        project_cloud__Project_Phase__c projectPhase = getProjectPhase();

        Product2 prod = getProduct();

        createTimeProjectTask(projectPhase.Id, prod.Id);
        createDelegateFeeTask(projectPhase.Id, prod.Id);
        createLicenceTask(projectPhase.Id, prod.Id);

        setCurrentPage(projectPhase);

        InvoiceWizardController invoiceController = new InvoiceWizardController();
        invoiceController.selectedPONumber = 'None';
        invoiceController.initData();

        System.assertEquals(3, invoiceController.timeTaskWrappers.size());
        System.assertEquals(1, invoiceController.delegateFeeTaskWrappers.size());
        System.assertEquals(1, invoiceController.licenceTaskWrappers.size());
        System.assertEquals(0, invoiceController.expenseWrappers.size());

        invoiceController.createInvoice();

//        System.assertEquals(1, invoiceController.timeTaskInvoiceItems.size());
//        System.assertEquals(3, invoiceController.timeTaskInvoiceItems[0].tasks.size());

        System.assertEquals(1, invoiceController.delegateFeeTaskInvoiceItems.size());
        System.assertEquals(1, invoiceController.delegateFeeTaskInvoiceItems[0].tasks.size());

        System.assertEquals(1, invoiceController.licenceTaskInvoiceItems.size());
        System.assertEquals(1, invoiceController.licenceTaskInvoiceItems[0].tasks.size());

        System.assertEquals(0, invoiceController.expenseInvoiceItems.size());
    }
    
    @isTest
    static void checkAllTasksAndExpense() {

        project_cloud__Project_Phase__c projectPhase = getProjectPhase();

        Product2 prod = getProduct();

        createTimeProjectTask(projectPhase.Id, prod.Id);
        createDelegateFeeTask(projectPhase.Id, prod.Id);
        createLicenceTask(projectPhase.Id, prod.Id);
        createExpense(projectPhase);

        createExpense(projectPhase);

        setCurrentPage(projectPhase);

        InvoiceWizardController invoiceController = new InvoiceWizardController();
        invoiceController.selectedPONumber = 'None';
        invoiceController.initData();

        System.assertEquals(3, invoiceController.timeTaskWrappers.size());
        System.assertEquals(1, invoiceController.delegateFeeTaskWrappers.size());
        System.assertEquals(1, invoiceController.licenceTaskWrappers.size());
        System.assertEquals(2, invoiceController.expenseWrappers.size());

        invoiceController.createInvoice();


        System.assertEquals(1, invoiceController.delegateFeeTaskInvoiceItems.size());
        System.assertEquals(1, invoiceController.delegateFeeTaskInvoiceItems[0].tasks.size());

        System.assertEquals(1, invoiceController.licenceTaskInvoiceItems.size());
        System.assertEquals(1, invoiceController.licenceTaskInvoiceItems[0].tasks.size());

    }

    @isTest
    static void checkGroupNumber() {
        project_cloud__Project_Phase__c projectPhase = getProjectPhase();

        Product2 prod = getProduct();

        project_cloud__Project_Task__c prTask = createTimeProjectTask(projectPhase.Id, prod.Id);
        prTask.Service__c = prod.Id;
        update prTask;

        createTimeProjectTask(projectPhase.Id, prod.Id);
        prTask.Service__c = null;
        update prTask;

        setCurrentPage(projectPhase);

        InvoiceWizardController invoiceController = new InvoiceWizardController();
        invoiceController.selectedPONumber = 'None';
        invoiceController.initData();

        System.assertEquals(1, invoiceController.timeTaskWrappers[0].groupNumber);
        System.assertEquals(2, invoiceController.timeTaskWrappers[1].groupNumber);

        invoiceController.timeTaskWrappers[0].groupNumber = 4;
    }

    @isTest
    static void checkOnGroupNumberErrors() {
        project_cloud__Project_Phase__c projectPhase = getProjectPhase();

        Product2 prod = getProduct();

        project_cloud__Project_Task__c prTask = createTimeProjectTask(projectPhase.Id, prod.Id);
        prTask.Service__c = prod.Id;
        update prTask;

        createTimeProjectTask(projectPhase.Id, prod.Id);
        createDelegateFeeTask(projectPhase.Id, prod.Id);

        setCurrentPage(projectPhase);

        InvoiceWizardController invoiceController = new InvoiceWizardController();
        invoiceController.selectedPONumber = 'None';
        invoiceController.initData();

        invoiceController.timeTaskWrappers[0].groupNumber = 3;

        invoiceController.delegateFeeTaskWrappers[0].groupNumber = 5;

        invoiceController.delegateFeeTaskWrappers[0].groupNumber = 4;
        System.assertNotEquals(null,invoiceController.createInvoice());
    }

    @isTest
    static void createDelegFeeAndLicenseTasks(){
        project_cloud__Project_Phase__c projectPhase = getProjectPhase();

        Product2 prod = getProduct();

        project_cloud__Project_Task__c delegTask = createDelegateFeeTask(projectPhase.Id, prod.Id);
        delegTask.Delegate_Fee_Product__c = prod.Id;
        update delegTask;

        project_cloud__Project_Task__c licenTask = createLicenceTask(projectPhase.Id, prod.Id);
        licenTask.Licence_Fee_Product__c = prod.Id;
        update licenTask;

        createDelegateFeeTask(projectPhase.Id, prod.Id);
        createLicenceTask(projectPhase.Id, prod.Id);
        createExpense(projectPhase);

        setCurrentPage(projectPhase);

        Test.startTest();
        InvoiceWizardController invoiceController = new InvoiceWizardController();
        invoiceController.selectedPONumber = 'None';
        invoiceController.initData();

        invoiceController.timeTaskWrappers[0].isGrouped = false;
        invoiceController.expenseWrappers[0].isGrouped = false;

        invoiceController.createInvoice();
        Test.stopTest();
    }

    @isTest
    static void saveInvoiceAndItsItems(){
        project_cloud__Project_Phase__c projectPhase = getProjectPhase();
        Account acc = new Account(Name='testAcc', CurrencyIsoCode = 'GBP');
        insert acc;

        Product2 prod = getProduct();

        createTimeProjectTask(projectPhase.Id, prod.Id);
        createDelegateFeeTask(projectPhase.Id, prod.Id);
        createLicenceTask(projectPhase.Id, prod.Id);

        createExpense(projectPhase);

        setCurrentPage(projectPhase);

        InvoiceWizardController invoiceController = new InvoiceWizardController();
        invoiceController.selectedPONumber = 'None';
        invoiceController.initData();

        Id curUserId = UserInfo.getUserId();
        User u = [
                SELECT
                    CompanyName
                FROM
                    User
                Where
                    ID = :curUserId
        ];
        u.CompanyName = 'test Company';
        update u;

        invoiceController.project.ccpe_ocp__Account__c = acc.Id;
        invoiceController.project.OwnerId = u.Id;
        update invoiceController.project;

        s2cor__Sage_COR_Currency__c curr = new s2cor__Sage_COR_Currency__c(
                Name = 'currency name',
                s2cor__Currency_Code__c = '123',
                s2cor__Minor_Unit__c = 1,
                s2cor__Numeric_Code__c = '321'
        );
        insert curr;

        s2cor__Sage_COR_Legislation__c legislation = new s2cor__Sage_COR_Legislation__c(Name='testLeg');
        insert legislation;

        s2cor__Sage_COR_Company__c company = new s2cor__Sage_COR_Company__c(
                Name = 'test Company',
                CurrencyIsoCode = 'GBP',
                s2cor__Base_Currency__c = curr.Id,
                s2cor__Legislation__c = legislation.Id
        );
        insert company;

        s2cor__Sage_INV_Trade_Document_Type__c tradeDocType = new s2cor__Sage_INV_Trade_Document_Type__c(
                CurrencyIsoCode = 'GBP'
        );
        insert tradeDocType;

        Sales_Invoice_Transaction_Types__c customSettingSITT =  new Sales_Invoice_Transaction_Types__c(
                Name = 'test',
                Sage_Company_Id__c = company.Id,
                Sales_Invoice_Transaction_Type_Id__c = tradeDocType.Id
        );
        insert customSettingSITT;

        invoiceController.createInvoice();
        invoiceController.goToTasksAndExpensesPage();
        invoiceController.recalculatePage();
        invoiceController.saveInvoiceAndItsItems();
    }

    private static project_cloud__Project_Phase__c getProjectPhase() {
        project_cloud__Project_Phase__c projectPhase = [
                SELECT
                    Id, project_cloud__Project__c
                FROM
                    project_cloud__Project_Phase__c
                LIMIT 1
        ];

        return projectPhase;
    }

    private static Product2 getProduct() {
        Product2 product = [
                SELECT
                    Id
                FROM
                    Product2
                LIMIT 1
        ];

        return product;
    }

    private static project_cloud__Project_Task__c createTimeProjectTask(Id projectPhaseId, Id productId) {
        Date td = Date.Today();
        Date startdate = td.toStartofWeek();
        project_cloud__Project_Task__c testTimeProjTask = new project_cloud__Project_Task__c(
                Name = 'testTask',
                project_cloud__Project_Phase__c = projectPhaseId,
                project_cloud__Autonomous_Start__c = startdate,
                project_cloud__Duration__c = 2,
                project_cloud__Completed_Date__c = Date.today().addDays(2),
                project_cloud__Estimated_Hours__c = 16,
                Service__c = productId,
                project_cloud__IsComplete__c = true,
                Ready_to_Invoice__c = true,
                Service_Price__c = 50,
                Invoiced__c = false,
                Billable__c = true
        );
        
        insert testTimeProjTask;
        
        return modifyTask(testTimeProjTask);
    }

    private static project_cloud__Project_Task__c createDelegateFeeTask(Id projectPhaseId, Id productId) {
        Date td = Date.Today();
        Date startdate = td.toStartofWeek();
        project_cloud__Project_Task__c testDelegateFeeTask = new project_cloud__Project_Task__c(
                Name = 'testTask',
                project_cloud__Project_Phase__c = projectPhaseId,
                Invoiced__c = false,
                Billable__c = true,
                project_cloud__IsComplete__c = true,
                Ready_to_Invoice__c = true,
                Delegate_Fee_Product__c = productId,
                Number_of_Delegates__c = 5,
                Delegate_Fee__c = 5,
                project_cloud__Autonomous_Start__c = startdate,
                project_cloud__Completed_Date__c = Date.today().addDays(2),
                project_cloud__Duration__c = 2,
                project_cloud__Estimated_Hours__c = 1
        );

        insert testDelegateFeeTask;
        
        return modifyTask(testDelegateFeeTask);
    }

    private static project_cloud__Project_Task__c createLicenceTask(Id projectPhaseId, Id productId) {
        Date td = Date.Today();
        Date startdate = td.toStartofWeek();
        project_cloud__Project_Task__c testLicenceTask = new project_cloud__Project_Task__c(
                Name = 'testTask',
                project_cloud__Project_Phase__c = projectPhaseId,
                Invoiced__c = false,
                Billable__c = true,
                project_cloud__IsComplete__c = true,
                Ready_to_Invoice__c = true,
                Licence_Fee_Product__c = productId,
                Number_of_Licences__c = 5,
                Licence_Fee__c = 5,
                project_cloud__Autonomous_Start__c = startdate,
                project_cloud__Completed_Date__c = Date.today().addDays(2),
                project_cloud__Duration__c = 2,
                project_cloud__Estimated_Hours__c = 10,
                Number_of_Delegates__c = 5
        );

        insert testLicenceTask;
        
        return modifyTask(testLicenceTask);
    }

    private static project_cloud__Expense2__c createExpense(project_cloud__Project_Phase__c projectPhase) {
        project_cloud__Expense2__c expense = new project_cloud__Expense2__c(
                project_cloud__Project__c = projectPhase.project_cloud__Project__c,
                Invoiced__c = false,
                Billable__c = true,
                project_cloud__Date__c = Date.today().addDays(2),
                project_cloud__Description__c = 'Description',
                project_cloud__Type__c = 'Actual',
                project_cloud__Amount__c = 50
        );
        insert expense;
        return expense;
    }

    private static project_cloud__Project_Task__c modifyTask(project_cloud__Project_Task__c task) {
        task.Ready_to_Invoice__c = true;
        update task;
        
        return task;
    }
    
    private static void setCurrentPage(project_cloud__Project_Phase__c projectPhase) {
        Test.setCurrentPage(Page.Tasks_and_Expenses);
        ApexPages.currentPage().getParameters().put('id', projectPhase.project_cloud__Project__c);
    }
}