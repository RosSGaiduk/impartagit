public class CustomerStatementLauncherController{

    private final Account accountRecord;
    Map<String, Id> congaSolutionQueryMap = new Map<String, Id>();
    public Id StatementDetailsId {get; set;}
    public Id InvoiceSumId {get; set;}
    public Id TotalDueId {get; set;}
    public Id CurrBalDueId {get; set;}
    public Id BalDue030Id {get; set;}
    public Id BalDue3060Id {get; set;}
    public Id BalDue6090Id {get; set;}
    public Id Over90DueId {get; set;}
    public Id AppliedPaymentsId {get; set;}
    public Id InvoiceCountId {get; set;}  
    public Id StatementTemplateId {get; set;}            
    public Id StatementEmailTemplateId {get; set;}            
    public Id Qvar0Id {get; set;}            
    public Id Qvar1Id {get; set;}            
    public Id Qvar2Id {get; set;}            
        
    /* Default Constructor */
    public CustomerStatementLauncherController(){   
        findQueryIds();
        findStatementTemplateId();
        findStatementEmailTemplateId();
        findQvarRecords();
    } 
    
    /* Standard Controller Constructor */
    public CustomerStatementLauncherController(ApexPages.StandardController stdController) {
        this.accountRecord = (Account)stdController.getRecord();
        findQueryIds();
        findStatementTemplateId();
        findStatementEmailTemplateId();
        findQvarRecords();
    }

    private void findQueryIds(){
        String queryAlias;
        
        for(APXTConga4__Conga_Solution_Query__c csq : [SELECT Id, Name, APXTConga4__Conga_Query__c, APXTConga4__Conga_Query__r.Name, APXTConga4__Conga_Query__r.APXTConga4__Name__c, APXTConga4__Conga_Solution__c, APXTConga4__Conga_Solution__r.Name 
                                                      FROM APXTConga4__Conga_Solution_Query__c 
                                                      WHERE isDeleted = false AND APXTConga4__Conga_Solution__r.Name = 'Conga Customer Statement' LIMIT 25000])
        {
            
            if(csq.APXTConga4__Conga_Query__r.APXTConga4__Name__c.contains('[') && csq.APXTConga4__Conga_Query__r.APXTConga4__Name__c.contains(']'))            
            {
                queryAlias = csq.APXTConga4__Conga_Query__r.APXTConga4__Name__c.substring(csq.APXTConga4__Conga_Query__r.APXTConga4__Name__c.IndexOf('[') + 1, csq.APXTConga4__Conga_Query__r.APXTConga4__Name__c.IndexOf(']'));
                congaSolutionQueryMap.put(queryAlias, csq.APXTConga4__Conga_Query__c);
            }            
            
        }
            system.debug('********************** congaSolutionQueryMap: ' + congaSolutionQueryMap);
            
            StatementDetailsId = congaSolutionQueryMap.get('StatementDetails');
                system.debug('********************** StatementDetailsId: ' + StatementDetailsId); 
            InvoiceSumId = congaSolutionQueryMap.get('InvoiceSum');
                system.debug('********************** InvoiceSumId: ' + InvoiceSumId); 
            TotalDueId = congaSolutionQueryMap.get('TotalDue');
                system.debug('********************** TotalDueId: ' + TotalDueId);  
            CurrBalDueId = congaSolutionQueryMap.get('CurrBalDue');
                system.debug('********************** CurrBalDueId: ' + CurrBalDueId);  
            BalDue030Id = congaSolutionQueryMap.get('BalDue030');
                system.debug('********************** BalDue030Id: ' + BalDue030Id);  
            BalDue3060Id = congaSolutionQueryMap.get('BalDue3060');
                system.debug('********************** BalDue3060Id: ' + BalDue3060Id);   
            BalDue6090Id = congaSolutionQueryMap.get('BalDue6090');
                system.debug('********************** BalDue6090Id: ' + BalDue6090Id);  
            Over90DueId = congaSolutionQueryMap.get('Over90Due');
                system.debug('********************** Over90DueId: ' + Over90DueId);  
            AppliedPaymentsId = congaSolutionQueryMap.get('AppliedPayments');
                system.debug('********************** AppliedPaymentsId: ' + AppliedPaymentsId);  
            InvoiceCountId = congaSolutionQueryMap.get('InvoiceCount');
                system.debug('********************** InvoiceCountId: ' + InvoiceCountId);                                    
    }
    
    private void findStatementTemplateId(){
        APXTConga4__Conga_Template__c statementInvoiceRecord = [SELECT Id FROM APXTConga4__Conga_Template__c 
                                        WHERE APXTConga4__Template_Group__c = 'Statement' 
                                        AND APXTConga4__Description__c LIKE 'Created By Solution Pack for Composer Solution: Conga Customer Statement (Account)%' 
                                        LIMIT 1];
        
        StatementTemplateId = statementInvoiceRecord.Id;
            system.debug('********************** StatementTemplateId: ' + StatementTemplateId);       
    }
    
    private void findStatementEmailTemplateId(){
        APXTConga4__Conga_Email_Template__c statementEmailRecord = [SELECT Id FROM APXTConga4__Conga_Email_Template__c 
                                        WHERE APXTConga4__Template_Group__c = 'Statement' 
                                        AND APXTConga4__Description__c = 'Created By Solution Pack for Composer Solution: Conga Customer Statement (Account)' 
                                        LIMIT 1];
        
        StatementEmailTemplateId = statementEmailRecord.Id;
            system.debug('********************** StatementEmailTemplateId: ' + StatementEmailTemplateId);       
    }
    
    private void findQvarRecords(){
        Map<String, Id> QvarMap = new Map<String, Id>();        
        
        for(APXTConga4__Conga_Merge_Query__c cq : [SELECT Id, Name, APXTConga4__Name__c, APXTConga4__Description__c 
                                                  FROM APXTConga4__Conga_Merge_Query__c 
                                                  WHERE APXTConga4__Name__c LIKE '%[QVAR%' 
                                                  LIMIT 25000])
        {
            system.debug('********************** cq.APXTConga4__Name__c: ' + cq.APXTConga4__Name__c);   
            system.debug('********************** cq.APXTConga4__Description__c: ' + cq.APXTConga4__Description__c);
            system.debug('********************** cq.APXTConga4__Name__c.contains(\'[QVAR\'): ' + cq.APXTConga4__Name__c.contains('[QVAR'));
            system.debug('********************** cq.APXTConga4__Description__c.contains(\'Composer Solution: Conga Customer Statement (Account)\'): ' + cq.APXTConga4__Description__c.contains('Composer Solution: Conga Customer Statement (Account)'));               
            
            if(cq.APXTConga4__Name__c.contains('[QVAR') && cq.APXTConga4__Description__c.contains('Composer Solution: Conga Customer Statement (Account)'))
            {
                QvarMap.put(cq.APXTConga4__Name__c, cq.Id);             
            }    
        }
            system.debug('********************** QvarMap: ' + QvarMap);   
    
        Qvar0Id = QvarMap.get('[QVAR0]');
            system.debug('********************** Qvar0Id: ' + Qvar0Id);
        Qvar1Id = QvarMap.get('[QVAR1]');
            system.debug('********************** Qvar1Id: ' + Qvar1Id);   
        Qvar2Id = QvarMap.get('[QVAR2]');
            system.debug('********************** Qvar2Id: ' + Qvar2Id);                              

    }   

}