@isTest
public class Test_OppProductTriggerHandler{
    
    public static testmethod void oppLineItemInsertion(){
        test.startTest();
        Account a = new account();
        a.name = 'Test1';
        a.CurrencyIsoCode = 'GBP';
        insert a;
        list<OpportunityLineItem> oppLineItemList = new list<OpportunityLineItem>();
       
        opportunity opp = new opportunity();
            opp.Name='test';
            opp.AccountId=a.Id;
            opp.NextStep='meet';
            opp.CurrencyIsoCode='GBP';
            opp.CloseDate = Date.today().addDays(1);
            opp.StageName='Decision';
            opp.Probability=0.8;
            opp.Win_Probability__c='80%';
        insert opp;
        
        Product2 pro = new Product2();
        pro.Name = 'Xenogenix Sample Product - Quantity';
        pro.ProductCode = 'X123';
        pro.CurrencyIsoCode = 'GBP';
        insert pro;
       
        PricebookEntry entry = new PricebookEntry();
        entry.Product2Id = pro.Id;
        entry.Pricebook2Id = Test.getStandardPricebookId();
        entry.UnitPrice = 100.00;
        entry.IsActive = true;
        insert entry;
        
        OpportunityLineItem oppProduct = new OpportunityLineItem();
        oppProduct.Product2Id = pro.Id;
        oppProduct.PricebookEntryId = entry.Id;
        oppProduct.OpportunityId=opp.Id;
        oppProduct.Quantity = 10;
        oppProduct.UnitPrice = 80.00;
        oppProduct.Cost__c=180.00;
        oppProduct.Line_Probability__c='80%';
        oppLineItemList.add(oppProduct);
        
        OpportunityLineItem oppProduct1 = new OpportunityLineItem();
        oppProduct1.Product2Id = pro.Id;
        oppProduct1.PricebookEntryId = entry.Id;
        oppProduct1.OpportunityId=opp.Id;
        oppProduct1.Quantity = 7;
        oppProduct1.UnitPrice = 50.00;
        oppProduct1.Cost__c=150.00;
        oppProduct1.Line_Probability__c='50%';
        oppProduct1.Start_Date__c = Date.newInstance(2019, 10, 23);
        //oppLineItemList.add(oppProduct1);
        
        insert oppLineItemList;
        
        List<Revenue_Item__c> revenueList = null;
        revenueList=[select Id,Opportunity_Product_Id__c,Quantity__c,Amount__c,Cost__c,Date__c,Product__c,Revenue_Probability__c from Revenue_Item__c];
        
        //system.assertEquals(2, revenueList.size());
        if(revenueList.size()!=0){
            for( Revenue_Item__c revenue:revenueList ){
                if(revenue.Opportunity_Product_Id__c==oppProduct.id){
                    system.assertEquals(revenue.Quantity__c, 10);
                    system.assertEquals(revenue.Amount__c, 800.00);
                    system.assertEquals(revenue.Cost__c,1800); 
                    system.assertEquals(revenue.Revenue_Probability__c,'80%'); 
                }
                else if(revenue.Opportunity_Product_Id__c==oppProduct1.id){
                    system.assertEquals(revenue.Quantity__c, 7);
                    system.assertEquals(revenue.Amount__c, 350.00);
                    system.assertEquals(revenue.Date__c, Date.newInstance(2019, 10, 23));
                    system.assertEquals(revenue.Cost__c,1050); 
                    system.assertEquals(revenue.Revenue_Probability__c,'50%'); 
                }
             
        }
        }
        
        test.stopTest();
        
    } 
        public static testmethod void oppLineItemUpdation(){
        Account a = new account();
        a.name = 'Test1';
        a.CurrencyIsoCode = 'GBP';
        insert a;
        list<OpportunityLineItem> oppLineItemList = new list<OpportunityLineItem>();
        list<OpportunityLineItem> oppLineItemUpdateList = new list<OpportunityLineItem>();
        
        test.startTest();
        opportunity opp = new opportunity();
            opp.Name='test';
            opp.AccountId=a.Id;
            opp.NextStep='meet';
            opp.CurrencyIsoCode='GBP';
            opp.CloseDate=Date.today().addDays(1);
            opp.StageName='Decision';
            opp.Probability=0.8;
        insert opp;
            
        opportunity opp1 = new opportunity();
            opp1.Name='test1';
            opp1.AccountId=a.Id;
            opp1.NextStep='meet';
            opp1.CurrencyIsoCode='GBP';
            opp1.CloseDate=Date.today().addDays(1);
            opp1.Project_Start_Date__c = Date.today().addDays(1);
            opp1.StageName='Decision';
            opp.Probability=0.8;
        insert opp1;
        
        Product2 pro = new Product2();
        pro.Name = 'Xenogenix Sample Product - Quantity';
        pro.ProductCode = 'X123';
        pro.CurrencyIsoCode = 'GBP';
        insert pro;
       
        PricebookEntry entry = new PricebookEntry();
        entry.Product2Id = pro.Id;
        entry.Pricebook2Id = Test.getStandardPricebookId();
        entry.UnitPrice = 100.00;
        entry.IsActive = true;
        insert entry;
        
        OpportunityLineItem oppProduct = new OpportunityLineItem();
        oppProduct.Product2Id = pro.Id;
        oppProduct.PricebookEntryId = entry.Id;
        oppProduct.OpportunityId=opp.Id;
        oppProduct.Quantity = 5;
        oppProduct.UnitPrice = 80.00;
        oppProduct.Cost__c=180.00;
        oppProduct.Line_Probability__c = '80%';
        oppLineItemList.add(oppProduct);
        
        OpportunityLineItem oppProduct1 = new OpportunityLineItem();
        oppProduct1.Product2Id = pro.Id;
        oppProduct1.PricebookEntryId = entry.Id;
        oppProduct1.OpportunityId=opp.Id;
        oppProduct1.Quantity = 7;
        oppProduct1.UnitPrice = 50.00;
        oppProduct1.Cost__c = 0;
        oppProduct1.Line_Probability__c = '80%';
        oppProduct1.Start_Date__c = Date.newInstance(2019, 10, 23);
        //oppLineItemList.add(oppProduct1);
        
        OpportunityLineItem oppProduct2 = new OpportunityLineItem();
        oppProduct2.Product2Id = pro.Id;
        oppProduct2.PricebookEntryId = entry.Id;
        oppProduct2.OpportunityId=opp1.Id;
        oppProduct2.Quantity = 5;
        oppProduct2.UnitPrice = 10.00;
        oppProduct2.Cost__c = 10;
        oppProduct2.Line_Probability__c = '80%';
        //oppLineItemList.add(oppProduct2);
        
        insert oppLineItemList;
        
         List<Revenue_Item__c> revenueInsertList = new List<Revenue_Item__c>();
         Revenue_Item__c rev = new Revenue_Item__c();
            rev.Opportunity__c = opp.Id;
            rev.Product__c = pro.Id;
            rev.Opportunity_Product_Id__c = oppProduct.Id;
            rev.Quantity__c = 4;
            rev.Amount__c = 4;
            rev.Cost__c=1;
         revenueInsertList.add(rev);
          Revenue_Item__c rev1 = new Revenue_Item__c();
            rev1.Opportunity__c = opp.Id;
            rev1.Product__c = pro.Id;
            rev1.Opportunity_Product_Id__c = oppProduct.Id;  
            rev1.Quantity__c = 6;
            rev1.Amount__c=4;
            rev1.Cost__c=1;
         revenueInsertList.add(rev1);
            
            insert revenueInsertList;
         
         
         List<OpportunityLineItem>   oppLineItemNewList = null;
            oppLineItemNewList = [Select id,Quantity,UnitPrice,Cost__c,Start_Date__c from OpportunityLineItem];
            for(OpportunityLineItem oppProd: oppLineItemNewList){
               if(oppProd.id==oppProduct.id){
                    oppProd.Quantity = 20;
                    oppProd.UnitPrice+=10;
                    oppProd.Start_Date__c = null;
                    oppLineItemUpdateList.add(oppProd);
              }
                else if(oppProd.id==oppProduct1.id){
                    oppProd.Start_Date__c = Date.newInstance(2019, 11, 23);
                    oppLineItemUpdateList.add(oppProd);
                }
                 else if(oppProd.id==oppProduct2.id){
                    oppProd.Cost__c = 20;
                    oppProd.Line_Probability__c = '50%';
                    oppLineItemUpdateList.add(oppProd);
                }
            }
                
        update oppLineItemUpdateList; 
        
        List<Revenue_Item__c> revenueList = null;
        revenueList=[select Id,Opportunity_Product_Id__c,Quantity__c,Amount__c,Cost__c,Date__c,Product__c from Revenue_Item__c];
        
        //system.assertEquals(5, revenueList.size());
            }
    
        public static testmethod void oppLineItemDeletion(){
        Account a = new account();
        a.name = 'Test1';
        a.CurrencyIsoCode = 'GBP';
        insert a;
        list<OpportunityLineItem> oppLineItemList = new list<OpportunityLineItem>();
        
        test.startTest();
        opportunity opp = new opportunity();
            opp.Name='test';
            opp.AccountId=a.Id;
            opp.NextStep='meet';
            opp.CurrencyIsoCode='GBP';
            opp.CloseDate = Date.today().addDays(1);
            opp.StageName='Decision';
            opp.Probability=0.8;
            opp.Project_Start_Date__c = Date.today().addDays(1);
        insert opp;
        
        Product2 pro = new Product2();
        pro.Name = 'Xenogenix Sample Product - Quantity';
        pro.ProductCode = 'X123';
        pro.CurrencyIsoCode = 'GBP';
        insert pro;
       
        PricebookEntry entry = new PricebookEntry();
        entry.Product2Id = pro.Id;
        entry.Pricebook2Id = Test.getStandardPricebookId();
        entry.UnitPrice = 100.00;
        entry.IsActive = true;
        insert entry;
        
        OpportunityLineItem oppProduct = new OpportunityLineItem();
        oppProduct.Product2Id = pro.Id;
        oppProduct.PricebookEntryId = entry.Id;
        oppProduct.OpportunityId=opp.Id;
        oppProduct.Quantity = 10;
        oppProduct.UnitPrice = 80.00;
        oppProduct.Cost__c=180.00;
        oppLineItemList.add(oppProduct);
        
        OpportunityLineItem oppProduct1 = new OpportunityLineItem();
        oppProduct1.Product2Id = pro.Id;
        oppProduct1.PricebookEntryId = entry.Id;
        oppProduct1.OpportunityId=opp.Id;
        oppProduct1.Quantity = 7;
        oppProduct1.UnitPrice = 50.00;
        oppProduct1.Cost__c=150.00;
        oppProduct1.Start_Date__c = Date.newInstance(2019, 10, 23);
        //oppLineItemList.add(oppProduct1);
        
        insert oppLineItemList;
        
        try {
            insert oppProduct1;
        } catch (Exception ex) {
            
        }
        
         Revenue_Item__c rev = new Revenue_Item__c();
            rev.Opportunity__c = opp.Id;
            rev.Product__c = pro.Id;
            rev.Amount__c = 7*50;
            rev.Quantity__c = 7;
            rev.Cost__c = 7*150;
            rev.Opportunity_Product_Id__c = oppProduct.Id;
            insert rev;

            delete oppProduct;

        List<Revenue_Item__c> revenueList = null;
        revenueList=[select Id,Opportunity_Product_Id__c,Quantity__c,Amount__c,Cost__c,Date__c,Product__c from Revenue_Item__c];
        
        //system.assertEquals(1, revenueList.size());
        test.stopTest();
        
    } 
    

}