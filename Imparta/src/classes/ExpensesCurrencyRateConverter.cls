public class ExpensesCurrencyRateConverter {

    public static void beforeInsert(List<project_cloud__Expense2__c> expenses) {
        
        List<CurrencyType> currencyTypes = [
            SELECT 
                ISOCode, ConversionRate
            FROM 
                CurrencyType 
            WHERE 
                IsActive = TRUE
        ];
        
        Map<String, CurrencyType> currenciesByISOCode = new Map<String, CurrencyType>();
        
        for (CurrencyType ct : currencyTypes) {
            currenciesByISOCode.put(ct.ISOCode, ct);
        }
        
        Map<Id, List<project_cloud__Expense2__c>> expensesByProject = new Map<Id, List<project_cloud__Expense2__c>>();
        
        for (project_cloud__Expense2__c expense : expenses) {
            
            if (expense.project_cloud__Project__c != null) {
                if (!expensesByProject.containsKey(expense.project_cloud__Project__c)) {
                    expensesByProject.put(expense.project_cloud__Project__c, new List<project_cloud__Expense2__c>{expense});
                } else {
                    expensesByProject.get(expense.project_cloud__Project__c).add(expense);
                }
            }
        }
        
        Map<Id, project_cloud__Project__c> projectsFromExpenses = new Map<Id, project_cloud__Project__c>([
            SELECT
                Id, CurrencyIsoCode
            FROM
                project_cloud__Project__c
            WHERE
                Id IN: expensesByProject.keySet()
        ]);
        
        List<project_cloud__Expense2__c> expensesToUpdate = new List<project_cloud__Expense2__c>();
        
        for (Id projectId : projectsFromExpenses.keySet()) {
            List<project_cloud__Expense2__c> expensesByProjectList = expensesByProject.get(projectId);
            
            for (project_cloud__Expense2__c expense : expensesByProjectList) {
                if (expense.CurrencyIsoCode != projectsFromExpenses.get(projectId).CurrencyIsoCode) {
                    expense.project_cloud__Amount__c = convertToParentCurrency(
                        expense.CurrencyIsoCode, projectsFromExpenses.get(projectId).CurrencyIsoCode,
                        expense.project_cloud__Amount__c, currenciesByISOCode);
                    
                    expense.CurrencyIsoCode = projectsFromExpenses.get(projectId).CurrencyIsoCode;
                    expensesToUpdate.add(expense);
                    System.debug('After convert rate: ' +  expense.project_cloud__Amount__c);
                }
            }
        }
    }
    
    private static Decimal convertToParentCurrency(String childCurrencyIsoCode, String parentCurrencyIsoCode,
        Decimal value, Map<String, CurrencyType> currenciesByISOCode) {
        
        System.debug('Child currency iso code: ' + childCurrencyIsoCode);
        System.debug('Parent currency iso code: ' + parentCurrencyIsoCode);
        System.debug('Before convert rate: ' + value);
        
        return value * currenciesByISOCode.get(parentCurrencyIsoCode).ConversionRate 
                / 
            currenciesByISOCode.get(childCurrencyIsoCode).ConversionRate;
    }
    
}