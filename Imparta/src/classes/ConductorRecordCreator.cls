public class ConductorRecordCreator{

    List<APXTConga4__Conga_Merge_Query__c> conductorQueries = new List<APXTConga4__Conga_Merge_Query__c>();
    List<APXT_BPM__Conductor__c> conductorRecords = new List<APXT_BPM__Conductor__c>();
    Map<String, Id> newQueryRecords = new Map<String, Id>();
    
    /* Default Constructor */
    public ConductorRecordCreator(){
        createConductorQueries();
        populateNewQueryRecordsMap();
        createConductorRecords();
    }

    private void createConductorQueries(){
        APXTConga4__Conga_Merge_Query__c salesInvoiceCNQuery = new APXTConga4__Conga_Merge_Query__c();
            salesInvoiceCNQuery.APXTConga4__Name__c = 'CCN – Invoices To Send';
            salesInvoiceCNQuery.APXTConga4__Query__c = 'SELECT Id FROM s2cor__Sage_INV_Trade_Document__c WHERE s2cor__Is_Email_Sent__c = FALSE AND s2cor__Status__c = \'SUBMITTED\' AND s2cor__Is_Paid__c = FALSE AND s2cor__Trade_Document_Type__r.s2cor__UID__c = \'SalesInvoice_TD\' ORDER BY s2cor__Posting_Date__c ASC LIMIT 1000';
            salesInvoiceCNQuery.APXTConga4__Description__c = 'Created for use with Conga for Sage Live.\n' + 'Specifically to be used with the \"Email Sales Invoices\" Conga Conductor Record.';
                conductorQueries.add(salesInvoiceCNQuery);
            
        APXTConga4__Conga_Merge_Query__c dlStatementsCNQuery = new APXTConga4__Conga_Merge_Query__c();
            dlStatementsCNQuery.APXTConga4__Name__c = 'CCN – Customer Statements To Send';
            dlStatementsCNQuery.APXTConga4__Query__c = 'SELECT Id FROM Account WHERE Id IN (SELECT s2cor__Account__c FROM s2cor__Sage_INV_Trade_Document__c WHERE s2cor__Is_Paid__c != TRUE AND s2cor__Status__c = \'Submitted\' AND s2cor__Trade_Document_Type__r.s2cor__UID__c = \'SalesInvoice_TD\') LIMIT 1000';
            dlStatementsCNQuery.APXTConga4__Description__c = 'Created for use with Conga for Sage Live.\n' + 'Specifically to be used with the \"Send Customer Statements\" Conga Conductor Record.';
                conductorQueries.add(dlStatementsCNQuery);

        APXTConga4__Conga_Merge_Query__c printChecksCNQuery = new APXTConga4__Conga_Merge_Query__c();
            printChecksCNQuery.APXTConga4__Name__c = 'CCN – Checks To Print';
            printChecksCNQuery.APXTConga4__Query__c = 'SELECT Id FROM s2cor__Sage_ACC_Journal__c WHERE s2cor__Status__c != \'Unsubmitted\' AND s2cor__Journal_Type__r.s2cor__UID__c = \'PaymentsbyCheque_JT\' AND Conga_Check_Printed__c != TRUE LIMIT 50';
            printChecksCNQuery.APXTConga4__Description__c = 'Created for use with Conga for Sage Live.\n' + 'Specifically to be used with the \"Print Checks\" Conga Conductor Record.';
                conductorQueries.add(printChecksCNQuery);   
                
        APXTConga4__Conga_Merge_Query__c paymentRemittanceCNQuery = new APXTConga4__Conga_Merge_Query__c();
            paymentRemittanceCNQuery.APXTConga4__Name__c = 'CCN – Payment Remittances To Send';
            paymentRemittanceCNQuery.APXTConga4__Query__c = 'SELECT Id FROM s2cor__Sage_ACC_Journal__c WHERE s2cor__Status__c != \'Unsubmitted\' AND Remittance_Sent__c = FALSE AND (s2cor__Journal_Type__r.s2cor__UID__c = \'PaymentsToSuppliers_JT\' OR s2cor__Journal_Type__r.s2cor__UID__c = \'PurchasePayments_JT\') AND s2cor__Date__c = LAST_N_Days:30 LIMIT 1000';
            paymentRemittanceCNQuery.APXTConga4__Description__c = 'Created for use with Conga for Sage Live.\n' + 'Specifically to be used with the \"Send Payment Remittances\" Conga Conductor Record.';
                conductorQueries.add(paymentRemittanceCNQuery);                                
                
        database.insert(conductorQueries);
            system.debug('************************* conductorQueries: ' + conductorQueries);
                                        
    }
    
    private void populateNewQueryRecordsMap(){
        for(APXTConga4__Conga_Merge_Query__c cmq : conductorQueries)
        {
            newQueryRecords.put(cmq.APXTConga4__Name__c, cmq.Id);                
        }
            system.debug('************************* newQueryRecords: ' + newQueryRecords);
    }
    
    private void createConductorRecords(){
        APXT_BPM__Conductor__c salesInvoicesCNRecord = new APXT_BPM__Conductor__c();
            salesInvoicesCNRecord.APXT_BPM__Title__c = 'Email Sales Invoices';
            salesInvoicesCNRecord.APXT_BPM__URL_Field_Name__c = 'Sales_Invoice_CN_URL__c';
            salesInvoicesCNRecord.APXT_BPM__Query_Id__c = newQueryRecords.get('CCN – Invoices To Send');
                conductorRecords.add(salesInvoicesCNRecord);
        
        APXT_BPM__Conductor__c downloadStatementsCNRecord = new APXT_BPM__Conductor__c();
            downloadStatementsCNRecord.APXT_BPM__Title__c = 'Send Customer Statements';
            downloadStatementsCNRecord.APXT_BPM__URL_Field_Name__c = 'CongaCN_Customer_Statement_CN_URL__c';
            downloadStatementsCNRecord.APXT_BPM__Query_Id__c = newQueryRecords.get('CCN – Customer Statements To Send');
                conductorRecords.add(downloadStatementsCNRecord);
                
        APXT_BPM__Conductor__c printChecksCNRecord = new APXT_BPM__Conductor__c();
            printChecksCNRecord.APXT_BPM__Title__c = 'Print Checks';
            printChecksCNRecord.APXT_BPM__URL_Field_Name__c = 'CongaCN_Print_Check_CN_URL__c';
            printChecksCNRecord.APXT_BPM__Query_Id__c = newQueryRecords.get('CCN – Checks To Print');
                conductorRecords.add(printChecksCNRecord);
                
        APXT_BPM__Conductor__c paymentRemittanceCNRecord = new APXT_BPM__Conductor__c();
            paymentRemittanceCNRecord.APXT_BPM__Title__c = 'Send Payment Remittances';
            paymentRemittanceCNRecord.APXT_BPM__URL_Field_Name__c = 'CongaCN_Conga_Payment_Remittance_CN_URL__c';
            paymentRemittanceCNRecord.APXT_BPM__Query_Id__c = newQueryRecords.get('CCN – Payment Remittances To Send');
                conductorRecords.add(paymentRemittanceCNRecord);                
    
        database.insert(conductorRecords);
            system.debug('************************* conductorRecords: ' + conductorRecords);        
    }
    
    
}