public class AllocationSummaryTriggerHandler {

    public static void handleAfterInsertOrUpdate(List<project_cloud__Allocation_Summary__c> newSummaries) {
        Set<Id> tasksIds = new Set<Id>();

        for (project_cloud__Allocation_Summary__c summary : newSummaries) {
            tasksIds.add(summary.project_cloud__Project_Task__c);
        }

        Map<Id, project_cloud__Project_Task__c> tasksToUpdateMap = new Map<Id, project_cloud__Project_Task__c>([
                SELECT
                    Id, CurrencyIsoCode
                FROM
                    project_cloud__Project_Task__c
                WHERE
                    Id IN :tasksIds
        ]);

        for (project_cloud__Allocation_Summary__c summary : newSummaries) {
            project_cloud__Project_Task__c task = tasksToUpdateMap.get(summary.project_cloud__Project_Task__c);

            task.Budgeted_Cost__c = MultiCurrencyService.convertCurrency(summary.Budgeted_Cost__c, summary.CurrencyIsoCode, task.CurrencyIsoCode);
            task.Calculated_Cost__c = MultiCurrencyService.convertCurrency(summary.Calculated_Cost__c, summary.CurrencyIsoCode, task.CurrencyIsoCode);
        }

        update tasksToUpdateMap.values();

        if (AllocationTriggerHandler.isExecutingInFuture || System.isBatch() || System.isFuture()) return;
        AllocationTriggerHandler.populateTasksAllocationsHoursField(tasksIds);
    }

    public static void handleBeforeDelete(List<project_cloud__Allocation_Summary__c> oldSummaries) {
        List<project_cloud__Project_Task__c> tasksToUpdate = new List<project_cloud__Project_Task__c>();

        for (project_cloud__Allocation_Summary__c summary : oldSummaries) {
            if(summary.project_cloud__Project_Task__c != null){
            project_cloud__Project_Task__c task = new project_cloud__Project_Task__c(
                    Id = summary.project_cloud__Project_Task__c, Budgeted_Cost__c = 0, Calculated_Cost__c = 0
            );
            tasksToUpdate.add(task);
            }
        }

        if(tasksToUpdate != null && tasksToUpdate.size() > 0)update tasksToUpdate;
    }

}