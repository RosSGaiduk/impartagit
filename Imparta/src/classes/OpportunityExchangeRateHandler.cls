public class OpportunityExchangeRateHandler {
   public static void calculateExchangeRate(List<Opportunity> opps) {
/**************************************************************************************
**** This Class calculates the Exchange Rate on an Opportunity and saves it in the ****
**** Opportunity_Exchange_Rate__c field on the Opportunity. It is used to calculate****
*** the Cost__c field on Opportunity Products                                      ****
***************************************************************************************/
system.debug('Start Exchange Rate Method');
try {

      // A list of the Currency codes to retrieve from the DatedConversionRates table
      List<String> CurrCodes = new List<String>();
      // A list of item numbers to process from the Opps[] collection
      List<Integer> TriggerItem = new List<Integer>();

      // Opps is a list of the 'Objects' that were updated
      // This loop iterates over the list, and adds any that have a
      // change in the 3 key fields
      for (Integer i = 0; i < Opps.size(); i++) {
        // Check to see if any of the following fields have changed:
        //  - AMOUNT field
        //  - DATE field (the date used for currency conversion)
        // If either has changed (of if this is a new record), remember
        // this record ID
           // Build a list of objects to be processed
           TriggerItem.add (i);
           CurrCodes.add( (string)Opps[i].CurrencyIsoCode);
      }
system.debug('currcodes = ' + currcodes);
      // To allow this to work for BULK operations (> 20 records), query all of
      // the conversion rates for the currencies that are used in the records being
      // processed. This acts as a Cache to avoid querying the table for each record
      // being inserted/updated. Loop through these in memory each time to find the
      // correct rate to use
      List<DatedConversionRate> ExchRateCache = [SELECT ISOCode, ConversionRate,
          StartDate, NextStartDate FROM DatedConversionRate
          WHERE ISOCode in :CurrCodes];
system.debug('ExchRateCache = ' + ExchRateCache );
      // For each record being inserted/updated
      for ( integer i : TriggerItem)
      {
         // Retrieve the proper DatedConversionRate based on the Date fields value
         // field. Change this field name to adjust to the appropriate Custom object.
         date dDateFieldValue = (date)Opps[i].CloseDate;
system.debug('dDateFieldValue = ' + dDateFieldValue);
         decimal nRate = -1;
         for (DatedConversionRate exchRate : ExchRateCache) {
            // Looping through the cached DatedConversionRate object
            if (exchRate.ISOCode == Opps[i].CurrencyIsoCode
            && exchRate.StartDate <= dDateFieldValue
            && exchRate.NextStartDate > dDateFieldValue) {
               // Look for a match for CURRENCYCODE and within the Date Ranges
               nRate = exchRate.ConversionRate;
               break;
            }
         }

         System.assertNotEquals(nRate, -1, 'A Rate was not found for the ' +
         dDateFieldValue + ' & ' + Opps[i].CurrencyIsoCode);
         /* System.Debug('Exchange Rate = ' + nRate); */

         // Update the field (BEFORE INSERT/UPDATE ONLY)
         Opps[i].Opportunity_Exchange_Rate__c = nRate;
system.debug('opps = ' + opps);
      }
  } catch (Exception e) {
      // Log any errors in the debug logs
      System.Debug('Error Detected...Logic Skipped: ' + e.getMessage()
         + '/' + e.getCause());
  }
system.debug('End Exchange Rate Method');
}

}