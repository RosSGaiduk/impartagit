@isTest
public class ProjectNavigationPathHandlerTest {
    private static testMethod void testProjectNav() {
        project_cloud__Project__c project = new project_cloud__Project__c(Name = 'Project Nav', project_cloud__Units__c ='Duration');
        insert project;
        project_cloud__Project_Phase__c phase = new project_cloud__Project_Phase__c(Name = 'Phase Nav', project_cloud__Project__c = project.Id);
        insert phase;
        project_cloud__Project_Task__c task = new project_cloud__Project_Task__c(Name = 'Task Nav', 
                                                                                 project_cloud__Project_Phase__c = phase.Id,
                                                                                 project_cloud__Estimated_Hours__c = 1, 
                                                                                 project_cloud__Autonomous_Start__c = Date.newInstance(2018, 4, 27),
                                                                                 project_cloud__Duration__c = 1);
        insert task;
        project_cloud__Ticket__c ticket = new project_cloud__Ticket__c(project_cloud__Ticket_Name__c = 'Ticket Nav', project_cloud__Project_CCPE_Ignore__c = project.Id);
        insert ticket;
        ProjectNavigationPathHandler.getProjectId(phase.Id);
        ProjectNavigationPathHandler.getProjectId(project.Id);
        ProjectNavigationPathHandler.getProjectId(task.Id);
        ProjectNavigationPathHandler.getProjectId(ticket.Id);
        String sObjName = ((Id)phase.Id).getSObjectType().getDescribe().getName();
        List<String> phaseIdSet = new List<String>();
        List<project_cloud__Project_Phase__c> phaseList = [SELECT Id FROM project_cloud__Project_Phase__c WHERE Id =: phase.Id];
        for (project_cloud__Project_Phase__c p : phaseList) {
            phaseIdSet.add(p.Id);
        }
        
        ProjectNavigationPathHandler.getId(sObjName, 'project_cloud__Project__c', phaseIdSet[0]);
    }
}