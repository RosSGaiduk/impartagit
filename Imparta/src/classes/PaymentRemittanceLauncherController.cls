public class PaymentRemittanceLauncherController{

    public Id TDDets {get; set;} 
    public Id RemitPayAppDets {get; set;}     
    public Id remittanceTemplateId {get; set;} 
    public Id remittanceEmailTemplateId {get; set;}     
    public Id Qvar0Id {get; set;} 
    public Id Qvar1Id {get; set;} 
    public Id Qvar2Id {get; set;}         
    private final s2cor__Sage_ACC_Journal__c journalRecord;

    /* Default Constructor */
    public PaymentRemittanceLauncherController(){
        findQueryIds();
        findRemittanceTemplateId();
        findRemittanceEmailTemplateId();
        findQvarRecords();
    }
    
    /* Standard Controller Constructor */
    public PaymentRemittanceLauncherController(ApexPages.StandardController stdController) {
        this.journalRecord = (s2cor__Sage_ACC_Journal__c)stdController.getRecord();
        findQueryIds();
        findRemittanceTemplateId();        
        findRemittanceEmailTemplateId();
        findQvarRecords();
    }

    private void findQueryIds(){
        LIST<APXTConga4__Conga_Merge_Query__c> remittanceQueries = [SELECT Id 
                                                            FROM APXTConga4__Conga_Merge_Query__c 
                                                            WHERE (APXTConga4__Name__c = 'Transaction Details for Payment Remittance [TDDets]' OR APXTConga4__Name__c = 'Remittance Payment Application Details [RemitPayAppDets]') AND APXTConga4__Description__c = 'Created By Solution Pack for Composer Solution: CPR (s2cor__Sage_ACC_Journal__c)' ORDER BY ID ASC LIMIT 2];        
        
        if(remittanceQueries.size() == 2)
        {
            TDDets = (Id) remittanceQueries.get(0).get('Id');
            RemitPayAppDets =  (Id) remittanceQueries.get(1).get('Id');
        }
        
        system.debug('********************** TDDets: ' + TDDets);      
        system.debug('********************** RemitPayAppDets: ' + RemitPayAppDets);      
    }

    private void findRemittanceTemplateId(){
        APXTConga4__Conga_Template__c remittancetTemplateRecord = [SELECT Id FROM APXTConga4__Conga_Template__c 
                                        WHERE APXTConga4__Template_Group__c = 'Remit' 
                                        AND APXTConga4__Description__c LIKE 'Created By Solution Pack for Composer Solution: CPR (s2cor__Sage_ACC_Journal__c)%' 
                                        LIMIT 1];
        
        remittanceTemplateId = remittancetTemplateRecord.Id;
            system.debug('********************** remittanceTemplateId: ' + remittanceTemplateId);   
    
    }
    
    private void findRemittanceEmailTemplateId(){
        APXTConga4__Conga_Email_Template__c remittanceEmailRecord = [SELECT Id FROM APXTConga4__Conga_Email_Template__c 
                                        WHERE APXTConga4__Template_Group__c = 'Remit' 
                                        AND APXTConga4__Description__c = 'Created By Solution Pack for Composer Solution: CPR (s2cor__Sage_ACC_Journal__c)' 
                                        LIMIT 1];
        
        remittanceEmailTemplateId = remittanceEmailRecord.Id;
            system.debug('********************** remittanceEmailTemplateId: ' + remittanceEmailTemplateId);   
    
    }

    private void findQvarRecords(){
        Map<String, Id> QvarMap = new Map<String, Id>();        
        
        for(APXTConga4__Conga_Merge_Query__c cq : [SELECT Id, Name, APXTConga4__Name__c, APXTConga4__Description__c 
                                                  FROM APXTConga4__Conga_Merge_Query__c 
                                                  WHERE APXTConga4__Name__c LIKE '%[QVAR%' 
                                                  LIMIT 25000])
        {
            system.debug('********************** cq.APXTConga4__Name__c: ' + cq.APXTConga4__Name__c);   
            system.debug('********************** cq.APXTConga4__Description__c: ' + cq.APXTConga4__Description__c);
            system.debug('********************** cq.APXTConga4__Name__c.contains(\'[QVAR\'): ' + cq.APXTConga4__Name__c.contains('[QVAR'));
            system.debug('********************** cq.APXTConga4__Description__c.contains(\'CPR (s2cor__Sage_ACC_Journal__c)\'): ' + cq.APXTConga4__Description__c.contains('CPR (s2cor__Sage_ACC_Journal__c)'));               
            
            if(cq.APXTConga4__Name__c.contains('[QVAR') && cq.APXTConga4__Description__c.contains('CPR (s2cor__Sage_ACC_Journal__c)'))
            {
                QvarMap.put(cq.APXTConga4__Name__c, cq.Id);             
            }    
        }
            system.debug('********************** QvarMap: ' + QvarMap);   
    
        Qvar0Id = QvarMap.get('[QVAR0]');
            system.debug('********************** Qvar0Id: ' + Qvar0Id);   
        Qvar1Id = QvarMap.get('[QVAR1]');
            system.debug('********************** Qvar1Id: ' + Qvar1Id);  
        Qvar2Id = QvarMap.get('[QVAR2]');
            system.debug('********************** Qvar2Id: ' + Qvar2Id);              

    }

}