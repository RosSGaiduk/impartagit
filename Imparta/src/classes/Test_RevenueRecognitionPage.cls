@isTest
private class Test_RevenueRecognitionPage {

    public testMethod static void methodNoRevenueItem() {
        Opportunity opp=new Opportunity(Name='Test Opp',StageName='Closed Won',CloseDate=Date.today(), Project_Start_Date__c = Date.today()+30);
        insert opp;

        Account acc=new Account(Name='Test Acc');
        insert acc;

        Product2 newProd = new Product2(Name = 'test product', family = 'test family');
        insert newProd;

        ID PB2Id = Test.getStandardPricebookId();

        PricebookEntry pbe = new PricebookEntry (Pricebook2Id=PB2Id, Product2Id=newProd.id, IsActive=true, UnitPrice=100.0);
        insert pbe;

        OpportunityLineItem oppLine = new OpportunityLineItem(PricebookEntryId=pbe.ID,Quantity = 2,OpportunityID = opp.Id,Revenue_Type__c='Milestones',Line_Number__c=1,Lead_Time_Weeks__c=2,UnitPrice=100,Cost__c = 50, Start_Date__c=Date.today(),Number_of_Revenue_Items__c=10);
        insert oppLine;

        

        PageReference pageRef = Page.RevenueRecognitionOpp;
        pageRef.getParameters().put('id', opp.ID);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(opp);
        Ctrl_RevenueRecognitionPage Ctrl=new Ctrl_RevenueRecognitionPage(sc);
        
        test.startTest();
        Ctrl.ChangeEditMode();
        Ctrl.ChangeEditMode();
        Ctrl.saveRevenueItems();
        Ctrl.RedirectToParent();
        test.stoptest();

    }

    public testMethod static void methodNewRevenueItem() {
        Opportunity opp=new Opportunity(Name='Test Opp',StageName='Closed Won',CloseDate=Date.today(), Project_Start_Date__c = Date.today()+30);
        insert opp;

        Account acc=new Account(Name='Test Acc');
        insert acc;

        Product2 newProd = new Product2(Name = 'test product', family = 'test family');
        insert newProd;

        ID PB2Id = Test.getStandardPricebookId();

        PricebookEntry pbe = new PricebookEntry (Pricebook2Id=PB2Id, Product2Id=newProd.id, IsActive=true, UnitPrice=100.0);
        insert pbe;

        OpportunityLineItem oppLine = new OpportunityLineItem(PricebookEntryId=pbe.ID,Quantity = 2,OpportunityID = opp.Id,Revenue_Type__c='Milestones',Line_Number__c=1,Lead_Time_Weeks__c=2,UnitPrice=100,Cost__c = 50, Start_Date__c=Date.today(),Number_of_Revenue_Items__c=10);
        insert oppLine;

        

        PageReference pageRef = Page.RevenueRecognitionOpp;
        pageRef.getParameters().put('id', opp.ID);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(opp);
        Ctrl_RevenueRecognitionPage Ctrl=new Ctrl_RevenueRecognitionPage(sc);
        
        test.startTest();
        Ctrl.ChangeEditMode();
        Ctrl.listRevenueRecog[0].listRevRecItems[0].Quantity__c =2;
        Ctrl.saveRevenueItems();
        
        Ctrl.ChangeEditMode();
        Ctrl.listRevenueRecog[0].listRevRecItems[0].Quantity__c =2;
        test.stoptest();

    }
   
   public testMethod static void methodDeleteRevenueItem() {
        Opportunity opp=new Opportunity(Name='Test Opp',StageName='Closed Won',CloseDate=Date.today(), Project_Start_Date__c = Date.today()+30);
        insert opp;

        Account acc=new Account(Name='Test Acc');
        insert acc;

        Product2 newProd = new Product2(Name = 'test product', family = 'test family');
        insert newProd;

        ID PB2Id = Test.getStandardPricebookId();

        PricebookEntry pbe = new PricebookEntry (Pricebook2Id=PB2Id, Product2Id=newProd.id, IsActive=true, UnitPrice=100.0);
        insert pbe;

        OpportunityLineItem oppLine = new OpportunityLineItem(PricebookEntryId=pbe.ID,Quantity = 2,OpportunityID = opp.Id,Revenue_Type__c='Milestones',Line_Number__c=1,Lead_Time_Weeks__c=2,UnitPrice=100,Cost__c = 50, Start_Date__c=Date.today(),Number_of_Revenue_Items__c=10);
        insert oppLine;

        

        PageReference pageRef = Page.RevenueRecognitionOpp;
        pageRef.getParameters().put('id', opp.ID);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(opp);
        Ctrl_RevenueRecognitionPage Ctrl=new Ctrl_RevenueRecognitionPage(sc);
        
        test.startTest();
        Ctrl.ChangeEditMode();
        Ctrl.listRevenueRecog[0].listRevRecItems[0].Quantity__c =1;
        Ctrl.listRevenueRecog[0].listRevRecItems[1].Quantity__c =1;
        Ctrl.saveRevenueItems();
        
        Ctrl.ChangeEditMode();
        Ctrl.listRevenueRecog[0].listRevRecItems[0].Quantity__c =0;
        Ctrl.listRevenueRecog[0].listRevRecItems[1].Quantity__c =2;
        Ctrl.saveRevenueItems();
        test.stoptest();

    }

    public testMethod static void methodMultipleProdRevenueItemWithSameOrder() {
        Opportunity opp=new Opportunity(Name='Test Opp',StageName='Closed Won',CloseDate=Date.today(), Project_Start_Date__c = Date.today()+30);
        insert opp;

        Account acc=new Account(Name='Test Acc');
        insert acc;

        Product2 newProd = new Product2(Name = 'test product', family = 'test family');
        insert newProd;

        Product2 newProd2 = new Product2(Name = 'test product2', family = 'test family');
        insert newProd2;

        ID PB2Id = Test.getStandardPricebookId();

        PricebookEntry pbe = new PricebookEntry (Pricebook2Id=PB2Id, Product2Id=newProd.id, IsActive=true, UnitPrice=100.0);
        insert pbe;

        PricebookEntry pbe2 = new PricebookEntry (Pricebook2Id=PB2Id, Product2Id=newProd2.id, IsActive=true, UnitPrice=200.0);
        insert pbe2;

        OpportunityLineItem oppLine = new OpportunityLineItem(PricebookEntryId=pbe.ID,Quantity = 2,OpportunityID = opp.Id,Revenue_Type__c='Milestones',Line_Number__c=1,Lead_Time_Weeks__c=2,UnitPrice=100,Cost__c = 50, Start_Date__c=Date.today(),Number_of_Revenue_Items__c=10);
        insert oppLine;

        OpportunityLineItem oppLine2 = new OpportunityLineItem(PricebookEntryId=pbe2.ID,Quantity = 2,OpportunityID = opp.Id,Revenue_Type__c='Milestones',Line_Number__c=1,Lead_Time_Weeks__c=2,UnitPrice=100,Cost__c = 50, Start_Date__c=Date.today(),Number_of_Revenue_Items__c=10);
        insert oppLine2;

        PageReference pageRef = Page.RevenueRecognitionOpp;
        pageRef.getParameters().put('id', opp.ID);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(opp);
        Ctrl_RevenueRecognitionPage Ctrl=new Ctrl_RevenueRecognitionPage(sc);
        
        test.startTest();
        Ctrl.ChangeEditMode();
        Ctrl.listRevenueRecog[0].listRevRecItems[0].Quantity__c =1;
        Ctrl.listRevenueRecog[0].listRevRecItems[1].Quantity__c =1;
        Ctrl.saveRevenueItems();
        
        Ctrl.ChangeEditMode();
        //Ctrl.listRevenueRecog[0].compareTo(Ctrl.listRevenueRecog[1]);
        Ctrl.listRevenueRecog[0].listRevRecItems[0].Quantity__c =0;
        Ctrl.listRevenueRecog[0].listRevRecItems[1].Quantity__c =2;
        Ctrl.saveRevenueItems();
        test.stoptest();

    }
    public testMethod static void methodMultipleProdRevenueItemWithDiffOrder() {
        Opportunity opp=new Opportunity(Name='Test Opp',StageName='Closed Won',CloseDate=Date.today(), Project_Start_Date__c = Date.today()+30);
        insert opp;

        Account acc=new Account(Name='Test Acc');
        insert acc;

        Product2 newProd = new Product2(Name = 'test product', family = 'test family',Sort_order__c=1);
        insert newProd;

        Product2 newProd2 = new Product2(Name = 'test product2', family = 'test family',Sort_order__c=2);
        insert newProd2;

        ID PB2Id = Test.getStandardPricebookId();

        PricebookEntry pbe = new PricebookEntry (Pricebook2Id=PB2Id, Product2Id=newProd.id, IsActive=true, UnitPrice=100.0);
        insert pbe;

        PricebookEntry pbe2 = new PricebookEntry (Pricebook2Id=PB2Id, Product2Id=newProd2.id, IsActive=true, UnitPrice=200.0);
        insert pbe2;

        OpportunityLineItem oppLine = new OpportunityLineItem(PricebookEntryId=pbe.ID,Quantity = 2,OpportunityID = opp.Id,Revenue_Type__c='Milestones',Line_Number__c=1,Lead_Time_Weeks__c=2,UnitPrice=100,Cost__c = 50, Start_Date__c=Date.today(),Number_of_Revenue_Items__c=10);
        insert oppLine;

        OpportunityLineItem oppLine2 = new OpportunityLineItem(PricebookEntryId=pbe2.ID,Quantity = 2,OpportunityID = opp.Id,Revenue_Type__c='Milestones',Line_Number__c=1,Lead_Time_Weeks__c=2,UnitPrice=100,Cost__c = 50, Start_Date__c=Date.today(),Number_of_Revenue_Items__c=10);
        insert oppLine2;

        PageReference pageRef = Page.RevenueRecognitionOpp;
        pageRef.getParameters().put('id', opp.ID);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(opp);
        Ctrl_RevenueRecognitionPage Ctrl=new Ctrl_RevenueRecognitionPage(sc);
        
        test.startTest();
        Ctrl.ChangeEditMode();
        Ctrl.listRevenueRecog[0].listRevRecItems[0].Quantity__c =1;
        Ctrl.listRevenueRecog[0].listRevRecItems[1].Quantity__c =1;
        Ctrl.saveRevenueItems();
        
        Ctrl.ChangeEditMode();
        //Ctrl.listRevenueRecog[0].compareTo(Ctrl.listRevenueRecog[1]);
        Ctrl.listRevenueRecog[0].listRevRecItems[0].Quantity__c =0;
        Ctrl.listRevenueRecog[0].listRevRecItems[1].Quantity__c =2;
        Ctrl.saveRevenueItems();
        test.stoptest();

    }

}