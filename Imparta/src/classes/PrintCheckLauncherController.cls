public class PrintCheckLauncherController{

    public Id PayToId {get; set;} 
    public Id BillDetsId {get; set;}     
    public Id printCheckTemplateId {get; set;} 
    public Id Qvar0Id {get; set;} 
    public Id Qvar1Id {get; set;}     
    private final s2cor__Sage_ACC_Journal__c journalRecord;

    /* Default Constructor */
    public PrintCheckLauncherController(){
        findQueryIds();
        findPRintCheckInvoiceTemplateId();
        findQvarRecords();
    }
    
    /* Standard Controller Constructor */
    public PrintCheckLauncherController(ApexPages.StandardController stdController) {
        this.journalRecord = (s2cor__Sage_ACC_Journal__c)stdController.getRecord();
        findQueryIds(); 
        findPRintCheckInvoiceTemplateId();
        findQvarRecords();     
    }
    
    private void findQueryIds(){
        List<APXTConga4__Conga_Merge_Query__c> payToQueryRecords = [SELECT Id 
                                                            FROM APXTConga4__Conga_Merge_Query__c 
                                                            WHERE (APXTConga4__Name__c = 'Check Print Pay To [PayTo]' OR APXTConga4__Name__c = 'Check Print Bill Details [BillsDets]') AND APXTConga4__Description__c = 'Created By Solution Pack for Composer Solution: CPC (s2cor__Sage_ACC_Journal__c)' 
                                                            ORDER BY APXTConga4__Name__c ASC 
                                                            LIMIT 2];        
        
        PayToId = (Id) payToQueryRecords.get(1).get('Id');
        BillDetsId = (Id) payToQueryRecords.get(0).get('Id');        
            system.debug('********************** PayToId: ' + PayToId);      
    }
    
    private void findPRintCheckInvoiceTemplateId(){
        APXTConga4__Conga_Template__c printCheckRecord = [SELECT Id FROM APXTConga4__Conga_Template__c 
                                        WHERE APXTConga4__Template_Group__c = 'Cheque' 
                                        AND APXTConga4__Description__c LIKE 'Created By Solution Pack for Composer Solution: CPC (s2cor__Sage_ACC_Journal__c)%' 
                                        LIMIT 1];
        
        printCheckTemplateId = printCheckRecord.Id;
            system.debug('********************** printCheckTemplateId: ' + printCheckTemplateId);   
    
    }
    
    private void findQvarRecords(){
        Map<String, Id> QvarMap = new Map<String, Id>();        
        
        for(APXTConga4__Conga_Merge_Query__c cq : [SELECT Id, Name, APXTConga4__Name__c, APXTConga4__Description__c 
                                                  FROM APXTConga4__Conga_Merge_Query__c 
                                                  WHERE APXTConga4__Name__c LIKE '%[QVAR%' 
                                                  ORDER BY APXTConga4__Name__c ASC 
                                                  LIMIT 25000])
        {
            system.debug('********************** cq.APXTConga4__Name__c: ' + cq.APXTConga4__Name__c);   
            system.debug('********************** cq.APXTConga4__Description__c: ' + cq.APXTConga4__Description__c);
            system.debug('********************** cq.APXTConga4__Name__c.contains(\'[QVAR\'): ' + cq.APXTConga4__Name__c.contains('[QVAR'));
            system.debug('********************** cq.APXTConga4__Description__c.contains(\'Composer Solution: CPC (s2cor__Sage_ACC_Journal__c)\'): ' + cq.APXTConga4__Description__c.contains('Composer Solution: CPC (s2cor__Sage_ACC_Journal__c)'));               
            
            if(cq.APXTConga4__Name__c.contains('[QVAR') && cq.APXTConga4__Description__c.contains('Composer Solution: CPC (s2cor__Sage_ACC_Journal__c)'))
            {
                QvarMap.put(cq.APXTConga4__Name__c, cq.Id);             
            }    
        }
            system.debug('********************** QvarMap: ' + QvarMap);   
    
        Qvar0Id = QvarMap.get('[QVAR0]');
        Qvar1Id = QvarMap.get('[QVAR1]');
            system.debug('********************** Qvar0Id: ' + Qvar0Id);   

    }
   

}