/*
		Name: opportunityTriggerHandler
		Discription: Trigger to handle updation of project start date which inturn impacts the dates of lineItem and revenue records
					and deleting relative revenue records if opp is a closed lost
		Created by: Lister Technologies
		Test Class: TestOpportunityTriggerHandler

	Change History
	   *********************************************************************************************************************************************
	   ModifiedBy        Date          Ticket No.      Requested By      Description                                                             Tag
	   *********************************************************************************************************************************************
*/
public class opportunityTriggerHandler {

	public static boolean avoidProductlineItemTrigger = false;
	public static void updatingRevenueRec(list<Opportunity> opportunityOld,Map<id,Opportunity> opportunityNew){

		set<id> oppId = new set<Id>();
		set<id> oppId1 = new set<Id>();
		List<Opportunity> oppRevenueRec = new List<Opportunity>();
		Map<String, list<Revenue_Item__c>> revenueListMap = new Map<String, list<Revenue_Item__c>>();
		Map<String, date[]> oppProdDateMap = new Map<String, date[]>();
		Map<Id, date> oppoldDateMap = new Map<Id, date>();
		List<Revenue_Item__c> revenueUpdatelist = new List<Revenue_Item__c>();
		List<Revenue_Item__c> revenueDeletelist = new List<Revenue_Item__c>();
		List<OpportunityLineItem> oppLineItemList = new List<OpportunityLineItem>();
		List<OpportunityLineItem> oppProductDateUpdatelist = new List<OpportunityLineItem>();
		String updateCheck = null;
		try{
			for(Opportunity oppOld :opportunityOld){

				if(oppOld.Project_Start_Date__c!=opportunityNew.get(oppOld.id).Project_Start_Date__c && opportunityNew.get(oppOld.id).Project_Start_Date__c!=null){
					updateCheck = 'DateChange';
					oppId.add(oppOld.Id);
					oppoldDateMap.put(oppOld.Id,oppOld.Project_Start_Date__c);
				}
				else if(opportunityNew.get(oppOld.Id).IsClosed && !opportunityNew.get(oppOld.Id).IsWon){
					oppId1.add(oppOld.Id);
					updateCheck = 'StageChange';
				}
			}
			system.debug('oppId.size()::'+oppId.size());

			if(updateCheck=='DateChange')  {
				if(oppId.size()!=0)
					oppRevenueRec =[select Id,Project_Start_Date__c,(select Id,Opportunity_Product_Id__c,Quantity__c,Amount__c,Cost__c,Date__c,
																	 Product__c from Revenue_Items__r) from Opportunity where id in :oppId];

				if(oppRevenueRec.size()!=0){
					for(Opportunity opp :oppRevenueRec){
						if(opp.Revenue_Items__r!=null && opp.Revenue_Items__r.size()!=0){
							for(Revenue_Item__c revenue : opp.Revenue_Items__r){
								if(revenueListMap.containsKey(revenue.Opportunity_Product_Id__c))
									revenueListMap.get(revenue.Opportunity_Product_Id__c).add(revenue);
								else
									revenueListMap.put(revenue.Opportunity_Product_Id__c, new list<Revenue_Item__c>{revenue});
								if(!oppProdDateMap.containsKey(revenue.Opportunity_Product_Id__c))
									oppProdDateMap.put(revenue.Opportunity_Product_Id__c, new date[]{opp.Project_Start_Date__c,oppoldDateMap.get(opp.Id)});
							}

						}
					}
					system.debug('revenueListMap.size()::'+revenueListMap.size());
					if(revenueListMap.size()!=0){
						for(String oppProdId : revenueListMap.keySet()){
							List<Revenue_Item__c> revenueListUpdate = updateRevenueDetailsChange(revenueListMap.get(oppProdId),
																								 checkDateDifference(oppProdDateMap.get(oppProdId)[1],oppProdDateMap.get(oppProdId)[0]));
							if(revenueListUpdate!=null)
								revenueUpdatelist.addAll(revenueListUpdate);
						}
					}

					oppLineItemList =[select Id,Start_Date__c from OpportunityLineItem where id in :oppProdDateMap.keySet()];

					if(oppLineItemList.size()!=0){
						for(OpportunityLineItem oppLineItem:oppLineItemList){
							if(oppProdDateMap.containsKey(oppLineItem.Id)){
								if(oppLineItem.Start_Date__c!=null){
								Date oppProdStartDate = oppLineItem.Start_Date__c;
								Integer daydiff = checkDateDifference(oppProdDateMap.get(oppLineItem.Id)[1],oppProdDateMap.get(oppLineItem.Id)[0]);
								oppLineItem.Start_Date__c = oppProdStartDate.addDays(daydiff);}
								oppProductDateUpdatelist.add(oppLineItem);}
						}
					}
				}
				if(revenueUpdatelist.size()>0)
					update revenueUpdatelist;
				if(oppProductDateUpdatelist.size()>0){
					avoidProductlineItemTrigger = true;
					update oppProductDateUpdatelist;
				}

			}
			else if(updateCheck=='StageChange'){
				if(oppId1.size()!=0)
					oppRevenueRec =[select Id,Project_Start_Date__c,(select Id,Opportunity_Product_Id__c,Quantity__c,Amount__c,Cost__c,Date__c,
																	 Product__c from Revenue_Items__r) from Opportunity where id in :oppId1];
				for(Opportunity opp :oppRevenueRec){
					if(opp.Revenue_Items__r!=null && opp.Revenue_Items__r.size()!=0){
						revenueDeletelist.addAll(opp.Revenue_Items__r);}
				}
				system.debug('revenueDeletelist.size::'+revenueDeletelist.size());
				if(revenueDeletelist.size()>0)
					delete revenueDeletelist;
			}
		}
		catch(DmlException e){
			system.debug('Unable to Update the Records' + e.getMessage());
		}
		catch(Exception e){
			system.debug('Improper Data Updation' + e.getMessage());
		}

	}

	public static list<Revenue_Item__c> updateRevenueDetailsChange(list<Revenue_Item__c> revenueList,Integer diffDays){
   try{
			List<Revenue_Item__c> startDateUpdateList = new List<Revenue_Item__c>();
			for(integer i=0;i<revenueList.size();i++){
				Revenue_Item__c revItem =  revenueList.get(i);
				if(revItem.Date__c!=null){
						if(diffDays!=0){date revenueDate = revItem.Date__c;
						revItem.Date__c=revenueDate.addDays(diffDays);
						startDateUpdateList.add(revItem);}
					}
			}
			return startDateUpdateList;
		}
		catch(Exception e){
			system.debug('Improper Data Updation' + e.getMessage());
			return null;
		}

	}

	 public static Integer checkDateDifference(Date oldDate,Date newDate){
		 Integer daysDiff = 0;
		 if(oldDate!=null)
			 daysDiff= oldDate.daysBetween(newDate);
		 return daysDiff;
	}

	public static void deleteRevenueRecords(list<Opportunity> OppList){

		try{
			List<Revenue_Item__c> revenueItemDeleteList = new List<Revenue_Item__c>();
			if(OppList.size()!=0){
				revenueItemDeleteList =[select Id from Revenue_Item__c where Opportunity__c in :OppList];
			}
			system.debug('revenueItemDeleteList = ' + revenueItemDeleteList);
			// deleting revenue list
			if(revenueItemDeleteList.size()>0)
				delete revenueItemDeleteList;
		}
		catch(DmlException e){
			system.debug('Unable to delete the Revenue Item Records' + e.getMessage());
		}
		catch(Exception e){
			system.debug('Improper Data Updation' + e.getMessage());
		}

	}

}