public class PaymentReceiptLauncherController{

    public Id PRMultiDetsId {get; set;} 
    public Id PayAppDetsId {get; set;}     
    public Id paymentReceiptTemplateId {get; set;} 
    public Id paymentReceiptEmailTemplateId {get; set;}     
    public Id Qvar0Id {get; set;} 
    public Id Qvar1Id {get; set;} 
    private final s2cor__Sage_ACC_Journal__c journalRecord;

    /* Default Constructor */
    public PaymentReceiptLauncherController(){
        findQueryIds();
        findPaymentReceiptInvoiceTemplateId();
        findPaymentReceiptEmailTemplateId();
        findQvarRecords();
    }
    
    /* Standard Controller Constructor */
    public PaymentReceiptLauncherController(ApexPages.StandardController stdController) {
        this.journalRecord = (s2cor__Sage_ACC_Journal__c)stdController.getRecord();
        findQueryIds();
        findPaymentReceiptInvoiceTemplateId();        
        findPaymentReceiptEmailTemplateId();
        findQvarRecords();
    }

    private void findQueryIds(){
        LIST<APXTConga4__Conga_Merge_Query__c> paymentReceiptQueries = [SELECT Id 
                                                            FROM APXTConga4__Conga_Merge_Query__c 
                                                            WHERE (APXTConga4__Name__c = 'Payment Receipt Multi-paid Invoice details [PRMultiDets]' OR APXTConga4__Name__c = 'Payment Application Details [PayAppDets]') AND APXTConga4__Description__c = 'Created By Solution Pack for Composer Solution: CRCPT (s2cor__Sage_ACC_Journal__c)' ORDER BY ID ASC LIMIT 2];        
        
        if(paymentReceiptQueries.size() == 2)
        {
            PRMultiDetsId = (Id) paymentReceiptQueries.get(0).get('Id');
            PayAppDetsId =  (Id) paymentReceiptQueries.get(1).get('Id');
        }
        
        system.debug('********************** PRMultiDetsId: ' + PRMultiDetsId);      
        system.debug('********************** PayAppDetsId: ' + PayAppDetsId);      
    }

    private void findPaymentReceiptInvoiceTemplateId(){
        APXTConga4__Conga_Template__c paymentReceiptRecord = [SELECT Id FROM APXTConga4__Conga_Template__c 
                                        WHERE APXTConga4__Template_Group__c = 'PRM' 
                                        AND APXTConga4__Description__c LIKE 'Created By Solution Pack for Composer Solution: CRCPT (s2cor__Sage_ACC_Journal__c)%' 
                                        LIMIT 1];
        
        paymentReceiptTemplateId = paymentReceiptRecord.Id;
            system.debug('********************** paymentReceiptTemplateId: ' + paymentReceiptTemplateId);   
    
    }
    
    private void findPaymentReceiptEmailTemplateId(){
        APXTConga4__Conga_Email_Template__c paymentReceiptEmailRecord = [SELECT Id FROM APXTConga4__Conga_Email_Template__c 
                                        WHERE APXTConga4__Template_Group__c = 'PRM' 
                                        AND APXTConga4__Description__c = 'Created By Solution Pack for Composer Solution: CRCPT (s2cor__Sage_ACC_Journal__c)' 
                                        LIMIT 1];
        
        paymentReceiptEmailTemplateId = paymentReceiptEmailRecord.Id;
            system.debug('********************** paymentReceiptEmailTemplateId: ' + paymentReceiptEmailTemplateId);   
    
    }

    private void findQvarRecords(){
        Map<String, Id> QvarMap = new Map<String, Id>();        
        
        for(APXTConga4__Conga_Merge_Query__c cq : [SELECT Id, Name, APXTConga4__Name__c, APXTConga4__Description__c 
                                                  FROM APXTConga4__Conga_Merge_Query__c 
                                                  WHERE APXTConga4__Name__c LIKE '%[QVAR%' 
                                                  LIMIT 25000])
        {
            system.debug('********************** cq.APXTConga4__Name__c: ' + cq.APXTConga4__Name__c);   
            system.debug('********************** cq.APXTConga4__Description__c: ' + cq.APXTConga4__Description__c);
            system.debug('********************** cq.APXTConga4__Name__c.contains(\'[QVAR\'): ' + cq.APXTConga4__Name__c.contains('[QVAR'));
            system.debug('********************** cq.APXTConga4__Description__c.contains(\'CRCPT (s2cor__Sage_ACC_Journal__c)\'): ' + cq.APXTConga4__Description__c.contains('CRCPT (s2cor__Sage_ACC_Journal__c)'));               
            
            if(cq.APXTConga4__Name__c.contains('[QVAR') && cq.APXTConga4__Description__c.contains('CRCPT (s2cor__Sage_ACC_Journal__c)'))
            {
                QvarMap.put(cq.APXTConga4__Name__c, cq.Id);             
            }    
        }
            system.debug('********************** QvarMap: ' + QvarMap);   
    
        Qvar0Id = QvarMap.get('[QVAR0]');
            system.debug('********************** Qvar0Id: ' + Qvar0Id);     
        Qvar1Id = QvarMap.get('[QVAR1]');
            system.debug('********************** Qvar1Id: ' + Qvar1Id);               

    }

}