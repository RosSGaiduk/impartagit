public class ProjectTaskTriggerHandler {

    public static final Set<String> syncFields = new Set<String>{'Actual_Activity__c', 'Service__c', 'Service_Price__c',
        'project_cloud__Estimated_Hours__c', 'Delegate_Fee_Product__c', 'Delegate_Fee__c', 'Number_of_Delegates__c',
        'Licence_Fee_Product__c', 'Licence_Fee__c', 'Number_of_Licences__c'
    };
    
    public static Boolean isProjectSync = false;    
        
    /*public static void handleBeforeInsert(List<project_cloud__Project_Task__c> newTasks) {
        Set<Id> workTypesIds = new Set<Id>();

        for (project_cloud__Project_Task__c task : newTasks) {
            if (task.project_cloud__Work_Type__c != null) {
                workTypesIds.add(task.project_cloud__Work_Type__c);
            }
        }

        Map<Id, project_cloud__Work_Type__c> workTypesMap = getWorkTypesMapByIds(workTypesIds);

        for (project_cloud__Project_Task__c task : newTasks) {
            if (task.project_cloud__Work_Type__c == null || task.project_cloud__Estimated_Hours__c == null) {
                continue;
            }

            project_cloud__Work_Type__c workType = workTypesMap.get(task.project_cloud__Work_Type__c);

            if (workType.ccpe_r__Default_Skill_Cost__c != null) {
                task.Default_Cost__c = task.project_cloud__Estimated_Hours__c / 8 * workType.ccpe_r__Default_Skill_Cost__c;
                task.Budgeted_Cost__c = task.project_cloud__Estimated_Hours__c / 8 * workType.ccpe_r__Default_Skill_Cost__c;
            }
        }
    }*/

    /*public static void handleBeforeUpdate(List<project_cloud__Project_Task__c> oldTasks, Map<Id, project_cloud__Project_Task__c> newTasksMap) {
        Set<Id> workTypesIds = new Set<Id>();

        for (project_cloud__Project_Task__c oldTask : oldTasks) {
            project_cloud__Project_Task__c newTask = newTasksMap.get(oldTask.Id);

            if (newTask.project_cloud__Work_Type__c != null && oldTask.project_cloud__Work_Type__c != newTask.project_cloud__Work_Type__c) {
                workTypesIds.add(newTask.project_cloud__Work_Type__c);
            }
        }

        Map<Id, project_cloud__Work_Type__c> workTypesMap = getWorkTypesMapByIds(workTypesIds);

        for (project_cloud__Project_Task__c oldTask : oldTasks) {
            project_cloud__Project_Task__c newTask = newTasksMap.get(oldTask.Id);

            if (newTask.project_cloud__Work_Type__c == null || oldTask.project_cloud__Work_Type__c == newTask.project_cloud__Work_Type__c
                    || newTask.project_cloud__Estimated_Hours__c == null) {
                continue;
            }

            project_cloud__Work_Type__c workType = workTypesMap.get(newTask.project_cloud__Work_Type__c);

            if (workType.ccpe_r__Default_Skill_Cost__c != null) {
                newTask.Default_Cost__c = newTask.project_cloud__Estimated_Hours__c / 8 * workType.ccpe_r__Default_Skill_Cost__c;
                newTask.Budgeted_Cost__c = newTask.project_cloud__Estimated_Hours__c / 8 * workType.ccpe_r__Default_Skill_Cost__c;
            }
        }
    }*/

    private static Map<Id, project_cloud__Work_Type__c> getWorkTypesMapByIds(Set<Id> workTypesIds) {
        Map<Id, project_cloud__Work_Type__c> workTypesMap = new Map<Id, project_cloud__Work_Type__c>([
                SELECT
                    Id, ccpe_r__Default_Skill_Cost__c
                FROM
                    project_cloud__Work_Type__c
                WHERE
                    Id IN :workTypesIds
        ]);

        return workTypesMap;
    }

    public static void handleAfterInsert(List<project_cloud__Project_Task__c> newTasks) {
        Map<Id, project_cloud__Project_Phase__c> phasesMap = getPhasesMap(newTasks);

        Map<Id, project_cloud__Project_Task__c> taskMap = new Map<Id, project_cloud__Project_Task__c>([
            SELECT
                Id,
                (SELECT Id, Product__c FROM Revenue_Items__r)
            FROM
                project_cloud__Project_Task__c
            WHERE
                Id IN :newTasks
        ]);

        List<Revenue_Item__c> revenueItemsToInsert = new List<Revenue_Item__c>();
        for (project_cloud__Project_Task__c task : newTasks) {
            if (task.Billable__c != true || taskMap.get(task.Id).Revenue_Items__r.size() > 0) continue;

            project_cloud__Project_Phase__c currentPhase = phasesMap.get(task.project_cloud__Project_Phase__c);
            
            if (task.Service__c != null && task.Revenue_Item_Id__c == null) {
                Revenue_Item__c revenueItem = createServiceRevenueItem(task, currentPhase.project_cloud__Project__c);
                revenueItem.Account__c = currentPhase.project_cloud__Project__r.ccpe_ocp__Account__c;
                if (currentPhase.project_cloud__Project__r.ccpe_ocp__Opportunity__c != null) {
                    if (currentPhase.project_cloud__Project__r.ccpe_ocp__Opportunity__r.Win_Probability__c != null) {
                        revenueItem.Revenue_Probability__c = currentPhase.project_cloud__Project__r.ccpe_ocp__Opportunity__r.Win_Probability__c;
                    } else {
                        revenueItem.Revenue_Probability__c = '0%';
                    }
                } else {
                    revenueItem.Revenue_Probability__c = '0%';
                }
                revenueItemsToInsert.add(revenueItem);
            }
            if (task.Delegate_Fee_Product__c != null && task.Delegate_Revenue_Item_Id__c == null) {
                Revenue_Item__c revenueItem = createDelegateRevenueItem(task, currentPhase.project_cloud__Project__c);
                revenueItem.Account__c = currentPhase.project_cloud__Project__r.ccpe_ocp__Account__c;
                if (currentPhase.project_cloud__Project__r.ccpe_ocp__Opportunity__c != null) {
                    if (currentPhase.project_cloud__Project__r.ccpe_ocp__Opportunity__r.Win_Probability__c != null) {
                        revenueItem.Revenue_Probability__c = currentPhase.project_cloud__Project__r.ccpe_ocp__Opportunity__r.Win_Probability__c;
                    } else {
                        revenueItem.Revenue_Probability__c = '0%';
                    }
                } else {
                    revenueItem.Revenue_Probability__c = '0%';
                }
                revenueItemsToInsert.add(revenueItem);
            }
            if (task.Licence_Fee_Product__c != null && task.Licence_Revenue_Item_Id__c == null) {
                Revenue_Item__c revenueItem = createLicenseRevenueItem(task, currentPhase.project_cloud__Project__c);
                revenueItem.Account__c = currentPhase.project_cloud__Project__r.ccpe_ocp__Account__c;
                if (currentPhase.project_cloud__Project__r.ccpe_ocp__Opportunity__c != null) {
                    if (currentPhase.project_cloud__Project__r.ccpe_ocp__Opportunity__r.Win_Probability__c != null) {
                        revenueItem.Revenue_Probability__c = currentPhase.project_cloud__Project__r.ccpe_ocp__Opportunity__r.Win_Probability__c;
                    } else {
                        revenueItem.Revenue_Probability__c = '0%';
                    }
                } else {
                    revenueItem.Revenue_Probability__c = '0%';
                }
                revenueItemsToInsert.add(revenueItem);
            }
        }

        insert revenueItemsToInsert;System.debug(JSON.serialize(revenueItemsToInsert));
        
        //validateTasksAfterInsertOrUpdate(newTasks);
    }

    public static void handleAfterUpdate(List<project_cloud__Project_Task__c> oldTasks) {
        Map<Id, project_cloud__Project_Task__c> newTasksMap = new Map<Id, project_cloud__Project_Task__c>([
            SELECT
                Id, project_cloud__End__c, ccpe_r__Cost__c, project_cloud__Project_Phase__c, Billable__c,
                Service__c, project_cloud__Estimated_Hours__c, Total_Professional_Fees__c,
                Delegate_Fee_Product__c, Delegate_Fee__c, Number_of_Delegates__c,
                Licence_Fee_Product__c, Licence_Fee__c, Number_of_Licences__c, Reported_Cost__c,
                Revenue_Item_Id__c, Delegate_Revenue_Item_Id__c, Licence_Revenue_Item_Id__c,
	            project_cloud__Project_Phase__r.project_cloud__Project__r.ccpe_ocp__Opportunity__c,
            	project_cloud__Project_Phase__r.project_cloud__Project__r.ccpe_ocp__Opportunity__r.Win_Probability__c,
                (SELECT Id, Product__c FROM Revenue_Items__r)
            FROM
                project_cloud__Project_Task__c
            WHERE
                Id IN :oldTasks
        ]);System.debug(JSON.serialize(newTasksMap));

        Map<Id, project_cloud__Project_Phase__c> phasesMap = getPhasesMap(newTasksMap.values());

        List<Revenue_Item__c> revenueItemsToDelete = new List<Revenue_Item__c>();
        List<Revenue_Item__c> revenueItemsToUpsert = new List<Revenue_Item__c>();
        
        Set<Id> projectIds = new Set<Id>();
        
        Map<Id, Revenue_Item__c> revenueItemsMap = new Map<Id, Revenue_Item__c>();
        for (project_cloud__Project_Task__c task : newTasksMap.values()) {
            if (task.Revenue_Item_Id__c != null) {
                revenueItemsMap.put(task.Revenue_Item_Id__c, null);
            }
            
            if (task.Delegate_Revenue_Item_Id__c != null) {
                revenueItemsMap.put(task.Delegate_Revenue_Item_Id__c, null);
            }
            
            if (task.Licence_Revenue_Item_Id__c != null) {
                revenueItemsMap.put(task.Licence_Revenue_Item_Id__c, null);
            }
        }
        
        revenueItemsMap = new Map<Id, Revenue_Item__c>([
            SELECT
                Id
            FROM
                Revenue_Item__c
            WHERE
                Id IN: revenueItemsMap.keySet()
        ]);
        
        for (project_cloud__Project_Task__c oldTask : oldTasks) {
            project_cloud__Project_Task__c newTask = newTasksMap.get(oldTask.Id);

            if (newTask.Billable__c == false) {
                if (oldTask.Billable__c == true) {
                    for (Revenue_Item__c ri : newTask.Revenue_Items__r) {
                        revenueItemsToDelete.add(ri);
                    }
                }
                continue;
            }

            Id projectId = phasesMap.get(newTask.project_cloud__Project_Phase__c).project_cloud__Project__c;
            projectIds.add(projectId);
            // handle service product
            if (oldTask.Service__c != null && newTask.Service__c == null) {
                Revenue_Item__c revenueItem = findRevenueItemByProductId(newTask.Revenue_Items__r, oldTask.Service__c);
                if (revenueItem != null) {
                    revenueItemsToDelete.add(revenueItem);
                }
            } else if (oldTask.Service__c == null && newTask.Service__c != null) {
                Revenue_Item__c revenueItem = findRevenueItemByProductId(newTask.Revenue_Items__r, newTask.Service__c);
                if (revenueItem == null) {
                	revenueItemsToUpsert.add(createServiceRevenueItem(newTask, projectId));
                }
            } else if (oldTask.Service__c != null && newTask.Service__c != null) {
                Revenue_Item__c revenueItem = findRevenueItemByProductId(newTask.Revenue_Items__r, oldTask.Service__c);
                if (revenueItem == null) {
                    revenueItem = revenueItemsMap.get(newTask.Revenue_Item_Id__c);
                }
                if (revenueItem == null && newTask.Revenue_Item_Id__c == null) {
                    revenueItemsToUpsert.add(createServiceRevenueItem(newTask, projectId));
                } else if (oldTask.Service__c != newTask.Service__c) {
                    Revenue_Item__c updatedRevenueItem = createServiceRevenueItem(newTask, projectId);
                    updatedRevenueItem.Id = revenueItem.Id;
                    revenueItemsToUpsert.add(updatedRevenueItem);
                    //revenueItemsToDelete.add(revenueItem);
                    //revenueItemsToUpsert.add(createServiceRevenueItem(newTask, projectId));
                } else {
                    if (revenueItem != null) {
                        Boolean isTaskChanged = false;
                        
                        if (oldTask.project_cloud__End__c != newTask.project_cloud__End__c) {
                            revenueItem.Date__c = newTask.project_cloud__End__c.addMonths(1).toStartOfMonth().addDays(-1);
                            isTaskChanged = true;
                        }
                        if (oldTask.Reported_Cost__c != newTask.Reported_Cost__c) {
                            revenueItem.Cost__c = newTask.Reported_Cost__c;
                            isTaskChanged = true;
                        }
                        if (oldTask.project_cloud__Estimated_Hours__c != newTask.project_cloud__Estimated_Hours__c) {
                            revenueItem.Quantity__c = newTask.project_cloud__Estimated_Hours__c / 8;
                            isTaskChanged = true;
                        }
                        if (oldTask.Total_Professional_Fees__c != newTask.Total_Professional_Fees__c) {
                            System.debug('Revenue item: '+JSON.serialize(revenueItem));
                            System.debug('Old task: '+JSON.serialize(oldTask));
                            System.debug('New task: '+JSON.serialize(newTask));
                            
                            revenueItem.Amount__c = newTask.Total_Professional_Fees__c;
                            isTaskChanged = true;
                        }
    
                        if (isTaskChanged) {
                            revenueItemsToUpsert.add(revenueItem);
                        }
                    }
                }
            }

            // handle delegate product
            if (oldTask.Delegate_Fee_Product__c != null && newTask.Delegate_Fee_Product__c == null) {
                Revenue_Item__c revenueItem = findRevenueItemByProductId(newTask.Revenue_Items__r, oldTask.Delegate_Fee_Product__c);
                if (revenueItem != null) {
                    revenueItemsToDelete.add(revenueItem);
                }
            } else if (oldTask.Delegate_Fee_Product__c == null && newTask.Delegate_Fee_Product__c != null) {
                Revenue_Item__c revenueItem = findRevenueItemByProductId(newTask.Revenue_Items__r, newTask.Delegate_Fee_Product__c);
                if (revenueItem == null) {
                	revenueItemsToUpsert.add(createDelegateRevenueItem(newTask, projectId));
                }
            } else if (oldTask.Delegate_Fee_Product__c != null && newTask.Delegate_Fee_Product__c != null) {
                Revenue_Item__c revenueItem = findRevenueItemByProductId(newTask.Revenue_Items__r, oldTask.Delegate_Fee_Product__c);
                if (revenueItem == null) {
                    revenueItem = revenueItemsMap.get(newTask.Delegate_Revenue_Item_Id__c);
                }
                System.debug('Rev item data: ' + JSON.serialize(revenueItem));
                System.debug('newTask.Revenue_Items__r: ' + JSON.serialize(newTask.Revenue_Items__r));
                
                if (revenueItem == null && newTask.Delegate_Revenue_Item_Id__c == null) {
                    revenueItemsToUpsert.add(createDelegateRevenueItem(newTask, projectId));
                } else if (oldTask.Delegate_Fee_Product__c != newTask.Delegate_Fee_Product__c) {
                    Revenue_Item__c updatedRevenueItem = createDelegateRevenueItem(newTask, projectId);
                    updatedRevenueItem.Id = revenueItem.Id;
                    revenueItemsToUpsert.add(updatedRevenueItem);
                    //revenueItemsToDelete.add(revenueItem);
                    //revenueItemsToUpsert.add(createDelegateRevenueItem(newTask, projectId));
                } else {
                    Boolean isTaskChanged = false;

                    if (oldTask.project_cloud__End__c != newTask.project_cloud__End__c) {
                        revenueItem.Date__c = newTask.project_cloud__End__c.addMonths(1).toStartOfMonth().addDays(-1);
                        isTaskChanged = true;
                    }
                    if (oldTask.Reported_Cost__c != newTask.Reported_Cost__c) {
                        revenueItem.Cost__c = newTask.Reported_Cost__c;
                        isTaskChanged = true;
                    }
                    if (oldTask.Number_of_Delegates__c != newTask.Number_of_Delegates__c || oldTask.Delegate_Fee__c != newTask.Delegate_Fee__c) {
                        System.debug('oldTask df: ' + JSON.serialize(oldTask.Delegate_Fee__c));
                        System.debug('newTask df: ' + JSON.serialize(newTask.Delegate_Fee__c));
                        System.debug('revenueItem: ' + JSON.serialize(revenueItem));
                        System.debug('newTask: ' + JSON.serialize(newTask));
                        revenueItem.Quantity__c = newTask.Number_of_Delegates__c;
                        revenueItem.Amount__c = newTask.Number_of_Delegates__c * newTask.Delegate_Fee__c;
                        isTaskChanged = true;
                    }

                    if (isTaskChanged) {
                        revenueItemsToUpsert.add(revenueItem);
                    }
                }
            }

            // handle license product
            if (oldTask.Licence_Fee_Product__c != null && newTask.Licence_Fee_Product__c == null) {
                Revenue_Item__c revenueItem = findRevenueItemByProductId(newTask.Revenue_Items__r, oldTask.Licence_Fee_Product__c);
                if (revenueItem != null) {
                    revenueItemsToDelete.add(revenueItem);
                }
            } else if (oldTask.Licence_Fee_Product__c == null && newTask.Licence_Fee_Product__c != null) {
                Revenue_Item__c revenueItem = findRevenueItemByProductId(newTask.Revenue_Items__r, newTask.Licence_Fee_Product__c);
                if (revenueItem == null) {
                	revenueItemsToUpsert.add(createLicenseRevenueItem(newTask, projectId));
                }
            } else if (oldTask.Licence_Fee_Product__c != null && newTask.Licence_Fee_Product__c != null) {
                Revenue_Item__c revenueItem = findRevenueItemByProductId(newTask.Revenue_Items__r, oldTask.Licence_Fee_Product__c);
                if (revenueItem == null) {
                    revenueItem = revenueItemsMap.get(newTask.Licence_Revenue_Item_Id__c);
                }
                if (revenueItem == null && newTask.Licence_Revenue_Item_Id__c == null) {
                    revenueItemsToUpsert.add(createLicenseRevenueItem(newTask, projectId));
                } else if (oldTask.Licence_Fee_Product__c != newTask.Licence_Fee_Product__c) {
                    Revenue_Item__c updatedRevenueItem = createLicenseRevenueItem(newTask, projectId);
                    updatedRevenueItem.Id = revenueItem.Id;
                    revenueItemsToUpsert.add(updatedRevenueItem);
                    //revenueItemsToDelete.add(revenueItem);
                    //revenueItemsToUpsert.add(createLicenseRevenueItem(newTask, projectId));
                } else {
                    Boolean isTaskChanged = false;

                    if (oldTask.project_cloud__End__c != newTask.project_cloud__End__c) {
                        revenueItem.Date__c = newTask.project_cloud__End__c.addMonths(1).toStartOfMonth().addDays(-1);
                        isTaskChanged = true;
                    }
                    if (oldTask.Reported_Cost__c != newTask.Reported_Cost__c) {
                        revenueItem.Cost__c = newTask.Reported_Cost__c;
                        isTaskChanged = true;
                    }
                    if (oldTask.Number_of_Licences__c != newTask.Number_of_Licences__c || oldTask.Licence_Fee__c != newTask.Licence_Fee__c) {
                        revenueItem.Quantity__c = newTask.Number_of_Licences__c;
                        revenueItem.Amount__c = newTask.Number_of_Licences__c * newTask.Licence_Fee__c;
                        isTaskChanged = true;
                    }

                    if (isTaskChanged) {
                        revenueItemsToUpsert.add(revenueItem);
                    }
                }
            }
        }

        delete revenueItemsToDelete;
        upsert revenueItemsToUpsert;System.debug(JSON.serialize(revenueItemsToUpsert));

        Map<Id, project_cloud__Project__c> projects = new Map<Id, project_cloud__Project__c>([
            SELECT
                Id, ccpe_ocp__Account__c
            FROM
                project_cloud__Project__c
            WHERE
                Id in: projectIds
        ]);
        
        List<Revenue_Item__c> revenueItems = [
            SELECT
                Id, Project__c
            FROM
                Revenue_Item__c
            WHERE
                Id IN: revenueItemsToUpsert
        ];
        
        for (Revenue_Item__c item : revenueItemsToUpsert) {
            if (projects.containsKey(item.Project__c)) {
                item.Account__c = projects.get(item.Project__c).ccpe_ocp__Account__c;
            }
        }
        
        update revenueItemsToUpsert;
        
        if (AllocationTriggerHandler.isExecutingInFuture || System.isBatch() || System.isFuture()) return;
        AllocationTriggerHandler.populateTasksAllocationsHoursField(newTasksMap.keySet());
    }
    
    /*private static List<project_cloud__Project_Task__c> gatherProjectTasks(List<project_cloud__Project_Task__c> newTasks) {
         Set<Id> projectIds = new Set<Id>();
        List<project_cloud__Project_Task__c> tasks = [
            SELECT
                Id, project_cloud__Project_Phase__r.project_cloud__Project__c, Service__c, Service_Price__c,
                Delegate_Fee_Product__c, Delegate_Fee__c, Licence_Fee_Product__c, Licence_Fee__c
            FROM
                project_cloud__Project_Task__c
            WHERE
                Id IN: newTasks
        ];
        
        for (project_cloud__Project_Task__c task : tasks) {
            projectIds.add(task.project_cloud__Project_Phase__r.project_cloud__Project__c);
        }
        
        return [
            SELECT
                Id, project_cloud__Project_Phase__r.project_cloud__Project__c, Service__c, Service_Price__c,
                Delegate_Fee_Product__c, Delegate_Fee__c, Licence_Fee_Product__c, Licence_Fee__c
            FROM
                project_cloud__Project_Task__c
            WHERE
                project_cloud__Project_Phase__r.project_cloud__Project__c IN: projectIds
        ];
    }*/
    
    
    /*public static void validateTasksAfterInsertOrUpdate(List<project_cloud__Project_Task__c> newTasks) {
        System.debug('Validate records ' + newTasks.size());
        System.debug('Tasks: ' + JSOn.serialize(newTasks));
        //ProjectId -> ['Service', 'Delegate', 'License'] -> 'ProductId' -> Price
        Set<Id> projectIds = new Set<Id>();
        List<project_cloud__Project_Task__c> tasks = gatherProjectTasks(newTasks);
        validateTasks(tasks);
    }
    
    private static void validateTasks(List<project_cloud__Project_Task__c> tasks) {
        Map<Id, Map<String, Map<Id, Decimal>>> pricesOfProductsInScopeOfTheProject = new Map<Id, Map<String, Map<Id, Decimal>>>();
        for (project_cloud__Project_Task__c projectTask : tasks) {
            Id projectId = projectTask.project_cloud__Project_Phase__r.project_cloud__Project__c;
            
            if (!pricesOfProductsInScopeOfTheProject.containsKey(projectId)) {
                pricesOfProductsInScopeOfTheProject.put(projectId, new Map<String, Map<Id, Decimal>>{
                    'Service' => new Map<Id, Decimal>(),
                    'Delegate' => new Map<Id, Decimal>(),
                    'License' => new Map<Id, Decimal>()
                });
            }
            
            if (projectTask.Service__c != null) {
                if (pricesOfProductsInScopeOfTheProject.get(projectId).get('Service').containsKey(projectTask.Service__c)) {
                    if (pricesOfProductsInScopeOfTheProject.get(projectId).get('Service').get(projectTask.Service__c) != projectTask.Service_Price__c) {
                        throw new TaskValidationException('There is another task for this project with the same Service product, but with different Service Price');
                    }
                } else {
                    pricesOfProductsInScopeOfTheProject.get(projectId).get('Service').put(projectTask.Service__c, projectTask.Service_Price__c);
                }
            }
            
            if (projectTask.Delegate_Fee_Product__c != null) {
                if (pricesOfProductsInScopeOfTheProject.get(projectId).get('Delegate').containsKey(projectTask.Delegate_Fee_Product__c)) {
                    if (pricesOfProductsInScopeOfTheProject.get(projectId).get('Delegate').get(projectTask.Delegate_Fee_Product__c) != projectTask.Delegate_Fee__c) {
                        throw new TaskValidationException('There is another task for this project with the same Delegate Fee product, but with different Delegate Fee Price');
                    }
                } else {
                    pricesOfProductsInScopeOfTheProject.get(projectId).get('Delegate').put(projectTask.Delegate_Fee_Product__c, projectTask.Delegate_Fee__c);
                }
            }
            
            if (projectTask.Licence_Fee_Product__c != null) {
                if (pricesOfProductsInScopeOfTheProject.get(projectId).get('License').containsKey(projectTask.Licence_Fee_Product__c)) {
                    if (pricesOfProductsInScopeOfTheProject.get(projectId).get('License').get(projectTask.Licence_Fee_Product__c) != projectTask.Licence_Fee__c) {
                        throw new TaskValidationException('There is another task for this project with the same License Fee product, but with different License Fee Price');
                    }
                } else {
                    pricesOfProductsInScopeOfTheProject.get(projectId).get('License').put(projectTask.Licence_Fee_Product__c, projectTask.Licence_Fee__c);
                }
            }
        }
        
        System.debug('after pricesOfProductsInScopeOfTheProject: ' + JSON.serialize(pricesOfProductsInScopeOfTheProject));
    }*/

    private static Map<Id, project_cloud__Project_Phase__c> getPhasesMap(List<project_cloud__Project_Task__c> tasks) {
        Set<Id> phasesIds = new Set<Id>();
        for (project_cloud__Project_Task__c task : tasks) {
            phasesIds.add(task.project_cloud__Project_Phase__c);
        }

        Map<Id, project_cloud__Project_Phase__c> phasesMap = new Map<Id, project_cloud__Project_Phase__c>([
            SELECT
                Id, project_cloud__Project__c, project_cloud__Project__r.ccpe_ocp__Account__c,
	            project_cloud__Project__r.ccpe_ocp__Opportunity__c,
            	project_cloud__Project__r.ccpe_ocp__Opportunity__r.Win_Probability__c
            FROM
                project_cloud__Project_Phase__c
            WHERE
                Id IN :phasesIds
        ]);

        return phasesMap;
    }

    private static Revenue_Item__c createServiceRevenueItem(project_cloud__Project_Task__c task, Id projectId) {
        Revenue_Item__c revenueItem = createRevenueItem(task, projectId);
        revenueItem.Product__c = task.Service__c;
        revenueItem.Quantity__c = task.project_cloud__Estimated_Hours__c != null ? task.project_cloud__Estimated_Hours__c / 8 : 0;
        revenueItem.Amount__c = task.Total_Professional_Fees__c;

        return revenueItem;
    }

    private static Revenue_Item__c createDelegateRevenueItem(project_cloud__Project_Task__c task, Id projectId) {
        Revenue_Item__c revenueItem = createRevenueItem(task, projectId);
        revenueItem.Product__c = task.Delegate_Fee_Product__c;
        revenueItem.Quantity__c = task.Number_of_Delegates__c;
        revenueItem.Amount__c = task.Number_of_Delegates__c != null && task.Delegate_Fee__c != null ? task.Number_of_Delegates__c * task.Delegate_Fee__c : 0;

        return revenueItem;
    }

    private static Revenue_Item__c createLicenseRevenueItem(project_cloud__Project_Task__c task, Id projectId) {
        Revenue_Item__c revenueItem = createRevenueItem(task, projectId);
        revenueItem.Product__c = task.Licence_Fee_Product__c;
        revenueItem.Quantity__c = task.Number_of_Licences__c;
        revenueItem.Amount__c = task.Number_of_Licences__c != null && task.Licence_Fee__c != null ? task.Number_of_Licences__c * task.Licence_Fee__c : 0;

        return revenueItem;
    }

    private static Revenue_Item__c createRevenueItem(project_cloud__Project_Task__c task, Id projectId) {
        Revenue_Item__c revenueItem = new Revenue_Item__c(
                Project_Task__c = task.Id, Project__c = projectId, Cost__c = task.Reported_Cost__c,
                Date__c = task.project_cloud__End__c.addMonths(1).toStartOfMonth().addDays(-1)
        );
        
        if (task.project_cloud__Project_Phase__r.project_cloud__Project__r.ccpe_ocp__Opportunity__c != null) {
            if (task.project_cloud__Project_Phase__r.project_cloud__Project__r.ccpe_ocp__Opportunity__r.Win_Probability__c != null) {
                revenueItem.Revenue_Probability__c = task.project_cloud__Project_Phase__r.project_cloud__Project__r.ccpe_ocp__Opportunity__r.Win_Probability__c;
            } else {
                revenueItem.Revenue_Probability__c = '0%';
            }
        } else {
            revenueItem.Revenue_Probability__c = '0%';
        }
        
        return revenueItem;
    }

    private static Revenue_Item__c findRevenueItemByProductId(List<Revenue_Item__c> revenueItems, Id productId) {
        Revenue_Item__c revenueItem;
        if (revenueItems != null && revenueItems.size() > 0) {
            for (Revenue_Item__c ri : revenueItems) {
                if (ri.Product__c == productId) {
                    revenueItem = ri;
                    break;
                }
            }
        }
        return revenueItem;
    }

    public static void handleBeforeDelete(List<project_cloud__Project_Task__c> oldTasks) {
        List<Revenue_Item__c> relatedRevenueItems = [
            SELECT
                Id, Opportunity__c
            FROM
                Revenue_Item__c
            WHERE
                Project_Task__c IN :oldTasks
            	//AND Opportunity__c = null
        ];
system.debug('oldTasks = ' + oldTasks);
system.debug('relatedRevenueItems  = ' + relatedRevenueItems);
        
        List<Revenue_Item__c> revenueItemsToUpdate = new List<Revenue_Item__c>();
        List<Revenue_Item__c> revenueItemsToDelete = new List<Revenue_Item__c>();
        
        for (Revenue_Item__c revenueItem : relatedRevenueItems) {
            if (revenueItem.Opportunity__c != null) {
            	revenueItem.Project__c = null;
                revenueItem.Project_Task__c = null;
                //revenueItemsToUpdate.add(revenueItem);
                revenueItemsToDelete.add(revenueItem);
            } else {
                revenueItemsToDelete.add(revenueItem);
            }
        }
        
        delete revenueItemsToDelete;
        update revenueItemsToUpdate;
    }
    
    public static void updateRevenueItemsIfSyncOptionIsChanged(Map<Id, project_cloud__Project_Task__c> oldTasks, 
        Map<Id, project_cloud__Project_Task__c> newTasks) {
        
        List<project_cloud__Project_Task__c> projectTasksWithChangedOption = new List<project_cloud__Project_Task__c>();
        
        for (Id taskId : newTasks.keySet()) {
            if (syncOptionIsChanged(oldTasks.get(taskId), newTasks.get(taskId))) {
                projectTasksWithChangedOption.add(newTasks.get(taskId));
            }
        }
        
        List<Revenue_Item__c> revenueItems = [
            SELECT
                Id
            FROM
                Revenue_Item__c
            WHERE
                Project_Task__c IN: projectTasksWithChangedOption
        ];
        
        for (Revenue_Item__c revenueItem : revenueItems) {
            revenueItem.Opportunity__c = null;
            revenueItem.Opportunity_Product_Id__c = null;
        }
        
        update revenueItems;
    }
    
    private static Boolean syncOptionIsChanged(project_cloud__Project_Task__c oldTask, project_cloud__Project_Task__c newTask) {
        for (String syncField : syncFields) {
            if (oldTask.get(syncField) != newTask.get(syncField)) {
                return true;
            }
        }
        return false;
    }
	
    public class TaskValidationException extends Exception {}
}