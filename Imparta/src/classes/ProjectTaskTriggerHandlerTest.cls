@IsTest
public class ProjectTaskTriggerHandlerTest {

    @TestSetup
    static void initData() {
        project_cloud__Project__c testProject = new project_cloud__Project__c(Name = 'testProject');
        insert testProject;

        project_cloud__Project_Phase__c testPhase = new project_cloud__Project_Phase__c(
                Name = 'testPhase', project_cloud__Project__c = testProject.Id
        );
        insert testPhase;

        project_cloud__Project_Task__c testTaskWithoutProducts = new project_cloud__Project_Task__c(
                Name = 'testTaskWithoutProducts', project_cloud__Autonomous_Start__c = Date.today().toStartOfWeek(),
                project_cloud__Project_Phase__c = testPhase.Id, project_cloud__Duration__c = 2, Billable__c = true,project_cloud__Estimated_Hours__c = 8
        );
        insert testTaskWithoutProducts;
    }

    @IsTest
    static void testAbsenceOfRevenueItemsAfterInit() {
        List<Revenue_Item__c> revenueItems = [SELECT Id FROM Revenue_Item__c];
        System.assertEquals(0, revenueItems.size());
    }

    @IsTest
    static void testTaskInsert() {
        createTaskWithProducts('');

        List<Revenue_Item__c> revenueItems = [SELECT Id FROM Revenue_Item__c];
        //System.assertEquals(3, revenueItems.size());
    }

    @IsTest
    static void testTaskDelete() {
        project_cloud__Project_Task__c task = createTaskWithProducts('');

        Test.startTest();
        delete task;
        Test.stopTest();

        List<Revenue_Item__c> revenueItems = [SELECT Id FROM Revenue_Item__c];
        //System.assertEquals(0, revenueItems.size());
    }

    @IsTest
    static void testTaskProductsClean() {
        project_cloud__Project_Task__c task = createTaskWithProducts('');

        List<Revenue_Item__c> revenueItems = [SELECT Id FROM Revenue_Item__c];
        //System.assertEquals(3, revenueItems.size());

        task.Service__c = null;
        task.Delegate_Fee_Product__c = null;
        task.Licence_Fee_Product__c = null;
        update task;

        revenueItems = [SELECT Id FROM Revenue_Item__c];
        System.assertEquals(0, revenueItems.size());
    }

    @IsTest
    static void testTaskProductsPopulation() {
        Product2 prod1 = new Product2(Name = 'prod1');
        Product2 prod2 = new Product2(Name = 'prod2');
        Product2 prod3 = new Product2(Name = 'prod3');
        insert new List<Product2>{prod1, prod2, prod3};

        project_cloud__Project_Task__c task = [SELECT Id FROM project_cloud__Project_Task__c LIMIT 1];

        task.Service__c = prod1.Id;
        //task.Delegate_Fee_Product__c = prod2.Id;
        //task.Licence_Fee_Product__c = prod3.Id;
        task.project_cloud__Estimated_Hours__c = 16;
        task.Number_of_Delegates__c = 2;
        task.Number_of_Licences__c = 2;
        task.Delegate_Fee__c = 20;
        task.Licence_Fee__c = 20;
        update task;

        List<Revenue_Item__c> revenueItems = [SELECT Id FROM Revenue_Item__c];
        //System.assertEquals(3, revenueItems.size());
    }

    @IsTest
    static void testTaskProductsChange() {
        Product2 prod4 = new Product2(Name = 'prod4');
        Product2 prod5 = new Product2(Name = 'prod5');
        Product2 prod6 = new Product2(Name = 'prod6');
        insert new List<Product2>{prod4, prod5, prod6};

        project_cloud__Project_Task__c task = createTaskWithProducts('');

        List<Revenue_Item__c> revenueItems = [SELECT Id, Name, Product__c FROM Revenue_Item__c];
        //System.assertEquals(3, revenueItems.size());

        task.Service__c = prod4.Id;
        //task.Delegate_Fee_Product__c = prod5.Id;
        //task.Licence_Fee_Product__c = prod6.Id;
        update task;

        List<Revenue_Item__c> newRevenueItems = [SELECT Id, Name, Product__c FROM Revenue_Item__c];
        //System.assertEquals(3, revenueItems.size());

        //System.assert(revenueItems != newRevenueItems);
    }

    @IsTest
    static void testTaskChange() {
        project_cloud__Project_Task__c task = createTaskWithProducts('');

        List<Revenue_Item__c> revenueItems = [SELECT Id FROM Revenue_Item__c];
        //System.assertEquals(3, revenueItems.size());

        task.project_cloud__Duration__c = 5;
        task.ccpe_r__Cost__c = 3;
        task.project_cloud__Estimated_Hours__c = 80;
        task.Number_of_Delegates__c = 4;
        task.Number_of_Licences__c = 4;
        update task;
    }
    
    @IsTest
    static void testTaskChangeDF() {
        project_cloud__Project_Task__c task = createTaskWithProducts('Delegate Fee');

        List<Revenue_Item__c> revenueItems = [SELECT Id FROM Revenue_Item__c];
        //System.assertEquals(3, revenueItems.size());

        task.project_cloud__Duration__c = 5;
        task.ccpe_r__Cost__c = 3;
        task.project_cloud__Estimated_Hours__c = 80;
        task.Number_of_Delegates__c = 4;
        task.Number_of_Licences__c = 4;
        update task;
    }
    
    @IsTest
    static void testTaskChangeLF() {
        project_cloud__Project_Task__c task = createTaskWithProducts('Licence Fee');

        List<Revenue_Item__c> revenueItems = [SELECT Id FROM Revenue_Item__c];
        //System.assertEquals(3, revenueItems.size());

        task.project_cloud__Duration__c = 5;
        task.ccpe_r__Cost__c = 3;
        task.project_cloud__Estimated_Hours__c = 80;
        task.Number_of_Delegates__c = 4;
        task.Number_of_Licences__c = 4;
        update task;
    }

    private static project_cloud__Project_Task__c createTaskWithProducts(String type) {
        Product2 prod1 = new Product2(Name = 'prod1');
        Product2 prod2 = new Product2(Name = 'prod2');
        Product2 prod3 = new Product2(Name = 'prod3');
        insert new List<Product2>{prod1, prod2, prod3};

        project_cloud__Project_Phase__c testPhase = [SELECT Id FROM project_cloud__Project_Phase__c LIMIT 1];

        project_cloud__Project_Task__c testTask = new project_cloud__Project_Task__c(
                Name = 'testTask', project_cloud__Autonomous_Start__c = Date.today().toStartOfWeek(),
                project_cloud__Project_Phase__c = testPhase.Id, project_cloud__Duration__c = 2,
                Service__c = prod1.Id, //Delegate_Fee_Product__c = prod2.Id, Licence_Fee_Product__c = prod3.Id,
                project_cloud__Estimated_Hours__c = 16, //Number_of_Delegates__c = 2, Number_of_Licences__c = 2,
                //Delegate_Fee__c = 20, Licence_Fee__c = 20,
                Billable__c = true
        );
        
        if (type == 'Delegate Fee') {
            testTask.project_cloud__Estimated_Hours__c = 1;
            testTask.Service__c = null;
            testTask.Delegate_Fee_Product__c = prod2.Id;
            testTask.Number_of_Delegates__c = 2;
        } else if (type == 'Licence Fee') {
            testTask.project_cloud__Estimated_Hours__c = 1;
            testTask.Service__c = null;
            testTask.Licence_Fee_Product__c = prod3.Id;
            testTask.Number_of_Licences__c = 2;
        }
        
        insert testTask;

        return testTask;
    }

}