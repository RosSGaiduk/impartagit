@isTest
public class CongaLauncherControllersTEST
{
    
    @isTest
    static void testCongaInvoiceLauncherController()    
    {
        /* create the required dummy data */
        List<APXTConga4__Conga_Template__c> ctsToInsert = new List<APXTConga4__Conga_Template__c>();
        List<APXTConga4__Conga_Email_Template__c> cetsToInsert = new List<APXTConga4__Conga_Email_Template__c>();
        
        APXTConga4__Conga_Template__c ct = new APXTConga4__Conga_Template__c();
            ct.APXTConga4__Template_Group__c = 'Invoicing';
            ct.APXTConga4__Description__c = 'Created By Solution Pack for Composer Solution: CSI (s2cor__Sage_INV_Trade_Document__c)';
                ctsToInsert.add(ct);
                                            
        APXTConga4__Conga_Email_Template__c cet = new APXTConga4__Conga_Email_Template__c();
            cet.APXTConga4__Template_Group__c = 'Invoicing';
            cet.APXTConga4__Description__c = 'Created By Solution Pack for Composer Solution: CSI (s2cor__Sage_INV_Trade_Document__c)';
                cetsToInsert.add(cet);

        APXTConga4__Conga_Template__c ct2 = new APXTConga4__Conga_Template__c();
            ct2.APXTConga4__Template_Group__c = 'Quoting';
            ct2.APXTConga4__Description__c = 'Created By Solution Pack for Composer Solution: CSQ (s2cor__Sage_INV_Trade_Document__c)';
                ctsToInsert.add(ct2);           
                
        APXTConga4__Conga_Email_Template__c cet2 = new APXTConga4__Conga_Email_Template__c();
            cet2.APXTConga4__Template_Group__c = 'Quoting';
            cet2.APXTConga4__Description__c = 'Created By Solution Pack for Composer Solution: CSQ (s2cor__Sage_INV_Trade_Document__c)';
                cetsToInsert.add(cet2);                      

        database.insert(ctsToInsert);
        database.insert(cetsToInsert);                                                                 

        Test.startTest();        
            CongaInvoiceLauncherController clc = new CongaInvoiceLauncherController();            
        Test.stopTest();
    }
    
    @isTest
    static void testCongaCheckPrintLauncherController()    
    {
        List<APXTConga4__Conga_Merge_Query__c> queriesToCreate = new List<APXTConga4__Conga_Merge_Query__c>();        
        
        APXTConga4__Conga_Template__c ct = new APXTConga4__Conga_Template__c();
            ct.APXTConga4__Template_Group__c = 'Cheque';
            ct.APXTConga4__Description__c = 'Created By Solution Pack for Composer Solution: CPC (s2cor__Sage_ACC_Journal__c)';
                database.insert(ct);
                
        APXTConga4__Conga_Merge_Query__c payToQuery = new APXTConga4__Conga_Merge_Query__c();
            payToQuery.APXTConga4__Name__c = 'Check Print Pay To [PayTo]';
            paytoQuery.APXTConga4__Description__c = 'Created By Solution Pack for Composer Solution: CPC (s2cor__Sage_ACC_Journal__c)';
                queriesToCreate.add(payToQuery);
                
        APXTConga4__Conga_Merge_Query__c billDetsQuery = new APXTConga4__Conga_Merge_Query__c();
            billDetsQuery.APXTConga4__Name__c = 'Check Print Bill Details [BillsDets]';
            billDetsQuery.APXTConga4__Description__c = 'Created By Solution Pack for Composer Solution: CPC (s2cor__Sage_ACC_Journal__c)';
                queriesToCreate.add(billDetsQuery);                
                    
        APXTConga4__Conga_Merge_Query__c Qvar0 = new APXTConga4__Conga_Merge_Query__c();
            Qvar0.APXTConga4__Name__c = '[QVAR0]';
            Qvar0.APXTConga4__Description__c = 'Composer Solution: CPC (s2cor__Sage_ACC_Journal__c)';
                queriesToCreate.add(Qvar0);
                
        APXTConga4__Conga_Merge_Query__c Qvar1 = new APXTConga4__Conga_Merge_Query__c();
            Qvar1.APXTConga4__Name__c = '[QVAR1]';
            Qvar1.APXTConga4__Description__c = 'Composer Solution: CPC (s2cor__Sage_ACC_Journal__c)';
                queriesToCreate.add(Qvar1);                
                
            database.insert(queriesToCreate);
        
        Test.startTest();        
            PrintCheckLauncherController pclc = new PrintCheckLauncherController();            
        Test.stopTest();    
    }
    
    @isTest
    static void testCongaPaymentReceiptLauncherController()    
    {        
        List<APXTConga4__Conga_Merge_Query__c> queriesToCreate2 = new List<APXTConga4__Conga_Merge_Query__c>();        
        
        APXTConga4__Conga_Template__c ct = new APXTConga4__Conga_Template__c();
            ct.APXTConga4__Template_Group__c = 'PRM';
            ct.APXTConga4__Description__c = 'Created By Solution Pack for Composer Solution: CRCPT (s2cor__Sage_ACC_Journal__c)';
                database.insert(ct);
                
        APXTConga4__Conga_Email_Template__c cet = new APXTConga4__Conga_Email_Template__c();
            cet.APXTConga4__Template_Group__c = 'PRM';
            cet.APXTConga4__Description__c = 'Created By Solution Pack for Composer Solution: CRCPT (s2cor__Sage_ACC_Journal__c)';
                database.insert(cet);                  
                
        APXTConga4__Conga_Merge_Query__c PRMMultiDetsQuery = new APXTConga4__Conga_Merge_Query__c();
            PRMMultiDetsQuery.APXTConga4__Name__c = 'Payment Receipt Multi-paid Invoice details [PRMultiDets]';
            PRMMultiDetsQuery.APXTConga4__Description__c = 'Created By Solution Pack for Composer Solution: CRCPT (s2cor__Sage_ACC_Journal__c)';
                queriesToCreate2.add(PRMMultiDetsQuery);
                
        APXTConga4__Conga_Merge_Query__c PayAppDetsQuery = new APXTConga4__Conga_Merge_Query__c();
            PayAppDetsQuery.APXTConga4__Name__c = 'Payment Application Details [PayAppDets]';
            PayAppDetsQuery.APXTConga4__Description__c = 'Created By Solution Pack for Composer Solution: CRCPT (s2cor__Sage_ACC_Journal__c)';
                queriesToCreate2.add(PayAppDetsQuery);                                
                    
        APXTConga4__Conga_Merge_Query__c Qvar0 = new APXTConga4__Conga_Merge_Query__c();
            Qvar0.APXTConga4__Name__c = '[QVAR0]';
            Qvar0.APXTConga4__Description__c = 'Composer Solution: CRCPT (s2cor__Sage_ACC_Journal__c)';
                queriesToCreate2.add(Qvar0);

        APXTConga4__Conga_Merge_Query__c Qvar1 = new APXTConga4__Conga_Merge_Query__c();
            Qvar1.APXTConga4__Name__c = '[QVAR1]';
            Qvar1.APXTConga4__Description__c = 'Composer Solution: CRCPT (s2cor__Sage_ACC_Journal__c)';
                queriesToCreate2.add(Qvar1);
                                                
            database.insert(queriesToCreate2);
        
        Test.startTest();         
            PaymentReceiptLauncherController prlc = new PaymentReceiptLauncherController();
        Test.stopTest(); 
    }
    
@isTest
    static void testCongaPaymentRemittanceLauncherController()    
    {        
        List<APXTConga4__Conga_Merge_Query__c> queriesToCreate4 = new List<APXTConga4__Conga_Merge_Query__c>();        
        
        APXTConga4__Conga_Template__c ct = new APXTConga4__Conga_Template__c();
            ct.APXTConga4__Template_Group__c = 'Remit';
            ct.APXTConga4__Description__c = 'Created By Solution Pack for Composer Solution: CPR (s2cor__Sage_ACC_Journal__c)';
                database.insert(ct);
                
        APXTConga4__Conga_Email_Template__c cet = new APXTConga4__Conga_Email_Template__c();
            cet.APXTConga4__Template_Group__c = 'Remit';
            cet.APXTConga4__Description__c = 'Created By Solution Pack for Composer Solution: CPR (s2cor__Sage_ACC_Journal__c)';
                database.insert(cet);                  
                
        APXTConga4__Conga_Merge_Query__c PRMMultiDetsQuery = new APXTConga4__Conga_Merge_Query__c();
            PRMMultiDetsQuery.APXTConga4__Name__c = 'Transaction Details for Payment Remittance [TDDets]';
            PRMMultiDetsQuery.APXTConga4__Description__c = 'Created By Solution Pack for Composer Solution: CPR (s2cor__Sage_ACC_Journal__c)';
                queriesToCreate4.add(PRMMultiDetsQuery);
                
        APXTConga4__Conga_Merge_Query__c PayAppDetsQuery = new APXTConga4__Conga_Merge_Query__c();
            PayAppDetsQuery.APXTConga4__Name__c = 'Remittance Payment Application Details [RemitPayAppDets]';
            PayAppDetsQuery.APXTConga4__Description__c = 'Created By Solution Pack for Composer Solution: CPR (s2cor__Sage_ACC_Journal__c)';
                queriesToCreate4.add(PayAppDetsQuery);                                
                    
        APXTConga4__Conga_Merge_Query__c Qvar0 = new APXTConga4__Conga_Merge_Query__c();
            Qvar0.APXTConga4__Name__c = '[QVAR0]';
            Qvar0.APXTConga4__Description__c = 'Composer Solution: CPR (s2cor__Sage_ACC_Journal__c)';
                queriesToCreate4.add(Qvar0);

        APXTConga4__Conga_Merge_Query__c Qvar1 = new APXTConga4__Conga_Merge_Query__c();
            Qvar1.APXTConga4__Name__c = '[QVAR1]';
            Qvar1.APXTConga4__Description__c = 'Composer Solution: CPR (s2cor__Sage_ACC_Journal__c)';
                queriesToCreate4.add(Qvar1);

        APXTConga4__Conga_Merge_Query__c Qvar2 = new APXTConga4__Conga_Merge_Query__c();
            Qvar2.APXTConga4__Name__c = '[QVAR2]';
            Qvar2.APXTConga4__Description__c = 'Composer Solution: CPR (s2cor__Sage_ACC_Journal__c)';
                queriesToCreate4.add(Qvar2);                                
                
            database.insert(queriesToCreate4);
        
        Test.startTest();         
            PaymentRemittanceLauncherController crlc = new PaymentRemittanceLauncherController();
        Test.stopTest(); 
    }    
    
    @isTest
    static void testCustomerStatementLauncherController()    
    {
        List<APXTConga4__Conga_Merge_Query__c> queriesToCreate3 = new List<APXTConga4__Conga_Merge_Query__c>();        
        
        APXTConga4__Conga_Template__c statementTemplate = new APXTConga4__Conga_Template__c();
            statementTemplate.APXTConga4__Template_Group__c = 'Statement';
            statementTemplate.APXTConga4__Description__c = 'Created By Solution Pack for Composer Solution: Conga Customer Statement (Account)';
                database.insert(statementTemplate);
                
        APXTConga4__Conga_Email_Template__c cet = new APXTConga4__Conga_Email_Template__c();
            cet.APXTConga4__Template_Group__c = 'Statement';
            cet.APXTConga4__Description__c = 'Created By Solution Pack for Composer Solution: Conga Customer Statement (Account)';
                database.insert(cet);
                
        APXTConga4__Conga_Merge_Query__c Qvar0 = new APXTConga4__Conga_Merge_Query__c();
            Qvar0.APXTConga4__Name__c = '[QVAR0]';
            Qvar0.APXTConga4__Description__c = 'Composer Solution: Conga Customer Statement (Account)';
                queriesToCreate3.add(Qvar0);
                
        APXTConga4__Conga_Merge_Query__c Qvar1 = new APXTConga4__Conga_Merge_Query__c();
            Qvar1.APXTConga4__Name__c = '[QVAR0]';
            Qvar1.APXTConga4__Description__c = 'Composer Solution: Conga Customer Statement (Account)';
                queriesToCreate3.add(Qvar1);
                
        APXTConga4__Conga_Merge_Query__c Qvar2 = new APXTConga4__Conga_Merge_Query__c();
            Qvar2.APXTConga4__Name__c = '[QVAR2]';
            Qvar2.APXTConga4__Description__c = 'Composer Solution: Conga Customer Statement (Account)';
                queriesToCreate3.add(Qvar2);                                
                
            database.insert(queriesToCreate3);                                              
        
        Test.startTest();         
            CustomerStatementLauncherController cslc = new CustomerStatementLauncherController();
        Test.stopTest();     
    }

}