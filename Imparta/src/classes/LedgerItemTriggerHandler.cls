public class LedgerItemTriggerHandler {

	public static void insertRevItems(Map<Id, s2cor__Sage_ACC_Ledger_Item__c> newMap) {

		List<Revenue_Item__c> newRevItems = new List<Revenue_Item__c>();

		Set<Id> custSettingLedAccIdsSet = getFromCustSettingLedgAccIds();

		Map<Id, s2cor__Sage_ACC_Ledger_Item__c> ledgItemEntryTransAccounts = getLedgItemEntryTransAccounts(newMap);

		for (s2cor__Sage_ACC_Ledger_Item__c newLedItem : newMap.values()) {
			if (custSettingLedAccIdsSet.contains(newLedItem.s2cor__Ledger_Account__c)) {
				newRevItems.add(createRevItem(newLedItem, newMap, ledgItemEntryTransAccounts));
			}
		}

		if (newRevItems.size() > 0) {
			insert newRevItems;
		}
	}

	public static void updateRevItems(Map<Id, s2cor__Sage_ACC_Ledger_Item__c> newMap, Map<Id, s2cor__Sage_ACC_Ledger_Item__c> oldMap) {

		List<Revenue_Item__c> newRevItems = new List<Revenue_Item__c>();
		List<Revenue_Item__c> toDeleteRevItems = new List<Revenue_Item__c>();
		List<Revenue_Item__c> toUpdateRevItems = new List<Revenue_Item__c>();

		Map<Id, s2cor__Sage_ACC_Ledger_Item__c> ledgItemEntryTransAccounts = getLedgItemEntryTransAccounts(newMap);
		Set<Id> custSettingLedAccIdsSet = getFromCustSettingLedgAccIds();
		List<Revenue_Item__c> allRelatedRevItems = [
				SELECT
					Id, Amount__c, Account__c, Date__c, Quantity__c, Ledger_Item__c
				FROM
					Revenue_Item__c
				WHERE
					Ledger_Item__c IN :newMap.keySet()
		];

		for (s2cor__Sage_ACC_Ledger_Item__c ledgItem : newMap.values()) {

			Boolean newAccInCustSett = custSettingLedAccIdsSet.contains(ledgItem.s2cor__Ledger_Account__c);
			Boolean oldAccInCustSett = custSettingLedAccIdsSet.contains(oldMap.get(ledgItem.Id).s2cor__Ledger_Account__c);

			if (newAccInCustSett && !oldAccInCustSett) {
				newRevItems.add(createRevItem(ledgItem, newMap, ledgItemEntryTransAccounts));
			} else if (oldAccInCustSett && !newAccInCustSett) {

				for (Revenue_Item__c revItem : allRelatedRevItems) {
					if (revItem.Ledger_Item__c == ledgItem.Id) {
						toDeleteRevItems.add(revItem);
					}
				}

			} else if (oldAccInCustSett && newAccInCustSett) {
				Boolean isRevenueItemAlreadyExisting = false;
				for (Revenue_Item__c revItem : allRelatedRevItems) {
					if (revItem.Ledger_Item__c == ledgItem.Id) {
						revItem.Amount__c = ledgItem.s2cor__Base_Credit__c;
						revItem.Account__c = ledgItemEntryTransAccounts.get(ledgItem.Id).s2cor__Ledger_Entry__r.s2cor__Transaction__r.s2cor__Account__c;
						revItem.Date__c = ledgItem.s2cor__Date__c;
						revItem.Quantity__c = 1;
						revItem.Ledger_Item__c = ledgItem.Id;
						toUpdateRevItems.add(revItem);
						isRevenueItemAlreadyExisting = true;
					}
				}
				if (!isRevenueItemAlreadyExisting) {
					newRevItems.add(createRevItem(ledgItem, newMap, ledgItemEntryTransAccounts));
				}
			}
		}

		if (toUpdateRevItems.size() > 0) {
			update toUpdateRevItems;
		}

		if (newRevItems.size() > 0) {
			insert newRevItems;
		}

		if (toDeleteRevItems.size() > 0) {
			delete toDeleteRevItems;
		}
	}

	public static void delRevItems(Map<Id, s2cor__Sage_ACC_Ledger_Item__c> oldMap) {
		List<Revenue_Item__c> revItems = [
				SELECT
					Id
				FROM
					Revenue_Item__c
				WHERE
					Ledger_Item__c IN :oldMap.keySet()
		];

		if (revItems.size() > 0) {
			delete revItems;
		}
	}

	private static Set<Id> getFromCustSettingLedgAccIds() {
		Set<Id> custSettingLedAccIdsSet = new Set<Id>();

		List<Revenue_Ledger_Accounts__c> custSettingRevLedAccs = Revenue_Ledger_Accounts__c.getAll().values();

		for (Revenue_Ledger_Accounts__c custSettingRevLedAcc : custSettingRevLedAccs) {
			custSettingLedAccIdsSet.add(custSettingRevLedAcc.Ledger_Account_Id__c);
		}

		return custSettingLedAccIdsSet;
	}

	private static Map<Id, s2cor__Sage_ACC_Ledger_Item__c> getLedgItemEntryTransAccounts(Map<Id, s2cor__Sage_ACC_Ledger_Item__c> newMap) {
		Map<Id, s2cor__Sage_ACC_Ledger_Item__c> ledgItemEntryTransAccounts = new Map<Id, s2cor__Sage_ACC_Ledger_Item__c>([
				SELECT
					s2cor__Ledger_Entry__r.s2cor__Transaction__r.s2cor__Account__c
				FROM
					s2cor__Sage_ACC_Ledger_Item__c
				WHERE
					Id IN :newMap.keySet()
		]);

		return ledgItemEntryTransAccounts;
	}

	private static Revenue_Item__c createRevItem(s2cor__Sage_ACC_Ledger_Item__c newLedItem,
												 Map<Id, s2cor__Sage_ACC_Ledger_Item__c> newMap,
												 Map<Id, s2cor__Sage_ACC_Ledger_Item__c> ledgItemEntryTransAccounts) {

		s2cor__Sage_ACC_Ledger_Item__c currentLedItem = newMap.get(newLedItem.Id);

		Revenue_Item__c revItem = new Revenue_Item__c(
				Amount__c = currentLedItem.s2cor__Base_Credit__c,
				Account__c = ledgItemEntryTransAccounts.get(newLedItem.Id).s2cor__Ledger_Entry__r.s2cor__Transaction__r.s2cor__Account__c,
				Date__c = currentLedItem.s2cor__Date__c,
				Quantity__c = 1,
				Ledger_Item__c = newLedItem.Id
		);

		return revItem;
	}
}