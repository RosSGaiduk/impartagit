@isTest
public class ProjectCloudWorkTriggerTest {
    @testSetup static void setup() {
        
        List<project_cloud__Project__c> projects = new List<project_cloud__Project__c>();
        for (Integer i = 0; i < 5; ++i) {
            projects.add(new project_cloud__Project__c());
        }
        insert projects;
        
        List<project_cloud__Work_Type__c> workTypes = new List<project_cloud__Work_Type__c>();
        for (Integer i = 0; i < 5; ++i) {
            workTypes.add(new project_cloud__Work_Type__c(Name = 'Work Type ' + i));
        }
        insert workTypes;
        
        List<project_cloud__Project_Phase__c> projectsPhases = new List<project_cloud__Project_Phase__c>();
        for (Integer i = 0; i < 5; ++i) {
           projectsPhases.add(new project_cloud__Project_Phase__c(project_cloud__Project__c = projects[i].Id));
        }
        insert projectsPhases;
        
        List<project_cloud__Project_Task__c> projectsTasks = new List<project_cloud__Project_Task__c>();
        for (Integer i = 0; i < 5; ++i) {
            projectsTasks.add(new project_cloud__Project_Task__c(project_cloud__Work_Type__c = workTypes[i].Id, project_cloud__Estimated_Hours__c = 1 + i, 
                                                                 project_cloud__Project_Phase__c = projectsPhases[i].Id,
                                                                 project_cloud__Autonomous_Start__c = Date.today().toStartOfWeek(),
                                                                 project_cloud__Duration__c = 3));
        }
        insert projectsTasks;
        
        
    }
    
    //no Expense Reports
    @isTest
    public static void test1() {
        List<project_cloud__Project__c> projects = [
            SELECT
                Id
            FROM
                project_cloud__Project__c
        ];

        List<project_cloud__Project_Task__c> projectsTasks = [
            SELECT
                Id, project_cloud__Project_Phase__r.project_cloud__Project__c
            FROM
                project_cloud__Project_Task__c
        ];
       
        List<project_cloud__Work__c> works = new List<project_cloud__Work__c>();
        for (Integer i = 0; i < 3; ++i) {
            works.add(new project_cloud__Work__c(project_cloud__Project_Task__c = projectsTasks[i].Id,
                                                 project_cloud__Date__c = Date.today().addMonths(i), project_cloud__Hours__c = i + 1));
        }

        insert works; 
        
        List<Expense_report__c> expenseReports = [
            SELECT
                Id, OwnerId, Expense_Report_Date__c, Project__c
            FROM
                Expense_report__c
        ];
        
        List<project_cloud__Work__c> allWorks =[
            SELECT
                Id,
                Expense_Report__c,
                project_cloud__Project__c,
                project_cloud__Project_Task__r.project_cloud__Project_Phase__r.project_cloud__Project__c
            FROM
                project_cloud__Work__c
        ];
        
//        System.assertEquals(3, expenseReports.size());
        for (Integer i = 0; i < works.size(); ++i) {
//            System.assert(works[i].project_cloud__Project_Task__c != null);
//            System.assertEquals(UserInfo.getUserId(), expenseReports[i].ownerId);
            Integer numberOfDays = Date.daysInMonth(Date.today().year(), Date.today().month() + i);
            Date lastDayOfMonth = Date.newInstance(Date.today().year(), Date.today().month() + i, numberOfDays);
//            System.assertEquals(Date.newInstance(Date.today().year(), Date.today().month() + i, lastDayOfMonth.day()), expenseReports[i].Expense_Report_Date__c); 
            //System.assert(allWorks[i].project_cloud__Project__c != null);
//            System.assertEquals(allWorks[i].project_cloud__Project_Task__r.project_cloud__Project_Phase__r.project_cloud__Project__c, expenseReports[i].Project__c);
//            System.assertEquals(expenseReports[i].Id, allWorks[i].Expense_Report__c);
        }
    }
    
    //all Expense Reports
    @isTest
    public static void test2() {
        List<project_cloud__Project__c> projects = [
            SELECT
                Id
            FROM
                project_cloud__Project__c
        ];

        List<project_cloud__Project_Task__c> projectsTasks = [
            SELECT
                Id, project_cloud__Project_Phase__r.project_cloud__Project__c
            FROM
                project_cloud__Project_Task__c
        ];
        
        List<Expense_Report__c> reports = new List<Expense_Report__c>();
        for (Integer i = 0; i < 3; ++i) {
           reports.add(new Expense_report__c (
                Expense_Report_Date__c = Date.today().addMonths(i),
                Project__c = projects[i].Id,
                Mileage_YTD__c = 0));
        }
        insert reports;
        
        List<project_cloud__Work__c> works = new List<project_cloud__Work__c>();
       
        for (Integer i = 0; i < 3; ++i) {
            works.add(new project_cloud__Work__c(
                project_cloud__Project_Task__c = projectsTasks[i].Id,
                project_cloud__Date__c = Date.today().addMonths(i),
                project_cloud__Hours__c = i + 1));
        }
        insert works;   
        
        List<Expense_report__c> expenseReports = [
            SELECT
                Id, OwnerId, Expense_Report_Date__c, Project__c, Project_Manager__c
            FROM
                Expense_report__c
        ];
                
        List<project_cloud__Work__c> allWorks =[
            SELECT
                Id, project_cloud__Date__c, Expense_Report__c, project_cloud__Project__c, project_cloud__Project_Task__r.project_cloud__Project_Phase__r.project_cloud__Project__c
            FROM
                project_cloud__Work__c
        ];        

//        System.assertEquals(3, expenseReports.size());
        for (Integer i = 0; i < works.size(); ++i) {
//            System.assert(works[i].project_cloud__Project_Task__c != null);
//            System.assertEquals(UserInfo.getUserId(), expenseReports[i].ownerId);
//            System.assertEquals(works[i].project_cloud__Date__c.year(), expenseReports[i].Expense_Report_Date__c.year()); 
//            System.assertEquals(works[i].project_cloud__Date__c.month(), expenseReports[i].Expense_Report_Date__c.month()); 
//            System.assertEquals(works[i].project_cloud__Date__c, expenseReports[i].Expense_Report_Date__c);
//            System.assertEquals(expenseReports[i].Id, allWorks[i].Expense_Report__c);
        }
         
    }
    
    //some Expense Reports are, some aren't
    @isTest
    public static void test3() {
        List<project_cloud__Project__c> projects = [
            SELECT
                Id
            FROM
                project_cloud__Project__c
        ];
                
        List<project_cloud__Project_Task__c> projectsTasks = [
            SELECT
                Id
            FROM
                project_cloud__Project_Task__c
        ];
        List<Expense_Report__c> reports = new List<Expense_Report__c>();
        for (Integer i = 0; i < 3; ++i) {
           reports.add(new Expense_report__c (
                Expense_Report_Date__c = Date.today().addMonths(i), Project__c = projects[i].Id,
                 Mileage_YTD__c = 0));
        }
        insert reports;  
        
        List<project_cloud__Work__c> works = new List<project_cloud__Work__c>();
        for (Integer i = 0; i < 5; ++i) {
            works.add(new project_cloud__Work__c(project_cloud__Project_Task__c = projectsTasks[i].Id,
                                                 project_cloud__Date__c = Date.today().addMonths(i), project_cloud__Hours__c = i + 1));
        }
        insert works;
      
        List<Expense_report__c> expenseReports = [
            SELECT
                Id, OwnerId, Expense_Report_Date__c, Project__c
            FROM
                Expense_report__c
        ];
                
        List<project_cloud__Work__c> allWorks =[
            SELECT
                Id, Expense_Report__c, project_cloud__Project_Task__r.project_cloud__Project_Phase__r.project_cloud__Project__c
            FROM
                project_cloud__Work__c
        ];
        
//        System.assertEquals(5, expenseReports.size());
        for (Integer i = 0; i < works.size(); ++i) {
//            System.assert(works[i].project_cloud__Project_Task__c != null);
//            System.assertEquals(UserInfo.getUserId(), expenseReports[i].ownerId);
            
            if (i < 3) {
//                System.assertEquals(works[i].project_cloud__Date__c.year(), expenseReports[i].Expense_Report_Date__c.year()); 
//                System.assertEquals(works[i].project_cloud__Date__c.month(), expenseReports[i].Expense_Report_Date__c.month()); 
            } else {
                Integer numberOfDays = Date.daysInMonth(Date.today().year(), Date.today().month() + i);
                Date lastDayOfMonth = Date.newInstance(Date.today().year(), Date.today().month() + i, numberOfDays);
//                System.assertEquals(Date.newInstance(Date.today().year(), Date.today().month() + i, lastDayOfMonth.day()), expenseReports[i].Expense_Report_Date__c); 
            }
//            System.assertEquals(allWorks[i].project_cloud__Project_Task__r.project_cloud__Project_Phase__r.project_cloud__Project__c, expenseReports[i].Project__c);
//            System.assertEquals(expenseReports[i].Id, allWorks[i].Expense_Report__c);
        }
         
    }
    
    @isTest
    public static void test4() {
        
        project_cloud__Project__c project = new project_cloud__Project__c();
   
        insert project;
        
        List<project_cloud__Project__c> projects = [
            SELECT
                Id
            FROM
                project_cloud__Project__c
        ];
        
        project_cloud__Project_Phase__c projectPhase = new project_cloud__Project_Phase__c(project_cloud__Project__c = projects[0].Id);
        insert projectPhase;
        
        List<project_cloud__Work_Type__c> workTypes = [
            SELECT
                Id
            FROM
                project_cloud__Work_Type__c
        ];
        
        List<Cost_Rate__c> costRates = new List<Cost_Rate__c>();
        for (Integer i = 0; i < 3; ++i) {
            costRates.add(new Cost_Rate__c(Work_Type__c = workTypes[i].Id, Project__c = projects[i].Id, Cost_Rate__c = 15, Resource__c = UserInfo.getUserId()));
        }
        costRates.add(new Cost_Rate__c(Work_Type__c = workTypes[0].Id, Project__c = projects[5].Id, Cost_Rate__c = 10, Resource__c = UserInfo.getUserId()));
        insert costRates;
        
        List<project_cloud__Project_Task__c> projectsTasks = [
            SELECT
                Id
            FROM
                project_cloud__Project_Task__c
        ];
        List<Expense_Report__c> reports = new List<Expense_Report__c>();
        for (Integer i = 0; i < 3; ++i) {
           reports.add(new Expense_report__c (
                Expense_Report_Date__c = Date.today().addMonths(i), Project__c = projects[i].Id,
                 Mileage_YTD__c = 0));
        }
        insert reports; 
        
        List<Expense_report__c> expenseReports0 = [
            SELECT
                Id, OwnerId, Expense_Report_Date__c, Project__c
            FROM
                Expense_report__c
        ];
                
        List<User> currentUser = [
            SELECT
                Id, Cost_Rate__c
            FROM
                User
            WHERE
                Id = :UserInfo.getUserId()
        ];
        
        currentUser[0].Cost_Rate__c = 7;
        
        update currentUser;
        
        List<project_cloud__Work__c> works = new List<project_cloud__Work__c>();
        for (Integer i = 0; i < 5; ++i) {
            if (i == 0) {
                works.add(new project_cloud__Work__c(project_cloud__Project_Task__c = projectsTasks[i].Id,
                                                    project_cloud__Date__c = Date.today().addMonths(i + 3), project_cloud__Hours__c = i + 1)); 
            } else {
                works.add(new project_cloud__Work__c(project_cloud__Project_Task__c = projectsTasks[i].Id,
                                                    project_cloud__Date__c = Date.today().addMonths(i), project_cloud__Hours__c = i + 1));
            }
             
        }
        insert works;
      
        List<Expense_report__c> expenseReports = [
            SELECT
                Id, OwnerId, Expense_Report_Date__c, Project__c
            FROM
                Expense_report__c
        ];   
       
        Decimal costRateFromUser = currentUser[0].Cost_Rate__c;
        List<project_cloud__Work__c> allWorks =[
            SELECT
                Id, Expense_Report__c, project_cloud__Project_Task__r.project_cloud__Project_Phase__r.project_cloud__Project__c, Rate__c
            FROM
                project_cloud__Work__c
        ];

//        System.assertEquals(6, expenseReports.size());
        for (Integer i = 0; i < allWorks.size(); ++i) {
            if (i < 3) {
//                System.assertEquals(costRates[i].Cost_Rate__c, allWorks[i].Rate__c);
            } else {
//                System.assertEquals(costRateFromUser, allWorks[i].Rate__c);
            }
        }
    }   
    
    @isTest
    public static void test5() {
        
        List<project_cloud__Project__c> projects = [
            SELECT
                Id
            FROM
                project_cloud__Project__c
        ];
        
        List<project_cloud__Project_Task__c> projectsTasks = [
            SELECT
                Id, project_cloud__Project_Phase__r.project_cloud__Project__c
            FROM
                project_cloud__Project_Task__c
        ];
                
        List<Expense_Report__c> reports = new List<Expense_Report__c>();
        for (Integer i = 0; i < 3; ++i) {
            Expense_report__c expenseReport;
            //if (i == 3) {
                //expenseReport = new Expense_report__c (
                    //Expense_Report_Date__c = Date.today().addMonths(i), Project__c = projects[5].Id,
                    //Mileage_YTD__c = 0);    
           // } else {
                expenseReport = new Expense_report__c (
                    Expense_Report_Date__c = Date.today().addMonths(i), Project__c = projects[i].Id,
                    Mileage_YTD__c = 0);
            //}
            
            reports.add(expenseReport);
        }
        
        insert reports;  
        
        List<project_cloud__Work__c> works = new List<project_cloud__Work__c>();
        for (Integer i = 0; i < 5; ++i) {
            if (i == 2) {
             works.add(new project_cloud__Work__c(project_cloud__Project__c = projects[i].Id,
                                                 project_cloud__Date__c = Date.today().addMonths(i), project_cloud__Hours__c = i + 1));   
            } else {
                works.add(new project_cloud__Work__c(project_cloud__Project_Task__c = projectsTasks[i].Id,
                                                 project_cloud__Date__c = Date.today().addMonths(i), project_cloud__Hours__c = i + 1));
            }
        }

        insert works;
      
        List<Expense_report__c> expenseReports = [
            SELECT
                Id, OwnerId, Expense_Report_Date__c, Project__c
            FROM
                Expense_report__c
        ];
                
        List<project_cloud__Work__c> allWorks =[
            SELECT
                Id, Expense_Report__c, project_cloud__Project_Task__r.project_cloud__Project_Phase__r.project_cloud__Project__c, project_cloud__Project__c
            FROM
                project_cloud__Work__c
        ];
        
//        System.assertEquals(5, expenseReports.size());
        for (Integer i = 0; i < works.size(); ++i) {
//            System.assertEquals(UserInfo.getUserId(), expenseReports[i].ownerId);
            
            if (i < 2) {
//                System.assertEquals(works[i].project_cloud__Date__c.year(), expenseReports[i].Expense_Report_Date__c.year()); 
//                System.assertEquals(works[i].project_cloud__Date__c.month(), expenseReports[i].Expense_Report_Date__c.month()); 
//                System.assertEquals(allWorks[i].project_cloud__Project_Task__r.project_cloud__Project_Phase__r.project_cloud__Project__c, expenseReports[i].Project__c);
            } else if (i == 2) {
//                System.assertEquals(works[i].project_cloud__Date__c.year(), expenseReports[i].Expense_Report_Date__c.year()); 
//                System.assertEquals(works[i].project_cloud__Date__c.month(), expenseReports[i].Expense_Report_Date__c.month()); 
//                System.assertEquals(allWorks[i].project_cloud__Project__c, expenseReports[i].Project__c);
            } else {
                Integer numberOfDays = Date.daysInMonth(Date.today().year(), Date.today().month() + i);
                Date lastDayOfMonth = Date.newInstance(Date.today().year(), Date.today().month() + i, numberOfDays);
//                System.assertEquals(Date.newInstance(Date.today().year(), Date.today().month() + i, lastDayOfMonth.day()), expenseReports[i].Expense_Report_Date__c);
//                System.assertEquals(allWorks[i].project_cloud__Project_Task__r.project_cloud__Project_Phase__r.project_cloud__Project__c, expenseReports[i].Project__c);
            }
//            System.assertEquals(expenseReports[i].Id, allWorks[i].Expense_Report__c);
        }

       List<project_cloud__Project__c> newProjects = new List<project_cloud__Project__c>();
        for (Integer i = 0; i < 2; ++i) {
            newProjects.add(new project_cloud__Project__c());
        }
        insert newProjects;

        List<project_cloud__Project__c> projects1 = [
            SELECT
                Id
            FROM
                project_cloud__Project__c
        ];
       
        Expense_report__c expenseReport = new Expense_report__c (
                    Expense_Report_Date__c = Date.today().addMonths(1), Project__c = projects1[5].Id,
                    Mileage_YTD__c = 0);   
        
        insert expenseReport;
        
        System.debug('size 1 = ' + [select Id from Expense_report__c].size());

        project_cloud__Work__c work = new project_cloud__Work__c(project_cloud__Project__c = projects1[6].Id,
                                                 project_cloud__Date__c = Date.today().addMonths(1), project_cloud__Hours__c = 2); 
        
        insert work;
        
        System.debug('size 2 = ' + [select Id from Expense_report__c].size());

        List<Expense_report__c> expenseReports1 = [
            SELECT
                Id, OwnerId, Expense_Report_Date__c, Project__c
            FROM
                Expense_report__c
        ];
        
        project_cloud__Work__c work1 =[
            SELECT
                Id, Expense_Report__c, project_cloud__Project_Task__r.project_cloud__Project_Phase__r.project_cloud__Project__c, project_cloud__Project__c
            FROM
                project_cloud__Work__c
            WHERE Id = :work.Id
        ];
        
//        System.assertEquals(7, expenseReports1.size());
//        System.assertEquals(expenseReports1[expenseReports1.size() - 1].Id, work1.Expense_Report__c); 
//        System.assertEquals(work1.project_cloud__Project__c, expenseReports1[expenseReports1.size() - 1].Project__c);
        Integer numberOfDays = Date.daysInMonth(Date.today().year(), Date.today().month() + 1);
        Date lastDayOfMonth = Date.newInstance(Date.today().year(), Date.today().month() + 1, numberOfDays);
//        System.assertEquals(Date.newInstance(Date.today().year(), Date.today().month() + 1, lastDayOfMonth.day()),
//                            expenseReports1[expenseReports1.size() - 1].Expense_Report_Date__c);
        
         
    }
}