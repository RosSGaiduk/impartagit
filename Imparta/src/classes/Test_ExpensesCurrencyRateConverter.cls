@isTest
public class Test_ExpensesCurrencyRateConverter {
    
    @testSetup
    public static void setup() {
        Account account = new Account(Name = 'Account');
        
        insert account;
        
        List<project_cloud__Project__c> projects = new List<project_cloud__Project__c>();
        projects.add(new project_cloud__Project__c(Name = 'Project1', CurrencyIsoCode = 'GBP', ccpe_ocp__Account__c = account.Id));
        projects.add(new project_cloud__Project__c(Name = 'Project2', CurrencyIsoCode = 'EUR', ccpe_ocp__Account__c = account.Id));
        
        insert projects;
    }
    
    
    @isTest
    public static void createExpensesTest() {
        
        List<CurrencyType> currencyTypes = [
            SELECT 
                ISOCode, ConversionRate
            FROM 
                CurrencyType 
            WHERE 
                IsActive = TRUE
        ];
        
        System.assertNotEquals(0, currencyTypes.size());
        
        List<project_cloud__Project__c> projects = [
            SELECT
                Id
            FROM
                project_cloud__Project__c
            ORDER BY Name
        ];
        
        
        List<project_cloud__Expense2__c> expenses = new List<project_cloud__Expense2__c>();
        expenses.add(new project_cloud__Expense2__c(project_cloud__Project__c = projects.get(0).Id, 
            project_cloud__Date__c = Date.today().addDays(10), project_cloud__Description__c = 'Description',
            project_cloud__Amount__c = 100, CurrencyIsoCode = 'USD'
        ));
        
        expenses.add(new project_cloud__Expense2__c(project_cloud__Project__c = projects.get(0).Id, 
            project_cloud__Date__c = Date.today().addDays(10), project_cloud__Description__c = 'Description',
            project_cloud__Amount__c = 100, CurrencyIsoCode = 'GBP'
        ));
        
        expenses.add(new project_cloud__Expense2__c(project_cloud__Project__c = projects.get(1).Id, 
            project_cloud__Date__c = Date.today().addDays(10), project_cloud__Description__c = 'Description',
            project_cloud__Amount__c = 100, CurrencyIsoCode = 'USD'
        ));
        
        expenses.add(new project_cloud__Expense2__c(project_cloud__Project__c = projects.get(1).Id, 
            project_cloud__Date__c = Date.today().addDays(10), project_cloud__Description__c = 'Description',
            project_cloud__Amount__c = 100, CurrencyIsoCode = 'EUR'
        ));
        
        insert expenses;
        
        expenses = [
            SELECT
                Id, project_cloud__Amount__c, CurrencyIsoCode, project_cloud__Project__c
            FROM
                project_cloud__Expense2__c
            WHERE
                Id IN: expenses
                AND CurrencyIsoCode != 'USD'
        ];
        
        System.assertEquals(4, expenses.size());
        
        System.debug('JSON result from test: ' + JSON.serialize(expenses));
    }
    
}