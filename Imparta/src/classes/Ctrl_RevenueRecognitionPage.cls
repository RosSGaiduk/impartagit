/*
Created By : Lister
Purpose : Custom Revenue Items page to create edit and delete quantity of revenue items
Test Class : Test_RevenueRecognitionOppCustom
*/
public with sharing class Ctrl_RevenueRecognitionPage {
	Id opportunityId;
	public Opportunity opportunityRec{get;set;}
	public List<RevenueRecogWrapper> listRevenueRecog{get;set;}
	public List<String> monthYearString{get;set;}
	public List<String> month3Char = new List<String>{'Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'};
	public map<Id,OpportunityLineItem> mapIdToOppLineItem{get;set;}
	public map<Id,Map<String,Revenue_Item__c>> mapOppLineItemIdToMapRevItem{get;set;}
	public String ErrorTitle {get;set;}
	public String ErrorSubtitle {get;set;}
	public Boolean ShowErrorSection {get;set;}
	public boolean NoLineItems {get;set;}
	public Boolean EditMode {get;set;}
	public Boolean isEditable {get;set;}
	public Boolean showQuantity{get;set;}
	public Boolean showAmount{get;set;}
	public Integer ProjectDurationMonths{get;set;}
	public Date ProjectStartDate{get;set;}

	public Ctrl_RevenueRecognitionPage(ApexPages.StandardController sc){
		ShowErrorSection = false;
		ErrorTitle = 'Test';
		ErrorSubtitle = 'Test';
		NoLineItems = true;
		EditMode = false;
		isEditable = true;
		showQuantity = true;
		showAmount = true;

		opportunityId = sc.getId();
		intializePage();
	}

	public void intializePage() {
		opportunityRec = [
			SELECT
				Id, Name, project_start_date__c, CloseDate, Project_Duration_Months__c
			FROM
				Opportunity
			WHERE
				Id = :opportunityId
		];

		ProjectDurationMonths = opportunityRec.Project_Duration_Months__c != null ? Integer.valueOf(opportunityRec.Project_Duration_Months__c) : 12;

		ProjectStartDate = opportunityRec.project_start_date__c != null ? opportunityRec.project_start_date__c : opportunityRec.closeDate;

		System.debug('opportunityRec ::'+opportunityRec );
		listRevenueRecog = new List<RevenueRecogWrapper>();
		mapIdToOppLineItem = new Map<Id,OpportunityLineItem>();
		mapOppLineItemIdToMapRevItem = new map<Id,Map<String,Revenue_Item__c>>();
		//query all products
		for(OpportunityLineItem opplineitem : [Select Id, Name, Quantity, TotalPrice, UnitPrice, Cost__c, Start_Date__c, CurrencyIsoCode,
																			Product2Id, Product2.Name, Line_Probability__c, Product2.Sort_Order__c, Actual_Activity__c
																			from OpportunityLineItem
																			where OpportunityId = : opportunityId]) {
			mapIdToOppLineItem.put(opplineitem.Id, opplineitem);
		}

		System.debug('mapIdToOppLineItem::'+mapIdToOppLineItem);
		if(mapIdToOppLineItem.keyset().size() > 0)
			NoLineItems = false;

		//query all revenue items
		for(Revenue_Item__c revItem : [Select Id, Name, Cost__c, Quantity__c, Date__c, Opportunity_Product_Id__c, Amount__c, Project__c, Project_Task__c
																			from Revenue_Item__c
																			where Opportunity_Product_Id__c like : mapIdToOppLineItem.keySet()]) {
			Id tempOppProdId = revItem.Opportunity_Product_Id__c;
			String revenueDateString = revItem.Date__c.month() + '' + revItem.Date__c.year();
                                                                                
			if (mapOppLineItemIdToMapRevItem.containsKey(tempOppProdId)) {
				System.debug('revItem::'+revItem);
                if (mapOppLineItemIdToMapRevItem.get(tempOppProdId).containsKey(revenueDateString)) {
                    Revenue_Item__c sameDateRevItem = mapOppLineItemIdToMapRevItem.get(tempOppProdId).get(revenueDateString);
                    sameDateRevItem.Quantity__c = sameDateRevItem.Quantity__c + revItem.Quantity__c;
                    sameDateRevItem.Amount__c = sameDateRevItem.Amount__c + revItem.Amount__c;
                } else {
                	mapOppLineItemIdToMapRevItem.get(tempOppProdId).put(revenueDateString, revItem);
                }
			} else {
				Map<String,Revenue_Item__c> tempMonthYearToRevItem = new Map<String,Revenue_Item__c>();
				tempMonthYearToRevItem.put(revenueDateString, revItem);
				mapOppLineItemIdToMapRevItem.put(tempOppProdId,tempMonthYearToRevItem);
			}

			isEditable = isEditable && revItem.Project_Task__c == null;
		}
		System.debug('mapOppLineItemIdToMapRevItem::'+mapOppLineItemIdToMapRevItem);
		//construct wrapper
		monthYearString = new List<String>();
		System.debug('month3Char::'+month3Char);
		for (integer i=0; i < ProjectDurationMonths; i++) {
			System.debug('::'+(ProjectStartDate.addmonths(i).month()-1));
			monthYearString.add(month3Char[ProjectStartDate.addmonths(i).month()-1]+'-'+ProjectStartDate.addmonths(i).year());
		}
		System.debug('monthYearString::'+monthYearString);

		for (Id opplineItemId : mapIdToOppLineItem.keySet()) {
			RevenueRecogWrapper revrecwrap = new RevenueRecogWrapper();
			revrecwrap.oppLineItem = mapIdToOppLineItem.get(opplineItemId);
			for (integer i=0; i < ProjectDurationMonths; i++) {
				String monthYear = ProjectStartDate.addmonths(i).month()+''+ProjectStartDate.addmonths(i).year();
				System.debug('monthyear:'+i+'::'+monthYear);
				if (mapOppLineItemIdToMapRevItem.containsKey(opplineItemId) && mapOppLineItemIdToMapRevItem.get(opplineItemId).containsKey(monthYear)) {
					revrecwrap.listRevRecItems.add(mapOppLineItemIdToMapRevItem.get(opplineItemId).get(monthYear));
					System.debug('revrecwrap::i'+revrecwrap);
				} else {
					Revenue_Item__c newRevItem = createRevItem(ProjectStartDate.addmonths(i), OpportunityId, opplineItemId, revrecwrap.oppLineItem.UnitPrice,revrecwrap.oppLineItem.Cost__c,revrecwrap.oppLineItem.Product2Id);
					revrecwrap.listRevRecItems.add(newRevItem);
				}
			}

			listRevenueRecog.add(revrecwrap);
		}
		System.debug('listRevenueRecog::'+listRevenueRecog);
		listRevenueRecog.sort();
	}


	public PageReference RedirectToParent() {
		PageReference PageRef = new PageReference('/' + opportunityRec.Id);
		return PageRef;
	}

	public void ChangeEditMode() {
		if (EditMode)
			EditMode = false;
		else
			EditMode = true;
	}

	//Create a new revenue item
	public Revenue_Item__c createRevItem(Date revdate, Id oppId, Id oppProdId , Decimal amount, Decimal cost, Id productId) {
		Revenue_Item__c newRevItem = new Revenue_Item__c();
		newRevItem.Date__c = revdate.addMonths(1).toStartofMonth().addDays(-1);
		newRevItem.Opportunity__c = oppId;
		newRevItem.Opportunity_Product_Id__c = oppProdId;
		newRevItem.Product__c = productId;
		newRevItem.Quantity__c = null;
		newRevItem.Cost__c = null;
		newRevItem.amount__c = null;

		return newRevItem;
	}

	//compute amount and cost for revenue items
	public Revenue_Item__c computeValues(Revenue_Item__c newRevItem,Decimal opplineAmount, Decimal opplineCost) {
		newRevItem.Cost__c = 0;
		newRevItem.amount__c = 0;
		if (newRevItem.Quantity__c != null) {
			if (opplineCost != null)
				newRevItem.Cost__c = opplineCost * newRevItem.Quantity__c;
			if (opplineAmount != null)
				newRevItem.amount__c = opplineAmount * newRevItem.Quantity__c;
		}

		return newRevItem;
	}

	public void saveRevenueItems() {
		ShowErrorSection = false;
		List<Revenue_Item__c> listRevenueItemToUpsert = new List<Revenue_Item__c>();
		List<Revenue_Item__c> listRevenueItemToDelete = new List<Revenue_Item__c>();
		List<OpportunityLineItem> listOppProdToUpsert = new List<OpportunityLineItem>();
		SavePoint sp = Database.setsavepoint();
		for (RevenueRecogWrapper revrecwrap : listRevenueRecog) {
			Double sumQuantity = 0;
			Boolean isFirst = true;
			for (Revenue_item__c revItem : revrecwrap.listRevRecItems) {
				if(revItem.Quantity__c != null)
					sumQuantity += revItem.Quantity__c;
			}
			Decimal TotalQuantity = (revrecwrap.oppLineItem.Quantity).setScale(2, System.RoundingMode.HALF_UP);
			if (sumQuantity != TotalQuantity) {
				ErrorSubtitle = 'Please ensure that Sum of Revenue Items Quantity is equal to the Opportunity Product Total Quantity for ' +
				revrecwrap.oppLineItem.Name + '. Adjust the difference by '+(TotalQuantity - sumQuantity);
				ShowErrorSection = true;
				ErrorTitle = 'Error in Saving the Record.';
				break;
			}
			for (Revenue_item__c revItem : revrecwrap.listRevRecItems) {
				if((revItem.Quantity__c == null || revItem.Quantity__c == 0) && revItem.Id != null)
					listRevenueItemToDelete.add(revItem);
				else if(revItem.Quantity__c != null && revItem.Quantity__c != 0) {
					Revenue_Item__c currentRevrec = revItem;
					currentRevrec = computeValues(revItem,revrecwrap.oppLineItem.UnitPrice,revrecwrap.oppLineItem.Cost__c);
					currentRevrec.Revenue_Probability__c = revrecwrap.oppLineItem.Line_Probability__c;
					listRevenueItemToUpsert.add(currentRevrec);
					system.debug('::currentRevrec'+currentRevrec);
					if (isFirst) {
						revrecwrap.oppLineItem.Start_Date__c = currentRevrec.Date__c;
						isFirst = false;
					}
				}

			}
			OppProductTriggerHandler.oppLineItemTrigger = false;
			listOppProdToUpsert.add(revrecwrap.oppLineItem);
		}
		try {
			system.debug('::upsertlist'+listRevenueItemToUpsert);

			if (listRevenueItemToUpsert.size()>0 && !ShowErrorSection)
				upsert listRevenueItemToUpsert;
			if (listRevenueItemToDelete.size()>0 && !ShowErrorSection)
				delete listRevenueItemToDelete;
			if (listOppProdToUpsert.size()>0)
				upsert listOppProdToUpsert;

			if (ShowErrorSection) {
				EditMode = true;
			} else {
				EditMode = false;
				intializePage();
			}
		} catch(Exception e) {
			ShowErrorSection = true;
			ErrorTitle = 'Error in Saving the Record.';
			ErrorSubtitle = e.getMessage();
			Database.rollback(sp);
		}
	}

	//Wrapper class for revenue recog
	public class RevenueRecogWrapper implements Comparable {
		public Map<String,Revenue_Item__c> mapMonthYearToRevRecItems{get;set;}
		public List<Revenue_Item__c> listRevRecItems{get;set;}
		public OpportunityLineItem oppLineItem{get;set;}

		public RevenueRecogWrapper() {
			mapMonthYearToRevRecItems = new Map<String,Revenue_Item__c>();
			listRevRecItems = new List<Revenue_item__c>();
			oppLineItem = new OpportunityLineItem();
		}

		// Implement the compareTo() method
		public Integer compareTo(Object compareTo) {
			RevenueRecogWrapper compareToEmp = (RevenueRecogWrapper)compareTo;
			if (oppLineItem.Product2.Sort_Order__c == compareToEmp.oppLineItem.Product2.Sort_Order__c) return 0;
			if (oppLineItem.Product2.Sort_Order__c > compareToEmp.oppLineItem.Product2.Sort_Order__c) return 1;
			return -1;
		}
	}

}