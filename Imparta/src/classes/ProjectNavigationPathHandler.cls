public without sharing class ProjectNavigationPathHandler {
    @AuraEnabled
    public static String getProjectId(String recordId){
        String sObjName = ((Id)recordId).getSObjectType().getDescribe().getName();
        switch on sObjName {
            when 'project_cloud__Project__c' {
                return recordId;
            }
            when 'project_cloud__Project_Phase__c' {
                return getId(sObjName, 'project_cloud__Project__c', recordId);
            }
            when 'project_cloud__Project_Task__c' {
                sObject record = Database.query('SELECT project_cloud__Project_Phase__r.project_cloud__Project__c FROM project_cloud__Project_Task__c WHERE Id = :recordId');
                return (String)record.getSobject('project_cloud__Project_Phase__r').get('project_cloud__Project__c');
            }
            when 'project_cloud__Ticket__c' {
                return getId(sObjName, 'project_cloud__Project_CCPE_Ignore__c', recordId);
            }
        }
        return null;
    }
    @AuraEnabled
    public static String getId(String objName, String fieldName, String recordId) {
        SObject record = Database.query('SELECT ' + fieldName + ' FROM ' + objName + ' WHERE Id = \'' + recordId + '\'');
        return (String)record.get(fieldName);
    }
}