@isTest
private class testTrgDeleteProjectPhase {

    private static testmethod void oppProjectDateUpdation() {
        Date startdate = date.Today();
        startdate = startdate.toStartofWeek();
        Account a = new account();
        a.name = 'Test1';
        a.CurrencyIsoCode = 'GBP';
        insert a;
        list < OpportunityLineItem > oppLineItemList = new list < OpportunityLineItem > ();
        list < OpportunityLineItem > oppLineItemUpdateList = new list < OpportunityLineItem > ();

        project_cloud__Project__c proj = new project_cloud__Project__c();
        proj.Name = 'test project';
        proj.ccpe_ocp__Account__c = a.Id;
        insert proj;

        project_cloud__Project_Sharing__c ps = [SELECT Id FROM project_cloud__Project_Sharing__c WHERE project_cloud__Project__c =: proj.Id Limit 1];

        project_cloud__Project_Phase__c phase1 = new project_cloud__Project_Phase__c();
        phase1.Name = 'Phase 1';
        phase1.project_cloud__Project__c = proj.Id;
        insert phase1;

        project_cloud__Project_Phase__c phase2 = new project_cloud__Project_Phase__c();
        phase2.Name = 'Phase 2';
        phase2.project_cloud__Project__c = proj.Id;
        insert phase2;
        
        project_cloud__Project_Phase__c phase3 = new project_cloud__Project_Phase__c();
        phase3.Name = 'Phase 3';
        phase3.project_cloud__Project__c = proj.Id;
        insert phase3;

        project_cloud__Project_Task__c task1 = new project_cloud__Project_Task__c();
        task1.Name = 'Task 1';
        task1.project_cloud__Project_Phase__c = phase1.Id;
        task1.project_cloud__Autonomous_Start__c = startdate;
        task1.project_cloud__Duration__c = 5;
        task1.project_cloud__Estimated_Hours__c = 20;
        insert task1;

        project_cloud__Project_Task__c task2 = new project_cloud__Project_Task__c();
        task2.Name = 'Task 2';
        task2.project_cloud__Project_Phase__c = phase1.Id;
        task2.project_cloud__Autonomous_Start__c = startdate;
        task2.project_cloud__Duration__c = 5;
        task2.project_cloud__Estimated_Hours__c = 20;
        insert task2;

        project_cloud__Project_Task_Dependency__c dep1 = new project_cloud__Project_Task_Dependency__c();
        dep1.project_cloud__Child__c = task2.Id;
        dep1.project_cloud__Parent__c = task1.Id;
        insert dep1;

        project_cloud__Project_Sub_Task__c subtask1 = new project_cloud__Project_Sub_Task__c();
        subtask1.project_cloud__Short_Description__c= 'Sub Task 1';
        subtask1.project_cloud__Project_Task__c = task1.Id;
        subTask1.project_cloud__Project_Sharing__c = ps.Id;

        insert subtask1; 

        project_cloud__Project_Sub_Task__c subtask2 = new project_cloud__Project_Sub_Task__c();
        subtask2.project_cloud__Short_Description__c= 'Sub Task 1';
        subtask2.project_cloud__Project_Task__c = task1.Id;
        subTask2.project_cloud__Project_Sharing__c = ps.Id;

        insert subtask2; 
         
system.debug('Task1 = ' + task1);
system.debug('Task2 = ' + task2);

        test.startTest();
        
        delete phase1;
        

    }
}