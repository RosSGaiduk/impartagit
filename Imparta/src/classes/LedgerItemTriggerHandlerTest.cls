@isTest(SeeAllData=true)
public class LedgerItemTriggerHandlerTest {

	@isTest
	static void testInsertLedgerItemWithNoAccInCustSett() {

		List<Revenue_Ledger_Accounts__c> custSettLedgAccs = [
				SELECT
					Ledger_Account_Id__c
				FROM
					Revenue_Ledger_Accounts__c
		];

		delete custSettLedgAccs;

		s2cor__Sage_ACC_Ledger_Entry__c testLedgEntry = createLedgEntry();
		s2cor__Sage_COR_Currency__c testCurr = createCurrency('c*2', 'n*2');

		List<s2cor__Sage_ACC_Ledger_Account__c> ledgerAccounts = [
				SELECT
					Id
				FROM
					s2cor__Sage_ACC_Ledger_Account__c
				LIMIT 1
		];

		s2cor__Sage_ACC_Ledger_Account__c testLedgerAcc = new s2cor__Sage_ACC_Ledger_Account__c(
				Name = 'Test acc',
				s2cor__Parent_Type__c = 'Class',
				s2cor__UID__c = '*123',
				s2cor__Parent__c = !ledgerAccounts.isEmpty() ? ledgerAccounts[0].Id : null
		);
		insert testLedgerAcc;

		s2cor__Sage_ACC_Ledger_Item__c testLedgItem = new s2cor__Sage_ACC_Ledger_Item__c(
				s2cor__Ledger_Entry__c = testLedgEntry.Id,
				s2cor__Ledger_Account__c = testLedgerAcc.Id,
				s2cor__Currency__c = testCurr.Id,
				s2cor__Base_Debit__c = 1
		);
		insert testLedgItem;

		List<Revenue_Item__c> revItems = [
				SELECT
					Id
				FROM
					Revenue_Item__c
				WHERE
					Ledger_Item__c = :testLedgItem.Id
		];

		System.assertEquals(0, revItems.size());
	}

	@isTest
	static void testInsertLedgerItemWithAccInCustSett() {

		s2cor__Sage_ACC_Ledger_Entry__c testLedgEntry = createLedgEntry();
		s2cor__Sage_COR_Currency__c testCurr = createCurrency('c*2', 'n*2');

		List<s2cor__Sage_ACC_Ledger_Account__c> ledgerAccounts = [
				SELECT
					Id
				FROM
					s2cor__Sage_ACC_Ledger_Account__c
				LIMIT 1
		];

		s2cor__Sage_ACC_Ledger_Account__c testLedgerAcc = new s2cor__Sage_ACC_Ledger_Account__c(
				Name = 'Test acc',
				s2cor__Parent_Type__c = 'Class',
				s2cor__UID__c = '*123',
				s2cor__Parent__c = !ledgerAccounts.isEmpty() ? ledgerAccounts[0].Id : null
		);
		insert testLedgerAcc;

		Revenue_Ledger_Accounts__c custSettAcc = new Revenue_Ledger_Accounts__c(
				Name =	'Test acc id',
				Ledger_Account_Id__c = testLedgerAcc.Id
		);
		insert custSettAcc;

		s2cor__Sage_ACC_Ledger_Item__c testLedgItem = new s2cor__Sage_ACC_Ledger_Item__c(
				s2cor__Ledger_Entry__c = testLedgEntry.Id,
				s2cor__Ledger_Account__c = testLedgerAcc.Id,
				s2cor__Currency__c = testCurr.Id,
				s2cor__Base_Debit__c = 1
		);
		insert testLedgItem;

		List<Revenue_Item__c> revItems = [
				SELECT
					Id
				FROM
					Revenue_Item__c
				WHERE
					Ledger_Item__c = :testLedgItem.Id
		];

		System.assertEquals(1, revItems.size());
	}

	@isTest
	static void testUpdateLedgerItem() {
		//old account in custom setting and new account NOT in custom setting

		s2cor__Sage_ACC_Ledger_Entry__c testLedgEntry = createLedgEntry();
		s2cor__Sage_COR_Currency__c testCurr = createCurrency('c*2', 'n*2');

		List<s2cor__Sage_ACC_Ledger_Account__c> ledgerAccounts = [
				SELECT
					Id
				FROM
					s2cor__Sage_ACC_Ledger_Account__c
				LIMIT 1
		];

		List<s2cor__Sage_ACC_Ledger_Account__c> testLedgerAccounts = new List<s2cor__Sage_ACC_Ledger_Account__c>();
		for (Integer i = 0; i < 2; i++) {
			s2cor__Sage_ACC_Ledger_Account__c testLedgerAcc = new s2cor__Sage_ACC_Ledger_Account__c(
					Name = 'Test acc',
					s2cor__Parent_Type__c = 'Class',
					s2cor__UID__c = '*123' + i,
					s2cor__Parent__c = !ledgerAccounts.isEmpty() ? ledgerAccounts[0].Id : null
			);
			testLedgerAccounts.add(testLedgerAcc);
		}
		insert testLedgerAccounts;

		Revenue_Ledger_Accounts__c custSettAcc = new Revenue_Ledger_Accounts__c(
				Name =	'Test acc id',
				Ledger_Account_Id__c = testLedgerAccounts[0].Id
		);
		insert custSettAcc;

		s2cor__Sage_ACC_Ledger_Item__c testLedgItem = new s2cor__Sage_ACC_Ledger_Item__c(
				s2cor__Ledger_Entry__c = testLedgEntry.Id,
				s2cor__Ledger_Account__c = testLedgerAccounts[0].Id,
				s2cor__Currency__c = testCurr.Id,
				s2cor__Base_Debit__c = 1
		);
		insert testLedgItem;

		Revenue_Item__c testRevItem = new Revenue_Item__c(
				Ledger_Item__c = testLedgItem.Id,
				Amount__c = 1
		);
		insert testRevItem;

		testLedgItem.s2cor__Ledger_Account__c = testLedgerAccounts[1].Id;
		update testLedgItem;

		List<Revenue_Item__c> revItems = [
				SELECT
					Id
				FROM
					Revenue_Item__c
				WHERE
					Ledger_Item__c = :testLedgItem.Id
		];

		System.assertEquals(0, revItems.size());
	}

	@isTest
	static void testUpdateLedgerItem2() {
		//old and new account in custom setting

		s2cor__Sage_ACC_Ledger_Entry__c testLedgEntry = createLedgEntry();
		s2cor__Sage_COR_Currency__c testCurr = createCurrency('c*2', 'n*2');

		List<s2cor__Sage_ACC_Ledger_Account__c> ledgerAccounts = [
				SELECT
					Id
				FROM
					s2cor__Sage_ACC_Ledger_Account__c
				LIMIT 1
		];

		List<s2cor__Sage_ACC_Ledger_Account__c> testLedgerAccounts = new List<s2cor__Sage_ACC_Ledger_Account__c>();
		for (Integer i = 0; i < 2; i++) {
			s2cor__Sage_ACC_Ledger_Account__c testLedgerAcc = new s2cor__Sage_ACC_Ledger_Account__c(
					Name = 'Test acc',
					s2cor__Parent_Type__c = 'Class',
					s2cor__UID__c = '*123' + i,
					s2cor__Parent__c = !ledgerAccounts.isEmpty() ? ledgerAccounts[0].Id : null
			);
			testLedgerAccounts.add(testLedgerAcc);
		}
		insert testLedgerAccounts;

		List<Revenue_Ledger_Accounts__c> custSettAccs = new List<Revenue_Ledger_Accounts__c>();
		for(Integer i = 0; i < testLedgerAccounts.size(); i++){
			Revenue_Ledger_Accounts__c custSettAcc = new Revenue_Ledger_Accounts__c(
					Name =	'Test acc id ' + i,
					Ledger_Account_Id__c = testLedgerAccounts[i].Id
			);
			custSettAccs.add(custSettAcc);
		}
		insert custSettAccs;

		s2cor__Sage_ACC_Ledger_Item__c testLedgItem = new s2cor__Sage_ACC_Ledger_Item__c(
				s2cor__Ledger_Entry__c = testLedgEntry.Id,
				s2cor__Ledger_Account__c = testLedgerAccounts[0].Id,
				s2cor__Currency__c = testCurr.Id,
				s2cor__Base_Credit__c = 1
		);
		insert testLedgItem;

		Revenue_Item__c revItem = [
				SELECT
					Id, Quantity__c, Amount__c, Date__c
				FROM
					Revenue_Item__c
				WHERE
					Ledger_Item__c = :testLedgItem.Id
		];
		revItem.Quantity__c = 2;
		revItem.Amount__c = 5;
		revItem.Date__c = Date.today().addDays(2);
		update revItem;

		testLedgItem.s2cor__Ledger_Account__c = testLedgerAccounts[1].Id;
		update testLedgItem;

		Revenue_Item__c revItemAfterUpdate = [
				SELECT
					Id, Quantity__c, Amount__c, Date__c
				FROM
					Revenue_Item__c
				WHERE
					Ledger_Item__c = :testLedgItem.Id
		];

		System.assertEquals(1, revItemAfterUpdate.Quantity__c);
		System.assertEquals(1, revItemAfterUpdate.Amount__c);
		System.assertEquals(Date.today(), revItemAfterUpdate.Date__c);
	}

	@isTest
	static void testDeleteLedgerItem() {
		s2cor__Sage_ACC_Ledger_Entry__c testLedgEntry = createLedgEntry();
		s2cor__Sage_COR_Currency__c testCurr = createCurrency('c*2', 'n*2');

		List<s2cor__Sage_ACC_Ledger_Account__c> ledgerAccounts = [
				SELECT
					Id
				FROM
					s2cor__Sage_ACC_Ledger_Account__c
				LIMIT 1
		];

		s2cor__Sage_ACC_Ledger_Account__c testLedgerAcc = new s2cor__Sage_ACC_Ledger_Account__c(
				Name = 'Test acc',
				s2cor__Parent_Type__c = 'Class',
				s2cor__UID__c = '*123',
				s2cor__Parent__c = !ledgerAccounts.isEmpty() ? ledgerAccounts[0].Id : null
		);
		insert testLedgerAcc;

		s2cor__Sage_ACC_Ledger_Item__c testLedgItem = new s2cor__Sage_ACC_Ledger_Item__c(
				s2cor__Ledger_Entry__c = testLedgEntry.Id,
				s2cor__Ledger_Account__c = testLedgerAcc.Id,
				s2cor__Currency__c = testCurr.Id,
				s2cor__Base_Debit__c = 1
		);
		insert testLedgItem;

		Revenue_Item__c testRevItem = new Revenue_Item__c(
				Ledger_Item__c = testLedgItem.Id,
				Amount__c = 1
		);
		insert testRevItem;

		delete testLedgItem;

		List<Revenue_Item__c> revItems = [
				SELECT
					Id
				FROM
					Revenue_Item__c
				WHERE
					Id = :testRevItem.Id
		];

		System.assertEquals(0, revItems.size());
	}

	private static s2cor__Sage_ACC_Ledger_Entry__c createLedgEntry() {
		s2cor__Sage_COR_Company__c testCompany = createCompany();

		s2cor__Sage_ACC_Ledger_Entry__c testLedgEntry = new s2cor__Sage_ACC_Ledger_Entry__c(
				s2cor__Date__c = Date.today(),
				s2cor__Company__c = testCompany.Id
		);
		insert testLedgEntry;

		return testLedgEntry;
	}

	private static s2cor__Sage_COR_Company__c createCompany() {
		s2cor__Sage_COR_Currency__c testCurrency = createCurrency('c*1', 'n*1');
		s2cor__Sage_COR_Legislation__c testLegislation = createLegislation();

		s2cor__Sage_COR_Company__c testCompany = new s2cor__Sage_COR_Company__c(
				Name = 'Test company',
				s2cor__Base_Currency__c = testCurrency.Id,
				s2cor__Legislation__c = testLegislation.Id
		);
		insert testCompany;

		return testCompany;
	}

	private static s2cor__Sage_COR_Currency__c createCurrency(String curCode, String numCode) {
		s2cor__Sage_COR_Currency__c testCurr = new s2cor__Sage_COR_Currency__c(
				Name = 'Test currency',
				s2cor__Currency_Code__c	 = curCode,
				s2cor__Numeric_Code__c = numCode,
				s2cor__Minor_Unit__c = 1
		);
		insert testCurr;

		return testCurr;
	}

	private static s2cor__Sage_COR_Legislation__c createLegislation() {
		s2cor__Sage_COR_Legislation__c testLegis = new s2cor__Sage_COR_Legislation__c(
				Name = 'Test legislation'
		);
		insert testLegis;

		return testLegis;
	}
}