@IsTest
public class AllocationTriggerHandlerTest {

    @isTest
    static void testCreateAllocation() {
        project_cloud__Project__c testProject = new project_cloud__Project__c(Name = 'testProject');
        insert testProject;

        project_cloud__Project_Phase__c testPhase = new project_cloud__Project_Phase__c(
                Name = 'testPhase', project_cloud__Project__c = testProject.Id
        );
        insert testPhase;

        project_cloud__Work_Type__c testWorkType = new project_cloud__Work_Type__c(
                Name = 'testWorkType',ccpe_r__Default_Skill_Cost__c = 80
        );
        insert testWorkType;

        project_cloud__Project_Task__c testTaskWithoutProducts = new project_cloud__Project_Task__c(
                Name = 'testTaskWithoutProducts', project_cloud__Autonomous_Start__c = Date.today().toStartOfWeek(),
                project_cloud__Project_Phase__c = testPhase.Id, project_cloud__Work_Type__c = testWorkType.Id,
                project_cloud__Duration__c = 2,project_cloud__Estimated_Hours__c = 8
        );
        insert testTaskWithoutProducts;

        Profile testProfile = [SELECT Id FROM Profile WHERE Name='Standard User'];

        User testUser = new User(Alias = 'tStUser', Email = 'testStandarduser@testorg.com',
                EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US', ProfileId = testProfile.Id,
                TimeZoneSidKey = 'America/Los_Angeles', UserName = 'testStandartUser@gmail.com', Cost_Rate__c = 12,
                User_External_Id__c = 'testExternal'
        );
        insert testUser;

        project_cloud__Allocation_Summary__c testAllocationSummary = new project_cloud__Allocation_Summary__c(
                project_cloud__Project_Task__c = testTaskWithoutProducts.Id,
                project_cloud__Project__c = testProject.Id
        );
        insert testAllocationSummary;

        project_cloud__Allocation_Group__c testAllocationGroup = new project_cloud__Allocation_Group__c(
                project_cloud__Allocation_Summary__c = testAllocationSummary.Id,
                project_cloud__Work_Type__c = testWorkType.Id
        );
        insert testAllocationGroup;


        Cost_Rate__c testCostRate = new Cost_Rate__c(
                Project__c = testProject.Id, Work_Type__c = testWorkType.Id, Resource__c = testUser.Id,
                Cost_Rate__c = 15
        );
        insert testCostRate;

        project_cloud__Allocation__c testAllocation = new project_cloud__Allocation__c(
                project_cloud__Allocation_Group__c = testAllocationGroup.Id, project_cloud__User__c = testUser.Id
        );
        insert testAllocation;

        project_cloud__Allocation__c newTestAllocation = [
            SELECT
                Id, Cost_Rate__c
            FROM
                project_cloud__Allocation__c
            WHERE
                Id = :testAllocation.Id
        ];
        System.assertEquals(testCostRate.Cost_Rate__c, newTestAllocation.Cost_Rate__c * 8);

        project_cloud__Project_Task__c newTestTaskWithoutProducts = [
            SELECT
                Id, Budgeted_Cost__c, Calculated_Cost__c
            FROM
                project_cloud__Project_Task__c
            WHERE
                Id = :testTaskWithoutProducts.Id
        ];
        System.assertNotEquals(null, newTestTaskWithoutProducts.Budgeted_Cost__c);
    }

}