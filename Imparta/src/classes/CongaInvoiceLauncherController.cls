public class CongaInvoiceLauncherController{

    Map<String, Id> congaSolutionQueryMap = new Map<String, Id>();
    public Id CompanyDets2Id {get; set;} 
    public Id LineItemsId {get; set;}
    public Id salesInvoiceTemplateId {get; set;}
    public Id salesInvoiceEmailTemplateId {get; set;}    
    public Id CompanyDets2IdQ {get; set;} 
    public Id LineItemsIdQ {get; set;}    
    public Id salesQuoteTemplateId {get; set;}
    public Id salesQuoteEmailTemplateId {get; set;}
    private final s2cor__Sage_INV_Trade_Document__c invoiceRecord;    

    /* Default Constructor */
    public CongaInvoiceLauncherController(){       
        populateCongaSolutionQueryMap();
        findSalesInvoiceTemplateId();
        findSalesInvoiceEmailTemplateId();
        findSalesQuoteTemplateId();
        findSalesQuoteEmailTemplateId();        
    }
    
    public CongaInvoiceLauncherController(ApexPages.StandardController stdController) {
        this.invoiceRecord = (s2cor__Sage_INV_Trade_Document__c)stdController.getRecord();            
        populateCongaSolutionQueryMap();
        findSalesInvoiceTemplateId();
        findSalesInvoiceEmailTemplateId();
        findSalesQuoteTemplateId();
        findSalesQuoteEmailTemplateId();           
    }
    
    private void populateCongaSolutionQueryMap(){
        String queryAlias;
        
        for(APXTConga4__Conga_Solution_Query__c csq : [SELECT Id, Name, APXTConga4__Conga_Query__c, APXTConga4__Conga_Query__r.Name, APXTConga4__Conga_Query__r.APXTConga4__Name__c, APXTConga4__Conga_Solution__c, APXTConga4__Conga_Solution__r.Name 
                                                      FROM APXTConga4__Conga_Solution_Query__c 
                                                      WHERE isDeleted = false AND (APXTConga4__Conga_Solution__r.Name = 'Conga Sales Invoice' OR APXTConga4__Conga_Solution__r.Name = 'Conga Sales Quote') LIMIT 25000])
        {
            
            if(csq.APXTConga4__Conga_Query__r.APXTConga4__Name__c.contains('[') && csq.APXTConga4__Conga_Query__r.APXTConga4__Name__c.contains(']'))            
            {
                queryAlias = csq.APXTConga4__Conga_Query__r.APXTConga4__Name__c.substring(csq.APXTConga4__Conga_Query__r.APXTConga4__Name__c.IndexOf('[')+1,csq.APXTConga4__Conga_Query__r.APXTConga4__Name__c.IndexOf(']'));
                congaSolutionQueryMap.put(queryAlias, csq.APXTConga4__Conga_Query__c);
            }            
            
        }
            system.debug('********************** congaSolutionQueryMap: ' + congaSolutionQueryMap);
            
            CompanyDets2Id = congaSolutionQueryMap.get('CompanyDets2');
                system.debug('********************** CompanyDets2Id: ' + CompanyDets2Id);    
            LineItemsId = congaSolutionQueryMap.get('LineItems');      
                system.debug('********************** LineItemsId: ' + LineItemsId);               
                
            CompanyDets2IdQ = congaSolutionQueryMap.get('CompanyDets2Q');
                system.debug('********************** CompanyDets2IdQ: ' + CompanyDets2IdQ);    
            LineItemsIdQ = congaSolutionQueryMap.get('LineItemsQ');      
                system.debug('********************** LineItemsIdQ: ' + LineItemsIdQ);                                                     
    }

    private void findSalesInvoiceTemplateId(){
        APXTConga4__Conga_Template__c salesInvoiceRecord = [SELECT Id FROM APXTConga4__Conga_Template__c 
                                        WHERE APXTConga4__Template_Group__c = 'Invoicing' 
                                        AND APXTConga4__Description__c LIKE 'Created By Solution Pack for Composer Solution: CSI (s2cor__Sage_INV_Trade_Document__c)%' 
                                        LIMIT 1];
        
        if(salesInvoiceRecord != null)
        {
            salesInvoiceTemplateId = salesInvoiceRecord.Id;
        }
            system.debug('********************** salesInvoiceTemplateId: ' + salesInvoiceTemplateId);   
    
    }
    
    private void findSalesQuoteTemplateId(){
        APXTConga4__Conga_Template__c salesQuoteRecord = [SELECT Id FROM APXTConga4__Conga_Template__c 
                                        WHERE APXTConga4__Template_Group__c = 'Quoting' 
                                        AND APXTConga4__Description__c LIKE 'Created By Solution Pack for Composer Solution: CSQ (s2cor__Sage_INV_Trade_Document__c)%' 
                                        LIMIT 1];
        
        salesQuoteTemplateId = salesQuoteRecord.Id;
            system.debug('********************** salesQuoteTemplateId: ' + salesQuoteTemplateId);   
    
    }    
    
    private void findSalesInvoiceEmailTemplateId(){
        APXTConga4__Conga_Email_Template__c salesInvoiceEmailRecord = [SELECT Id FROM APXTConga4__Conga_Email_Template__c 
                                        WHERE APXTConga4__Template_Group__c = 'Invoicing' 
                                        AND APXTConga4__Description__c = 'Created By Solution Pack for Composer Solution: CSI (s2cor__Sage_INV_Trade_Document__c)' 
                                        LIMIT 1];
        
        salesInvoiceEmailTemplateId = salesInvoiceEmailRecord.Id;
            system.debug('********************** salesInvoiceEmailTemplateId: ' + salesInvoiceEmailTemplateId);   
    
    }
    
    private void findSalesQuoteEmailTemplateId(){
        APXTConga4__Conga_Email_Template__c salesQuoteEmailRecord = [SELECT Id FROM APXTConga4__Conga_Email_Template__c 
                                        WHERE APXTConga4__Template_Group__c = 'Quoting' 
                                        AND APXTConga4__Description__c = 'Created By Solution Pack for Composer Solution: CSQ (s2cor__Sage_INV_Trade_Document__c)' 
                                        LIMIT 1];
        
        salesQuoteEmailTemplateId = salesQuoteEmailRecord.Id;
            system.debug('********************** salesQuoteEmailTemplateId: ' + salesQuoteEmailTemplateId);   
    
    }


}