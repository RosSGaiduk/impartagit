public class ProjectCloudWorkTriggerHandler {
    
    public static void handlerBeforeInsert(List<project_cloud__Work__c> works) {
        
        populateWorkWithCostRate(works);
        
    }
    
    private static Id getCurrentUserId() {
        
        return  UserInfo.getUserId(); 
    }
    
    private static Map<Id, project_cloud__Project_Task__c> getMapOfProjectsTasks() {
        
         Map<Id, project_cloud__Project_Task__c> projectsTasksMap = new Map<Id, project_cloud__Project_Task__c>(
            [SELECT Id, project_cloud__Project_Phase__r.project_cloud__Project__c, project_cloud__Work_Type__c FROM project_cloud__Project_Task__c]
        );
        
        return projectsTasksMap;
    }
    
    private static List<Expense_Report__c> getExpenseReports(List<project_cloud__Work__c> works) {
            Map<Id, project_cloud__Project_Task__c> projectsTasksMap = getMapOfProjectsTasks();
            Date maxDateWork = works[0].project_cloud__Date__c;
            Date minDateWork = works[0].project_cloud__Date__c;
            List<Id> projectsIdsFromWorks = new List<Id>();
            for (project_cloud__Work__c work : works) {
                if (work.project_cloud__Project__c != null) {
                    projectsIdsFromWorks.add(work.project_cloud__Project__c); 
                } else {
                    projectsIdsFromWorks.add(projectsTasksMap.get(work.project_cloud__Project_Task__c).project_cloud__Project_Phase__r.project_cloud__Project__c);
                }
                
                if (work.project_cloud__Date__c > maxDateWork) {
                    maxDateWork =  work.project_cloud__Date__c;   
                }
                
                if (work.project_cloud__Date__c < minDateWork) {
                    minDateWork =  work.project_cloud__Date__c;   
                }
            }
            
            Date firstDayOfMinDateWorkMonth = minDateWork.toStartOfMonth();
            Integer numberOfDays = Date.daysInMonth(maxDateWork.year(), maxDateWork.month());
            Date lastDayOfMaxDateWorkMonth = Date.newInstance(maxDateWork.year(), maxDateWork.month(), numberOfDays);
            Id currentUserId = getCurrentUserId();
        
            List<Expense_report__c> expenseReports = [
                SELECT
                    Id, Expense_Report_Date__c, Project__c
                FROM
                    Expense_report__c
                WHERE
                    OwnerId = :currentUserId
                    AND Expense_Report_Date__c >= :firstDayOfMinDateWorkMonth 
                    AND Expense_Report_Date__c <= :lastDayOfMaxDateWorkMonth
                    AND Project__c IN :projectsIdsFromWorks
            ];
        
            return expenseReports;
        }
            
    private static List<Expense_Report__c> searchOrCreateExpenseReports(List<project_cloud__Work__c> works) {
        Map<Id, project_cloud__Project_Task__c> projectsTasksMap = getMapOfProjectsTasks();
        List<Expense_report__c> expenseReports = getExpenseReports(works);
        Set<Expense_report__c> newExpenseReports = new Set<Expense_report__c>();
        boolean isReport = false;

        if (!expenseReports.isEmpty()){
            isReport = true;
        }

        boolean createNewExpenseReport = false;
        for (project_cloud__Work__c work : works) {
            Id projectIdFromWork = work.project_cloud__Project__c;
            if (projectIdFromWork == null) {
                projectIdFromWork = projectsTasksMap.get(work.project_cloud__Project_Task__c).project_cloud__Project_Phase__r.project_cloud__Project__c;   
            }

            if (isReport) {
                for (Expense_report__c report : expenseReports) {
                    createNewExpenseReport = false;
                    if (work.project_cloud__Date__c.year() == report.Expense_Report_Date__c.year() && 
                        work.project_cloud__Date__c.month() == report.Expense_Report_Date__c.month() && 
                        projectIdFromWork == report.Project__c) {
                        work.Expense_Report__c = report.Id;   
                        isReport = true;
                        createNewExpenseReport = true;
                        break;
                    } else isReport = false;  
                }
            } 
            if (!createNewExpenseReport) {
                Id currentUserId = getCurrentUserId();
                Id projectId;
                Integer numberOfDays = Date.daysInMonth(work.project_cloud__Date__c.year(), work.project_cloud__Date__c.month());
                Date lastDayOfMonth = Date.newInstance(work.project_cloud__Date__c.year(), work.project_cloud__Date__c.month(), numberOfDays);
                if (work.project_cloud__Project__c != null) {
                    projectId = work.project_cloud__Project__c;
                }
                else {
                    projectId = projectsTasksMap.get(work.project_cloud__Project_Task__c).project_cloud__Project_Phase__r.project_cloud__Project__c;        
                }

                newExpenseReports.add(new Expense_report__c (
                    OwnerId = currentUserId, Expense_Report_Date__c = lastDayOfMonth, Project__c = projectId,
                    //TODO :  Mileage_YTD__c
                    Project_Manager__c = work.project_cloud__Project_Owner__c, Mileage_YTD__c = 0)); 
                isReport = true;
            }
        }

        List<Expense_report__c> expenseReportsForInsert = new List<Expense_report__c>(newExpenseReports);

        insert expenseReportsForInsert;

        return expenseReportsForInsert;

    }

    private static List<Cost_Rate__c> searchCostRates(List<project_cloud__Work__c> works) {
        Map<Id, project_cloud__Project_Task__c> projectsTasksMap = getMapOfProjectsTasks();
        List<project_cloud__Work__c> allWorks = new List<project_cloud__Work__c>();            
        List<Expense_report__c> newExpenseReports = searchOrCreateExpenseReports(works);
        Map<Id, Id> bookingIdAndExpenseReportToRelate = new Map<Id, Id>();

        for (project_cloud__Work__c work : works) {
            if (work.Expense_Report__c == null) {
                allWorks.add(work);     
            }
            if (work.BookingId__c != null) {
                bookingIdAndExpenseReportToRelate.put(work.BookingId__c, work.Expense_Report__c);
            }
        }

        for (Integer i = 0; i < allWorks.size(); ++i) {
            allWorks[i].Expense_Report__c = newExpenseReports[i].Id;
            if (allWorks[i].BookingId__c != null) {
                bookingIdAndExpenseReportToRelate.put(allWorks[i].BookingId__c, newExpenseReports[i].Id);
            }
        }

        Set<Id> workTypesIds = new Set<Id>();
        Set<Id> projectsIds = new Set<Id>();
        Set<Id> projectsTasksIds = new Set<Id>();
        boolean notFound = false;
        Id currentUserId = getCurrentUserId();

        for (project_cloud__Work__c work : works) {
            Id projectId;
            if (projectsTasksMap.get(work.project_cloud__Project_Task__c) != null) {
                workTypesIds.add(projectsTasksMap.get(work.project_cloud__Project_Task__c).project_cloud__Work_Type__c);
                projectId = projectsTasksMap.get(work.project_cloud__Project_Task__c).project_cloud__Project_Phase__r.project_cloud__Project__c;
                projectsIds.add(projectId);
            }
        }

        List<Cost_Rate__c> costRates = [
            SELECT
                Work_Type__c, Project__c, Resource__c, Cost_Rate__c
            FROM
                Cost_Rate__c
            WHERE
                Work_Type__c IN :workTypesIds
                AND Project__c IN :projectsIds
                AND Resource__c = :currentUserId
        ];

        List<Booking__c> bookingsToUpdate = [
            SELECT
                Id
            FROM
                Booking__c
            WHERE
                Id IN: bookingIdAndExpenseReportToRelate.keySet()
        ];

        for (Booking__c booking : bookingsToUpdate) {
            booking.Expense_report__c = bookingIdAndExpenseReportToRelate.get(booking.Id);
        }

        update bookingsToUpdate;

        return costRates;
    }

    private static Decimal getUserCostRate() {  
        Id currentUserId = getCurrentUserId();
        List<User> currentUser = [
            SELECT
                Id, Cost_Rate__c
            FROM
                User
            WHERE
                Id = :currentUserId
        ];

        return currentUser[0].Cost_Rate__c;
    }

    private static void populateWorkWithCostRate(List<project_cloud__Work__c> works) {
        Map<Id, project_cloud__Project_Task__c> projectsTasksMap = getMapOfProjectsTasks();
        List<Cost_Rate__c> costRates = searchCostRates(works);
        Decimal userCostRate = getUserCostRate();
        Id currentUserId = getCurrentUserId();
        Boolean areCostRatesEmpty = true;
        if (costRates.isEmpty()) {
            areCostRatesEmpty = false;
        }
        List<project_cloud__Work__c> worksWithNoCostRates = new List<project_cloud__Work__c>();
        for (project_cloud__Work__c work : works) {
            Integer counter = 0;
            for (Cost_Rate__c rate : costRates) { 
                counter++;
                Id projectId;
                if (projectsTasksMap.get(work.project_cloud__Project_Task__c) != null) {
                    projectId = projectsTasksMap.get(work.project_cloud__Project_Task__c).project_cloud__Project_Phase__r.project_cloud__Project__c;
                }
                if (projectId == rate.Project__c && projectsTasksMap.get(work.project_cloud__Project_Task__c).project_cloud__Work_Type__c == rate.Work_Type__c && currentUserid == rate.Resource__c) {
                    work.Rate__c = rate.Cost_Rate__c;  
                    break;
                } else {
                    if (counter == costRates.size()) {
                        worksWithNoCostRates.add(work);  
                    }
                }
            }
        }
        if (!areCostRatesEmpty) {
            for (project_cloud__Work__c work : works) {
                work.Rate__c = userCostRate;    
            }
        } else {
            for (project_cloud__Work__c work : worksWithNoCostRates) {
                work.Rate__c = userCostRate;
            }
        }
    }
}