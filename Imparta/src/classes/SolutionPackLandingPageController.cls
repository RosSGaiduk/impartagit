public with sharing class SolutionPackLandingPageController{

    public List<APXTConga4__Conga_Solution__c> createdSolutions = [SELECT Id, Name, APXTConga4__Master_Object_Type__c, APXTConga4__Sample_Record_Id__c, APXTConga4__Weblink_Id__c, APXTConga4__Button_Link_API_Name__c FROM APXTConga4__Conga_Solution__c WHERE Id != null AND CreatedDate = TODAY AND LastModifiedDate = TODAY AND APXTConga4__Weblink_Id__c = null ORDER BY APXTConga4__Master_Object_Type__c ASC, Id ASC];
    private User currentUser = [SELECT Id, Name, Email, Phone FROM User WHERE Id = :UserInfo.getUserId()];
    private Organization orgData = [SELECT Id, Name, City, Country, Fax, Division, IsSandbox, OrganizationType, Phone, PostalCode, PrimaryContact, State, Street FROM Organization WHERE Id =: UserInfo.getOrganizationId() LIMIT 1];
    private List<s2cor__Sage_COR_Action__c> congaSLCustomActions = [SELECT Id FROM s2cor__Sage_COR_Action__c WHERE (Name = 'Conga Sales Invoice' OR Name = 'Conga Print Check' OR Name = 'Conga Payment Receipt' OR Name = 'Conga Sales Quote' OR Name = 'Conga Payment Remittance')];
    public Boolean canSeeButton = false;
    public Boolean canUnpackSolutions = true;
    public Boolean canSeeRecordLinks = false;
    public Boolean remoteSiteAdded {get;set;}
    public Boolean conductorRecordsCreated {get;set;}    
    public Boolean nonSolutionTemplatesUnpacked = false;
    private Organization orgType = [SELECT OrganizationType FROM Organization LIMIT 1];
    public List<eFolders> folders {get;set;}    
    
    public SolutionPackLandingPageController()
    {
        Map<String,String> qspMap = ApexPages.currentPage().getParameters();
            if(qspMap.get('nonSolutionTemplatesUnpacked') == 'true')
            {
                nonSolutionTemplatesUnpacked = true;
            }
        
        if(createdSolutions.size() >= 1)
        {
            canSeeButton = true;
        }
        List<Folder> massTemplateFolderCheck = [SELECT Id FROM Folder WHERE DeveloperName = 'CongaSP_Templates_Folder' LIMIT 1];
        if(massTemplateFolderCheck.size() == 0)
        {
            nonSolutionTemplatesUnpacked = true;
        }
        else
        {
            List<Document> docsInMassTemplateFolder = [SELECT Id FROM Document WHERE FolderId = :massTemplateFolderCheck[0].Id];
            if (docsInMassTemplateFolder.size() == 0)
            {
                nonSolutionTemplatesUnpacked = true;
            }
        }
    }
    
    public List<APXTConga4__Conga_Solution__c> getCreatedSolutionRecords()
    {
        return createdSolutions;
    }
    
    public void createButtons()
    {
        canSeeButton = false;
        APXTConga4.ComposerSolutionExtension cse = new APXTConga4.ComposerSolutionExtension();
        
        for (APXTConga4__Conga_Solution__c cs : createdSolutions)
        {
           if (cs.APXTConga4__Master_Object_Type__c != 'APXTConga4__Composer_QuickMerge__c' && cs.APXTConga4__Master_Object_Type__c != 'Home Page')
           {
               try
               {
                   cse.createFullButtonString(cs);
               }
               catch (Exception ex)
               {
                   ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Something went wrong during the creation of the Composer Buttons.')); 
                   ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Navigate to the new Conga Solution records and create buttons individually for more information.'));
                   System.debug('************** Exception message in SolutionPackLandingPageController.createButtons: ' + ex.getMessage());                   
               }               
           }
        }
        canSeeRecordLinks = true;        
        database.update(cse.solutionRecordsToUpdate);
    }
        
    public Boolean getCanSeeButton()
    {
        return canSeeButton;
    }  
        
    public Boolean getCanUnpackSolutions()
    {
        if(createdSolutions.size() >= 1)
        {
            canUnpackSolutions = false;
        }   
        return canUnpackSolutions;
    }          
    
    public PageReference unpackSolutions()
    {        
        List<String> folderIds = new List<String>();
                
        for(eFolders ef : getDocumentFolders())
        {
            system.Debug('****************************** ef in SolutionPackLandingPageController.unpackSolutions: ' + ef);
            system.Debug('****************************** ef.isSelected in SolutionPackLandingPageController.unpackSolutions: ' + ef.isSelected);
            if(ef.isSelected == true)
            {
                folderIds.add(ef.folderId);
            }
        }
        system.Debug('********************** folderIds in SolutionPackLandingPageController.unpackSolutions: ' + folderIds);
        
        List<Folder> foldersToProcess = [SELECT Id, Name, DeveloperName FROM Folder WHERE Id IN :folderIds];        
        
        SolutionPackExtractController sp = new SolutionPackExtractController();
        sp.unpackSolutions(foldersToProcess);
        canUnpackSolutions = false;      
        
        String pageReturn = '';
        
        if (orgType.OrganizationType == 'Professional Edition')
        {
            canSeeRecordLinks = true;   
        }                                         
        
        if(nonSolutionTemplatesUnpacked == true)
        {
            pageReturn = '/apex/SolutionPackExtractor?nonSolutionTemplatesUnpacked=true';
        }
        else
        {
            nonSolutionTemplatesUnpacked = false;
            pageReturn = '/apex/SolutionPackExtractor';
        }

// Add SageLive Custom Actions that actually work as intended only if it isn't already in the Org (potential future and test class failure)
        if(congaSLCustomActions.size() == 0)
        {
            List<s2cor__Sage_COR_Action__c> sageActionsToInsert = new List<s2cor__Sage_COR_Action__c>();
            
            // English
            {
            s2cor__Sage_COR_Action__c CongaSICustomAction = new s2cor__Sage_COR_Action__c(Name = 'Conga Sales Invoice', s2cor__Context__c = 'Record', s2cor__Title__c = 'Conga Sales Invoice', s2cor__Type__c = 'Visualforce Page', s2cor__Target__c = 'CongaInvoiceLauncher', s2cor__UID__c = 'Conga_SendInvoice_en', s2cor__Language__c = 'en', s2cor__SObject_Type__c  ='s2cor__Sage_INV_Trade_Document__c', s2cor__Icon_Name__c = 'standard:client', s2cor__Display_in_Tables__c = TRUE, s2cor__Order__c = 3, s2cor__SObject_UIDs__c = 'SalesInvoice_TD');
                sageActionsToInsert.add(CongaSICustomAction);

            s2cor__Sage_COR_Action__c CongaSQCustomAction = new s2cor__Sage_COR_Action__c(Name = 'Conga Sales Quote', s2cor__Context__c = 'Record', s2cor__Title__c = 'Conga Sales Quote', s2cor__Type__c = 'Visualforce Page', s2cor__Target__c = 'CongaQuoteLauncher', s2cor__UID__c = 'Conga_SendQuote_en', s2cor__Language__c = 'en', s2cor__SObject_Type__c  = 's2cor__Sage_INV_Trade_Document__c', s2cor__Icon_Name__c = 'standard:client', s2cor__Display_in_Tables__c = TRUE, s2cor__Order__c = 3, s2cor__SObject_UIDs__c = 'SalesQuote_TD,Sales_Proposal');
                sageActionsToInsert.add(CongaSQCustomAction);                
            
            s2cor__Sage_COR_Action__c CongaPCCustomAction = new s2cor__Sage_COR_Action__c(Name = 'Conga Print Check', s2cor__Context__c = 'Record', s2cor__Title__c = 'Conga Print Check', s2cor__Type__c = 'Visualforce Page', s2cor__Target__c = 'CongaPrintCheckLauncher', s2cor__UID__c = 'Conga_PrintCheck_en', s2cor__Language__c = 'en', s2cor__SObject_Type__c  ='s2cor__Sage_ACC_Journal__c', s2cor__Icon_Name__c = 'standard:client', s2cor__Display_in_Tables__c = TRUE, s2cor__Order__c = 4, s2cor__SObject_UIDs__c = 'PaymentsbyCheque_JT,PurchasePayments_JT,PaymentsToSuppliers_JT');
                sageActionsToInsert.add(CongaPCCustomAction);                
            
            s2cor__Sage_COR_Action__c CongaPRMCustomAction = new s2cor__Sage_COR_Action__c(Name = 'Conga Multi Payment Receipt', s2cor__Context__c = 'Record', s2cor__Title__c = 'Conga Multi Payment Receipt', s2cor__Type__c = 'Visualforce Page', s2cor__Target__c = 'CongaMultiPaymentReceiptLauncher', s2cor__UID__c = 'Conga_PaymentReceipt_en', s2cor__Language__c = 'en', s2cor__SObject_Type__c  ='s2cor__Sage_ACC_Journal__c', s2cor__Icon_Name__c = 'standard:client', s2cor__Display_in_Tables__c = TRUE, s2cor__Order__c = 3, s2cor__SObject_UIDs__c = 'CustomerReceipts_JT,SalesReceipts_JT');
                sageActionsToInsert.add(CongaPRMCustomAction);
                
            s2cor__Sage_COR_Action__c CongaPRCustomAction = new s2cor__Sage_COR_Action__c(Name = 'Conga Payment Remittance', s2cor__Context__c = 'Record', s2cor__Title__c = 'Conga Payment Remittance', s2cor__Type__c = 'Visualforce Page', s2cor__Target__c = 'CongaPaymentRemittanceLauncher', s2cor__UID__c = 'Conga_PaymentRemittance_en', s2cor__Language__c = 'en', s2cor__SObject_Type__c  ='s2cor__Sage_ACC_Journal__c', s2cor__Icon_Name__c = 'standard:client', s2cor__Display_in_Tables__c = TRUE, s2cor__Order__c = 3, s2cor__SObject_UIDs__c = 'PurchasePayments_JT,PaymentsToSuppliers_JT');
                sageActionsToInsert.add(CongaPRCustomAction);                
            }
            
            // French
            {
            s2cor__Sage_COR_Action__c CongaSICustomAction = new s2cor__Sage_COR_Action__c(Name = 'Conga Sales Invoice', s2cor__Context__c = 'Record', s2cor__Title__c = 'Conga Sales Invoice', s2cor__Type__c = 'Visualforce Page', s2cor__Target__c = 'CongaInvoiceLauncher', s2cor__UID__c = 'Conga_SendInvoice_fr', s2cor__Language__c = 'fr', s2cor__SObject_Type__c  ='s2cor__Sage_INV_Trade_Document__c', s2cor__Icon_Name__c = 'standard:client', s2cor__Display_in_Tables__c = TRUE, s2cor__Order__c = 3, s2cor__SObject_UIDs__c = 'SalesInvoice_TD');
                sageActionsToInsert.add(CongaSICustomAction);

            s2cor__Sage_COR_Action__c CongaSQCustomAction = new s2cor__Sage_COR_Action__c(Name = 'Conga Sales Quote', s2cor__Context__c = 'Record', s2cor__Title__c = 'Conga Sales Quote', s2cor__Type__c = 'Visualforce Page', s2cor__Target__c = 'CongaQuoteLauncher', s2cor__UID__c = 'Conga_SendQuote_fr', s2cor__Language__c = 'fr', s2cor__SObject_Type__c  = 's2cor__Sage_INV_Trade_Document__c', s2cor__Icon_Name__c = 'standard:client', s2cor__Display_in_Tables__c = TRUE, s2cor__Order__c = 3, s2cor__SObject_UIDs__c = 'SalesQuote_TD,Sales_Proposal');
                sageActionsToInsert.add(CongaSQCustomAction);                
            
            s2cor__Sage_COR_Action__c CongaPCCustomAction = new s2cor__Sage_COR_Action__c(Name = 'Conga Print Check', s2cor__Context__c = 'Record', s2cor__Title__c = 'Conga Print Check', s2cor__Type__c = 'Visualforce Page', s2cor__Target__c = 'CongaPrintCheckLauncher', s2cor__UID__c = 'Conga_PrintCheck_fr', s2cor__Language__c = 'fr', s2cor__SObject_Type__c  ='s2cor__Sage_ACC_Journal__c', s2cor__Icon_Name__c = 'standard:client', s2cor__Display_in_Tables__c = TRUE, s2cor__Order__c = 4, s2cor__SObject_UIDs__c = 'PaymentsbyCheque_JT,PurchasePayments_JT,PaymentsToSuppliers_JT');
                sageActionsToInsert.add(CongaPCCustomAction);                
            
            s2cor__Sage_COR_Action__c CongaPRMCustomAction = new s2cor__Sage_COR_Action__c(Name = 'Conga Multi Payment Receipt', s2cor__Context__c = 'Record', s2cor__Title__c = 'Conga Multi Payment Receipt', s2cor__Type__c = 'Visualforce Page', s2cor__Target__c = 'CongaMultiPaymentReceiptLauncher', s2cor__UID__c = 'Conga_PaymentReceipt_fr', s2cor__Language__c = 'fr', s2cor__SObject_Type__c  ='s2cor__Sage_ACC_Journal__c', s2cor__Icon_Name__c = 'standard:client', s2cor__Display_in_Tables__c = TRUE, s2cor__Order__c = 3, s2cor__SObject_UIDs__c = 'CustomerReceipts_JT,SalesReceipts_JT');
                sageActionsToInsert.add(CongaPRMCustomAction);
                
            s2cor__Sage_COR_Action__c CongaPRCustomAction = new s2cor__Sage_COR_Action__c(Name = 'Conga Payment Remittance', s2cor__Context__c = 'Record', s2cor__Title__c = 'Conga Payment Remittance', s2cor__Type__c = 'Visualforce Page', s2cor__Target__c = 'CongaPaymentRemittanceLauncher', s2cor__UID__c = 'Conga_PaymentRemittance_fr', s2cor__Language__c = 'fr', s2cor__SObject_Type__c  ='s2cor__Sage_ACC_Journal__c', s2cor__Icon_Name__c = 'standard:client', s2cor__Display_in_Tables__c = TRUE, s2cor__Order__c = 3, s2cor__SObject_UIDs__c = 'PurchasePayments_JT,PaymentsToSuppliers_JT');
                sageActionsToInsert.add(CongaPRCustomAction);                
            }
            
            // German
            {
            s2cor__Sage_COR_Action__c CongaSICustomAction = new s2cor__Sage_COR_Action__c(Name = 'Conga Sales Invoice', s2cor__Context__c = 'Record', s2cor__Title__c = 'Conga Sales Invoice', s2cor__Type__c = 'Visualforce Page', s2cor__Target__c = 'CongaInvoiceLauncher', s2cor__UID__c = 'Conga_SendInvoice_de', s2cor__Language__c = 'de', s2cor__SObject_Type__c  ='s2cor__Sage_INV_Trade_Document__c', s2cor__Icon_Name__c = 'standard:client', s2cor__Display_in_Tables__c = TRUE, s2cor__Order__c = 3, s2cor__SObject_UIDs__c = 'SalesInvoice_TD');
                sageActionsToInsert.add(CongaSICustomAction);

            s2cor__Sage_COR_Action__c CongaSQCustomAction = new s2cor__Sage_COR_Action__c(Name = 'Conga Sales Quote', s2cor__Context__c = 'Record', s2cor__Title__c = 'Conga Sales Quote', s2cor__Type__c = 'Visualforce Page', s2cor__Target__c = 'CongaQuoteLauncher', s2cor__UID__c = 'Conga_SendQuote_de', s2cor__Language__c = 'de', s2cor__SObject_Type__c  = 's2cor__Sage_INV_Trade_Document__c', s2cor__Icon_Name__c = 'standard:client', s2cor__Display_in_Tables__c = TRUE, s2cor__Order__c = 3, s2cor__SObject_UIDs__c = 'SalesQuote_TD,Sales_Proposal');
                sageActionsToInsert.add(CongaSQCustomAction);                
            
            s2cor__Sage_COR_Action__c CongaPCCustomAction = new s2cor__Sage_COR_Action__c(Name = 'Conga Print Check', s2cor__Context__c = 'Record', s2cor__Title__c = 'Conga Print Check', s2cor__Type__c = 'Visualforce Page', s2cor__Target__c = 'CongaPrintCheckLauncher', s2cor__UID__c = 'Conga_PrintCheck_de', s2cor__Language__c = 'de', s2cor__SObject_Type__c  ='s2cor__Sage_ACC_Journal__c', s2cor__Icon_Name__c = 'standard:client', s2cor__Display_in_Tables__c = TRUE, s2cor__Order__c = 4, s2cor__SObject_UIDs__c = 'PaymentsbyCheque_JT,PurchasePayments_JT,PaymentsToSuppliers_JT');
                sageActionsToInsert.add(CongaPCCustomAction);                
            
            s2cor__Sage_COR_Action__c CongaPRMCustomAction = new s2cor__Sage_COR_Action__c(Name = 'Conga Multi Payment Receipt', s2cor__Context__c = 'Record', s2cor__Title__c = 'Conga Multi Payment Receipt', s2cor__Type__c = 'Visualforce Page', s2cor__Target__c = 'CongaMultiPaymentReceiptLauncher', s2cor__UID__c = 'Conga_PaymentReceipt_de', s2cor__Language__c = 'de', s2cor__SObject_Type__c  ='s2cor__Sage_ACC_Journal__c', s2cor__Icon_Name__c = 'standard:client', s2cor__Display_in_Tables__c = TRUE, s2cor__Order__c = 3, s2cor__SObject_UIDs__c = 'CustomerReceipts_JT,SalesReceipts_JT');
                sageActionsToInsert.add(CongaPRMCustomAction);
                
            s2cor__Sage_COR_Action__c CongaPRCustomAction = new s2cor__Sage_COR_Action__c(Name = 'Conga Payment Remittance', s2cor__Context__c = 'Record', s2cor__Title__c = 'Conga Payment Remittance', s2cor__Type__c = 'Visualforce Page', s2cor__Target__c = 'CongaPaymentRemittanceLauncher', s2cor__UID__c = 'Conga_PaymentRemittance_de', s2cor__Language__c = 'de', s2cor__SObject_Type__c  ='s2cor__Sage_ACC_Journal__c', s2cor__Icon_Name__c = 'standard:client', s2cor__Display_in_Tables__c = TRUE, s2cor__Order__c = 3, s2cor__SObject_UIDs__c = 'PurchasePayments_JT,PaymentsToSuppliers_JT');
                sageActionsToInsert.add(CongaPRCustomAction);                
            }
            
            // Spanish
            {
            s2cor__Sage_COR_Action__c CongaSICustomAction = new s2cor__Sage_COR_Action__c(Name = 'Conga Sales Invoice', s2cor__Context__c = 'Record', s2cor__Title__c = 'Conga Sales Invoice', s2cor__Type__c = 'Visualforce Page', s2cor__Target__c = 'CongaInvoiceLauncher', s2cor__UID__c = 'Conga_SendInvoice_es', s2cor__Language__c = 'es', s2cor__SObject_Type__c  ='s2cor__Sage_INV_Trade_Document__c', s2cor__Icon_Name__c = 'standard:client', s2cor__Display_in_Tables__c = TRUE, s2cor__Order__c = 3, s2cor__SObject_UIDs__c = 'SalesInvoice_TD');
                sageActionsToInsert.add(CongaSICustomAction);

            s2cor__Sage_COR_Action__c CongaSQCustomAction = new s2cor__Sage_COR_Action__c(Name = 'Conga Sales Quote', s2cor__Context__c = 'Record', s2cor__Title__c = 'Conga Sales Quote', s2cor__Type__c = 'Visualforce Page', s2cor__Target__c = 'CongaQuoteLauncher', s2cor__UID__c = 'Conga_SendQuote_es', s2cor__Language__c = 'es', s2cor__SObject_Type__c  = 's2cor__Sage_INV_Trade_Document__c', s2cor__Icon_Name__c = 'standard:client', s2cor__Display_in_Tables__c = TRUE, s2cor__Order__c = 3, s2cor__SObject_UIDs__c = 'SalesQuote_TD,Sales_Proposal');
                sageActionsToInsert.add(CongaSQCustomAction);                
            
            s2cor__Sage_COR_Action__c CongaPCCustomAction = new s2cor__Sage_COR_Action__c(Name = 'Conga Print Check', s2cor__Context__c = 'Record', s2cor__Title__c = 'Conga Print Check', s2cor__Type__c = 'Visualforce Page', s2cor__Target__c = 'CongaPrintCheckLauncher', s2cor__UID__c = 'Conga_PrintCheck_es', s2cor__Language__c = 'es', s2cor__SObject_Type__c  ='s2cor__Sage_ACC_Journal__c', s2cor__Icon_Name__c = 'standard:client', s2cor__Display_in_Tables__c = TRUE, s2cor__Order__c = 4, s2cor__SObject_UIDs__c = 'PaymentsbyCheque_JT,PurchasePayments_JT,PaymentsToSuppliers_JT');
                sageActionsToInsert.add(CongaPCCustomAction);                
            
            s2cor__Sage_COR_Action__c CongaPRMCustomAction = new s2cor__Sage_COR_Action__c(Name = 'Conga Multi Payment Receipt', s2cor__Context__c = 'Record', s2cor__Title__c = 'Conga Multi Payment Receipt', s2cor__Type__c = 'Visualforce Page', s2cor__Target__c = 'CongaMultiPaymentReceiptLauncher', s2cor__UID__c = 'Conga_PaymentReceipt_es', s2cor__Language__c = 'es', s2cor__SObject_Type__c  ='s2cor__Sage_ACC_Journal__c', s2cor__Icon_Name__c = 'standard:client', s2cor__Display_in_Tables__c = TRUE, s2cor__Order__c = 3, s2cor__SObject_UIDs__c = 'CustomerReceipts_JT,SalesReceipts_JT');
                sageActionsToInsert.add(CongaPRMCustomAction);
                
            s2cor__Sage_COR_Action__c CongaPRCustomAction = new s2cor__Sage_COR_Action__c(Name = 'Conga Payment Remittance', s2cor__Context__c = 'Record', s2cor__Title__c = 'Conga Payment Remittance', s2cor__Type__c = 'Visualforce Page', s2cor__Target__c = 'CongaPaymentRemittanceLauncher', s2cor__UID__c = 'Conga_PaymentRemittance_es', s2cor__Language__c = 'es', s2cor__SObject_Type__c  ='s2cor__Sage_ACC_Journal__c', s2cor__Icon_Name__c = 'standard:client', s2cor__Display_in_Tables__c = TRUE, s2cor__Order__c = 3, s2cor__SObject_UIDs__c = 'PurchasePayments_JT,PaymentsToSuppliers_JT');
                sageActionsToInsert.add(CongaPRCustomAction);                
            }
                            
            database.insert(sageActionsToInsert);
        }
        
        // Send an email to the Conga OEM team when someone extracts the solutions in the Conga for SageLive package
        Messaging.SingleEmailMessage generatePostInstallEmail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {'SageOrders@getconga.com'};          
            generatePostInstallEmail.setSubject('NOT AND ORDER - Conga for SageLive Extract Conga Solutions has been pressed'); 
            generatePostInstallEmail.setToAddresses(toAddresses);
            generatePostInstallEmail.setPlainTextBody('ORG INFORMATION' + '\n' + 'User Name: ' + UserInfo.getName() + '\n' + 'User Email | Phone: ' + currentUser.Email + ' | ' + currentUser.Phone + '\n' + 'Org Name: ' + orgData.Name + '\n' + 'Org Id: ' + orgData.Id + '\n' + 'Org is a Sandbox: ' + orgData.IsSandbox);                                                      
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { generatePostInstallEmail });

        // Create the Conductor records for the 3 currently included batch processes
        createConductorRecords();

        PageReference p = new PageReference(pageReturn);
        p.setRedirect(true); 
        return p;                             
    }
    
    public Integer getCreatedSolutionsSize(){
        return createdSolutions.size();
    }
    
    public Boolean getCanSeeRecordLinks()
    {
        return canSeeRecordLinks;
    }
    
    public String getOrgType(){
        return orgType.OrganizationType;
    }
    
    public static String getNameSpaceVFServerURL()
    {
        return Url.getSalesforceBaseUrl().getHost();
    }
        
    public static String getCongaOLogoURL()
    {
        List<Document> congaOLogoId = [SELECT Id FROM Document WHERE DeveloperName = 'Conga_O' and isDeleted = false]; 
        
        //The URL needed to use the Conga O logo that is part of the Composer package on the Composer Solution Manager pages
        //Avoid an error in the console about not using an https:// url to get that logo on to the pages       
        if(congaOLogoId.size() > 0)
        {
            return 'https://' + Url.getSalesforceBaseUrl().getHost().replace('visual','content') + '/servlet/servlet.ImageServer?id=' + congaOLogoId.get(0).Id + '&oid=' + UserInfo.getOrganizationId();
        }
        else
        {
            return '';
        }
    }
    
    public List<eFolders> getDocumentFolders()
    {
        List<String> SMRecordNameandObject = New List<String>();
        
        for(APXTConga4__Conga_Solution__c csr : [SELECT ID, Name, APXTConga4__Master_Object_Type__c FROM APXTConga4__Conga_Solution__c LIMIT 2000])
        {
            SMRecordNameandObject.Add(csr.Name + ' (' + csr.APXTConga4__Master_Object_Type__c + ')');
        }
        
        if (folders == null)
        {
            folders = new List<eFolders>();
            for (Folder f: [SELECT ID, DeveloperName, NamespacePrefix, Name FROM Folder WHERE DeveloperName LIKE 'CongaSP_%' AND Type = 'Document' AND Name != :SMRecordNameandObject AND DeveloperName Not IN ('CongaSP_Test_Document_Folder_Account', 'CongaSP_Example_Starter_Pack', 'CongaSP_Templates_Folder') AND NameSpacePrefix != 'APXTConga4'])
            {
                eFolders ef = new eFolders(f.Name, f.Id, f.DeveloperName);
                folders.add(ef);    
            }
        }
        return folders;
    }
    
    public PageReference setRemoteSiteAdded()
    {
        remoteSiteAdded = true;
        return null;
    }    
    
    public PageReference unpackNonSolutionTemplates()
    {
        List<Folder> massTemplateFolder = [SELECT Id FROM Folder WHERE DeveloperName = 'CongaSP_Templates_Folder' LIMIT 1];
        if(massTemplateFolder.size() == 0)
        {
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Required Document Folder with the DeveloperName of \"CongaSP_Templates_Folder\" cannot be found.'));
        }
        else
        {
            System.debug('****************** massTemplateFolder in unpackNonSolutionTemplates method: ' + massTemplateFolder);
            SolutionPackExtractController.unpackTemplateGroupTemplates(massTemplateFolder[0]);
            nonSolutionTemplatesUnpacked = true;  
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Successfully extracted Non-Linked Conga Templates.'));                      
        }
        return null;    
    }
    
    public void createConductorRecords()
    {
        ConductorRecordCreator crc = new ConductorRecordCreator();
        conductorRecordsCreated = true;
    }
    
    public Boolean getNonSolutionTemplatesUnpackedValue()
    {
        return nonSolutionTemplatesUnpacked;
    }       
    
    public class eFolders{
        public Boolean isSelected {get;set;}
        public String folderName {get;set;}
        public String folderDevName {get;set;}
        public Id folderId {get;set;}
        
        public eFolders(String eFolderName, Id eFolderId, String eFolderDevName){
            folderName = eFolderName;
            isSelected = true;
            folderId = eFolderId;
            folderDevName = eFolderDevName;
        }        
    }

}