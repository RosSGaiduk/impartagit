/*
		Name: OppProductTriggerHandler
		Discription: Trigger to handle Creation,updation and deletion of lineItem records which inturn impacts revenue records
		Created by: Lister Technologies
		Test Class: Test_OppProductTriggerHandler

	Change History
	   *********************************************************************************************************************************************
	   ModifiedBy        Date          Ticket No.      Requested By      Description                                                             Tag
	   *********************************************************************************************************************************************
*/
public class OppProductTriggerHandler {

	public static boolean oppLineItemTrigger = true;

	public static void creatingRevenueRecords(list<OpportunityLineItem> OppProductList){

		set<id> oppId = new set<Id>();
		List<Revenue_Item__c> revenueItemUpdateList = new List<Revenue_Item__c>();
		List<OpportunityLineItem> oppLineItemList = new List<OpportunityLineItem>();
		List<OpportunityLineItem> oppProductDateUpdatelist = new List<OpportunityLineItem>();
		Map<ID, Opportunity> oppCreatedDateMap = null;
		Map<String, date> oppProdDateMap = new Map<String, date>();
		try{
			for(OpportunityLineItem oppProduct: OppProductList){
				if(oppProduct.Start_Date__c==null)
					oppId.add(oppProduct.OpportunityId);
			}
			if(oppId.size()!=0)
				oppCreatedDateMap = new Map<ID, Opportunity>([select Id,Project_Start_Date__c,CloseDate from Opportunity where id in : oppId]);

			for(OpportunityLineItem oppProduct: OppProductList){
				if (oppProduct.Project__c != null) continue;

				Revenue_Item__c revenueItem = null;
				Date Start_Date = null;
				if(oppProduct.Start_Date__c!=null){
					Start_Date = oppProduct.Start_Date__c;
				}
				else if(oppProduct.Start_Date__c==null && oppCreatedDateMap.get(oppProduct.OpportunityId).Project_Start_Date__c!=null ){
					Start_Date = oppCreatedDateMap.get(oppProduct.OpportunityId).Project_Start_Date__c;
					oppProdDateMap.put(oppProduct.id, Start_Date);
				}
				else if(oppProduct.Start_Date__c==null && oppCreatedDateMap.get(oppProduct.OpportunityId).Project_Start_Date__c==null){
					Start_Date = oppCreatedDateMap.get(oppProduct.OpportunityId).CloseDate;
					oppProdDateMap.put(oppProduct.id, Start_Date);
				}
				if(Start_Date!=null){
					revenueItem = setRevenueItemDetails(oppProduct,Start_Date);
					if(revenueItem!=null)
						revenueItemUpdateList.add(revenueItem);
				}
			}

			if(oppCreatedDateMap.size()!=0){
				oppLineItemList =[select Id,Start_Date__c from OpportunityLineItem where id in :oppProdDateMap.keySet()];
				if(oppLineItemList.size()!=0){
					for(OpportunityLineItem oppLineItem:oppLineItemList){
						if(oppProdDateMap.containsKey(oppLineItem.id)){
							oppLineItem.Start_Date__c = oppProdDateMap.get(oppLineItem.id);
							oppProductDateUpdatelist.add(oppLineItem);}
					}
				}
			}

			// Inserting revenue list and updating product details Start_Date__c
			if(revenueItemUpdateList.size()>0)
				insert revenueItemUpdateList;
			if(oppProductDateUpdatelist.size()>0)
				update oppProductDateUpdatelist;

		}
		catch(DmlException e){
			system.debug('Unable to insert the Revenue Item Records'+ e.getMessage());
		}
		catch(Exception e){
			system.debug('Improper Data Updation'+ e.getMessage());
		}
	}

	public static void updatingRevenueRecords(map<id,OpportunityLineItem> OppProductold,Map<id,OpportunityLineItem> oppProductNew){

		set<id> oppId = new set<Id>();
		List<Opportunity> oppRevenueRec = new List<Opportunity>();
		Map<String, list<Revenue_Item__c>> revenueListMap = new Map<String, list<Revenue_Item__c>>();
		List<Revenue_Item__c> revenueUpdatelist = new List<Revenue_Item__c>();
		Map<Id,set<String>> updateFlagMap = new Map<Id,set<String>>();

		try{
			for(OpportunityLineItem oppOld :OppProductold.values()){
				if (oppOld.Project__c != null && oppProductNew.get(oppOld.id).Project__c != null) continue;

				if(oppOld.Start_Date__c!=oppProductNew.get(oppOld.id).Start_Date__c||oppOld.TotalPrice!=oppProductNew.get(oppOld.id).TotalPrice ||oppOld.Total_Cost__c!=oppProductNew.get(oppOld.id).Total_Cost__c ||oppOld.Quantity!=oppProductNew.get(oppOld.id).Quantity||oppOld.Line_Probability__c!=oppProductNew.get(oppOld.id).Line_Probability__c){
					oppId.add(oppOld.OpportunityId);
					updateFlagMap.put(oppOld.Id,checkUpdateLogic(oppOld,oppProductNew.get(oppOld.Id)));

				}
			}
			if(oppId.size()!=0)
				oppRevenueRec =[select Id,Project_Start_Date__c,CreatedDate,(select Id,Opportunity_Product_Id__c,Quantity__c,Amount__c,Cost__c,Date__c,Product__c,Revenue_Probability__c from
																			 Revenue_Items__r) from Opportunity where id in :oppId];
			system.debug('oppId.size():::'+oppId.size());
			if(oppRevenueRec.size()!=0){
				for(Opportunity opp :oppRevenueRec){
					if(opp.Revenue_Items__r!=null && opp.Revenue_Items__r.size()!=0){
						for(Revenue_Item__c revenue : opp.Revenue_Items__r){
							if(revenueListMap.containsKey(revenue.Opportunity_Product_Id__c))
								revenueListMap.get(revenue.Opportunity_Product_Id__c).add(revenue);
							else
								revenueListMap.put(revenue.Opportunity_Product_Id__c, new list<Revenue_Item__c>{revenue});
						}
					}
				}
				system.debug('revenueListMap:::'+revenueListMap.size());
				if(revenueListMap.size()!=0){
					for(Id oppProductId : oppProductNew.keySet()){
						if(oppProductNew.containsKey(oppProductId)){
						   set<String> updateFlag = updateFlagMap.get(oppProductId);
							Date start_date = null;
							Integer daysDiff = 0;
							if(updateFlag.contains('A')){
								 Date previous_date = null;
								if(oppProductNew.get(oppProductId).Start_Date__c!=null ){
									start_date = oppProductNew.get(oppProductId).Start_Date__c;
									previous_date = OppProductold.get(oppProductId).Start_Date__c;
									daysDiff = checkDateDifference(previous_date,start_date);
								}
							}
								List<Revenue_Item__c> revenueListUpdate = updateRevenueItemChange(revenueListMap.get(oppProductId),
																								  oppProductNew.get(oppProductId),
																								  OppProductold.get(oppProductId),
																								  daysDiff,updateFlag);
								if(revenueListUpdate!=null)
									revenueUpdatelist.addAll(revenueListUpdate);
						}
					}
				}
			}
			system.debug('revenueUpdatelist:::'+revenueUpdatelist.size());
			if(revenueUpdatelist.size()>0)
				update revenueUpdatelist;
		}
		catch(DmlException e){
			system.debug('Unable to Update the Revenue Item Records'+e.getMessage());
		}
		catch(Exception e){
		   system.debug('Improper Data Updation' + e.getMessage());
		}
	}

	public static void deletingRevenueRecords(list<OpportunityLineItem> OppProductList){

		try{
			set<id> oppId = new set<Id>();
			set<id> oppProdId = new set<Id>();
			List<Revenue_Item__c> revenueItemDeleteList = new List<Revenue_Item__c>();
			List<Opportunity> oppRevenueRec = new List<Opportunity>();
			Map<String, list<Revenue_Item__c>> revenueListMap = new Map<String, list<Revenue_Item__c>>();
            Set<String> oppLineItemsIds = new Set<String>();
			for(OpportunityLineItem oppProduct: OppProductList){
                oppLineItemsIds.add(oppProduct.Id);
				if (oppProduct.Project__c != null) continue;

				oppId.add(oppProduct.OpportunityId);
				oppProdId.add(oppProduct.id);
			}

			if(oppId.size()!=0)
				oppRevenueRec=[select Id,(select Id,Opportunity_Product_Id__c from Revenue_Items__r)
							   from Opportunity where id in :oppId];

			if(oppRevenueRec.size()!=0){
				for(Opportunity opp :oppRevenueRec){
					if(opp.Revenue_Items__r!=null && opp.Revenue_Items__r.size()!=0){
						for(Revenue_Item__c revenue : opp.Revenue_Items__r){
							if(revenueListMap.containsKey(revenue.Opportunity_Product_Id__c))
								revenueListMap.get(revenue.Opportunity_Product_Id__c).add(revenue);
							else
								revenueListMap.put(revenue.Opportunity_Product_Id__c, new list<Revenue_Item__c>{revenue});
						}
					}
				}
				for(String oppProductid:revenueListMap.keySet()){
					if(oppProdId.contains(oppProductid))
						revenueItemDeleteList.addAll(revenueListMap.get(oppProductid));
				}
			}
			// deleting revenue list
			if(revenueItemDeleteList.size()>0)
				delete revenueItemDeleteList;
            
            List<Revenue_Item__c> revenueItemsByOpportunityLineItemId = [
                SELECT
                    Id
                FROM
                    Revenue_Item__c
                WHERE
                    Opportunity_Product_Id__c IN: oppLineItemsIds
            ];
            
            for (Revenue_Item__c revItem : revenueItemsByOpportunityLineItemId) {
                revItem.Opportunity__c = null;
                revItem.Opportunity_Product_Id__c = null;
            }
            
            update revenueItemsByOpportunityLineItemId;
		}
		catch(DmlException e){
			system.debug('Unable to delete the Revenue Item Records' + e.getMessage());
		}
		catch(Exception e){
			system.debug('Improper Data Updation' + e.getMessage());
		}

	}

	public static Revenue_Item__c setRevenueItemDetails(OpportunityLineItem oppProduct,Date start_date){

		try{
			Revenue_Item__c revenueItem = new Revenue_Item__c();
			revenueItem.Opportunity__c = oppProduct.OpportunityId;
			revenueItem.Product__c = oppProduct.Product2Id;
			revenueItem.Opportunity_Product_Id__c = oppProduct.id;
			revenueItem.Quantity__c = oppProduct.Quantity;
			revenueItem.Amount__c = oppProduct.TotalPrice;
			revenueItem.Cost__c = oppProduct.Total_Cost__c;
			revenueItem.Date__c = start_date;
			revenueItem.Revenue_Probability__c = oppProduct.Line_Probability__c;

			return revenueItem;
		}
		catch(Exception e){
			system.debug('Improper Data Updation' + e.getMessage());
			return null;
		}

	}
	   public static list<Revenue_Item__c> updateRevenueItemChange(list<Revenue_Item__c> revenueList,
																   OpportunityLineItem oppProd,
																   OpportunityLineItem oppProdOld,
																   integer daysDiff,
																   set<String> updateFlag){

		List<Revenue_Item__c> RevenueItemsUpdateList = new List<Revenue_Item__c>();
		try{
			for(integer i=0;i<revenueList.size();i++){
				Revenue_Item__c revItem =  revenueList.get(i);
				decimal Quantity = revItem.Quantity__c;
				if(updateFlag.contains('A')){
					if(revItem.Date__c!=null){
						if(daysDiff!=0){date revenueDate = revItem.Date__c;
						revItem.Date__c=revenueDate.addDays(daysDiff);}}
				}

				if(updateFlag.contains('B')){
				if(revItem.Quantity__c!=null && oppProdOld.Quantity!=0 && oppProdOld.Quantity!=null){
					decimal portion = (oppProd.Quantity/oppProdOld.Quantity);
					Quantity = (revItem.Quantity__c*portion).setScale(2,System.RoundingMode.HALF_UP);
						if(Quantity!= revItem.Quantity__c)
							revItem.Quantity__c=Quantity;}}

					if(updateFlag.contains('C')){
					if(revItem.Cost__c!=null){
						decimal cost = (Quantity*oppProd.Cost__c).setScale(2,System.RoundingMode.HALF_UP);
						if(cost!=revItem.Cost__c)
							revItem.Cost__c = cost;}}
					if(updateFlag.contains('D')){
					if(revItem.Amount__c!=null){
						decimal amount = (Quantity*oppProd.UnitPrice).setScale(2,System.RoundingMode.HALF_UP);
						if(amount!=revItem.Amount__c)
							revItem.Amount__c = amount;}}
					if(updateFlag.contains('E')){
						if(oppProd.Line_Probability__c!=revItem.Revenue_Probability__c)
							revItem.Revenue_Probability__c = oppProd.Line_Probability__c;}

				RevenueItemsUpdateList.add(revItem);
			}
			return RevenueItemsUpdateList;
		}
		catch(Exception e){
			system.debug('Improper Data Updation' + e.getMessage());
			return null;
		}
	}

	public static Set<String> checkUpdateLogic(OpportunityLineItem oppOld, OpportunityLineItem oppNew) {
		Set<String> updateFlag = new Set<String>();
		try {
			Date oldDate = oppOld.Start_Date__c;
			Date newDate = oppNew.Start_Date__c;
			if (oldDate!=newDate) updateFlag.add('A');
			if (oppOld.Quantity!=oppNew.Quantity) updateFlag.add('B');
			if (oppOld.Cost__c!=oppNew.Cost__c) updateFlag.add('C');
			if (oppOld.TotalPrice!=oppNew.TotalPrice) updateFlag.add('D');
			if (oppOld.Line_Probability__c!=oppNew.Line_Probability__c) updateFlag.add('E');
			return updateFlag;
		} catch(Exception e) {
			System.debug('Improper Data Updation' + e.getMessage());
			return null;
		}
	}

	public static Integer checkDateDifference(Date oldDate, Date newDate) {
		Integer daysDiff = oldDate.daysBetween(newDate);
		return daysDiff;
	}

	public static void updateProjectTaskRecords(List<OpportunityLineItem> oldOpportunityLineItems, Map<Id, OpportunityLineItem> newOpportunityLineItemMap) {
		Map<Id, Set<Id>> projectIdProductsIdsMap = new Map<Id, Set<Id>>();
		Map<Id, List<OpportunityLineItem>> productIdOpportunityProductsMap = new Map<Id, List<OpportunityLineItem>>();
        List<Id> changedOpportunityLineItems = new List<Id>();
		for (OpportunityLineItem oldOli : oldOpportunityLineItems) {
			OpportunityLineItem newOli = newOpportunityLineItemMap.get(oldOli.Id);
			if (oldOli.UnitPrice != newOli.UnitPrice && newOli.Project__c != null) {
				if (projectIdProductsIdsMap.get(newOli.Project__c) == null) {
					projectIdProductsIdsMap.put(newOli.Project__c, new Set<Id>());
				}
				projectIdProductsIdsMap.get(newOli.Project__c).add(newOli.Product2Id);

				if (productIdOpportunityProductsMap.get(newOli.Product2Id) == null) {
					productIdOpportunityProductsMap.put(newOli.Product2Id, new List<OpportunityLineItem>());
				}
				productIdOpportunityProductsMap.get(newOli.Product2Id).add(newOli);
                changedOpportunityLineItems.add(newOli.Id);
			}
		}

		Set<Id> allProductsIds = new Set<Id>();
		for (Set<Id> productsIds : projectIdProductsIdsMap.values()) {
			allProductsIds.addAll(productsIds);
		}

		List<project_cloud__Project_Task__c> tasks = [
			SELECT
				Service__c, Delegate_Fee_Product__c, Licence_Fee_Product__c,
				project_cloud__Project_Phase__r.project_cloud__Project__c,
                (SELECT Id, Opportunity_Product_Id__c FROM Revenue_Items__r WHERE Opportunity_Product_Id__c IN: changedOpportunityLineItems)   
			FROM
				project_cloud__Project_Task__c
			WHERE
				project_cloud__Project_Phase__r.project_cloud__Project__c IN :projectIdProductsIdsMap.keySet()
				AND Billable__c = true
				AND (Service__c IN :allProductsIds OR Delegate_Fee_Product__c IN :allProductsIds OR Licence_Fee_Product__c IN :allProductsIds)
		];

		List<project_cloud__Project_Task__c> tasksToUpdate = new List<project_cloud__Project_Task__c>();

		for (project_cloud__Project_Task__c task : tasks) {
            
            if (task.Revenue_Items__r.size() == 0) {
                continue;
            }
            
			Id projectId = task.project_cloud__Project_Phase__r.project_cloud__Project__c;
			Set<Id> productsIds = projectIdProductsIdsMap.get(projectId);

			Boolean isTaskUpdated = false;

            Boolean exitFromOpportunityLineItemsLoop = false;
            
			if (productsIds.contains(task.Service__c)) {
				for (OpportunityLineItem oli : productIdOpportunityProductsMap.get(task.Service__c)) {
                    for (Revenue_Item__c revenueItem : task.Revenue_Items__r) {
                        if (oli.Project__c == projectId && revenueItem.Opportunity_Product_Id__c == oli.Id) {
                            task.Service_Price__c = oli.UnitPrice;
                            isTaskUpdated = true;
                            exitFromOpportunityLineItemsLoop = true;
                            break;
                        }
                    }
                    if (exitFromOpportunityLineItemsLoop) {
                        break;
                    }
                }
			}

            exitFromOpportunityLineItemsLoop = false;
            
			if (productsIds.contains(task.Delegate_Fee_Product__c)) {
				for (OpportunityLineItem oli : productIdOpportunityProductsMap.get(task.Delegate_Fee_Product__c)) {
                    for (Revenue_Item__c revenueItem : task.Revenue_Items__r) {
                        if (oli.Project__c == projectId && revenueItem.Opportunity_Product_Id__c == oli.Id) {
                            task.Delegate_Fee__c = oli.UnitPrice;
                            isTaskUpdated = true;
                            exitFromOpportunityLineItemsLoop = true;
                            break;
                        }
                    }
                    if (exitFromOpportunityLineItemsLoop) {
                        break;
                    }
				}
			}
            
            exitFromOpportunityLineItemsLoop = false;

			if (productsIds.contains(task.Licence_Fee_Product__c)) {
				for (OpportunityLineItem oli : productIdOpportunityProductsMap.get(task.Licence_Fee_Product__c)) {
                    for (Revenue_Item__c revenueItem : task.Revenue_Items__r) {
                        if (oli.Project__c == projectId && revenueItem.Opportunity_Product_Id__c == oli.Id) {
                            task.Licence_Fee__c = oli.UnitPrice;
                            isTaskUpdated = true;
                            exitFromOpportunityLineItemsLoop = true;
                            break;
                        }
                    }
                    if (exitFromOpportunityLineItemsLoop) {
                        break;
                    }
				}
			}

			if (isTaskUpdated) tasksToUpdate.add(task);
		}

		update tasksToUpdate;
	}
    
    public static void validateRecordsByActualActivityAfterUpdate(Map<Id, OpportunityLineItem> oldItems, 
        Map<Id, OpportunityLineItem> newItems) {
        
        Set<Id> oppIds = new Set<Id>();
        for (Id oppItemId : oldItems.keySet()) {
            //if (oldItems.get(oppItemId).Actual_Activity__c != newItems.get(oppItemId).Actual_Activity__c) {
                oppIds.add(newItems.get(oppItemId).OpportunityId);
            //}
        }
        validateItems(oppIds);
    }
    
    public static void validateRecordsByActualActivityAfterInsert(List<OpportunityLineItem> oppItems) {
        Set<Id> oppIds = new Set<Id>();
        for (OpportunityLineItem oppItem : oppItems) {
            oppIds.add(oppItem.OpportunityId);
        }
        validateItems(oppIds);
    }
    
    private static void validateItems(Set<Id> oppIds) {
        List<OpportunityLineItem> oppItems = [
            SELECT
                Id, OpportunityId, Opportunity.Name, Actual_Activity__c
            FROM
                OpportunityLineItem
            WHERE
                OpportunityId IN: oppIds
        ];
        
        Map<Id, Set<String>> actualActivitiesByOpps = new Map<Id, Set<String>>();
        
        for (OpportunityLineItem oppLineItem : oppItems) {
            if (!actualActivitiesByOpps.containsKey(oppLineItem.OpportunityId)) {
                actualActivitiesByOpps.put(oppLineItem.OpportunityId, new Set<String>());
            }
            if (actualActivitiesByOpps.get(oppLineItem.OpportunityId).contains(oppLineItem.Actual_Activity__c)) {
                throw new ActualActivityValidationException('Opportunity ' + oppLineItem.Opportunity.Name 
                    + ' already has opportunity item with actual activity \'' + oppLineItem.Actual_Activity__c + '\'');
            } else {
                actualActivitiesByOpps.get(oppLineItem.OpportunityId).add(oppLineItem.Actual_Activity__c);
            }
        }
    }
    
    public class ActualActivityValidationException extends Exception {}
    
}