public class MultiCurrencyService {

	public static Map<String, Double> actualDatedConversionRates {
		get {
			if (actualDatedConversionRates == null) {
				actualDatedConversionRates = getActualDatedConversionRates();
			}
			return actualDatedConversionRates;
		}
		set;
	}

	private static Map<String, Double> getActualDatedConversionRates() {
		List<DatedConversionRate> datedConversionRates = [
				SELECT
					Id, IsoCode, ConversionRate
				FROM
					DatedConversionRate
				WHERE
					StartDate <= TODAY
					AND NextStartDate > TODAY
		];

		Map<String, Double> result = new Map<String, Double>();

		for (DatedConversionRate dcr : datedConversionRates) {
			result.put(dcr.IsoCode, dcr.ConversionRate);
		}

		return result;
	}

	public static Decimal convertCurrency(Decimal baseAmount, String baseCurrency, String outputCurrency) {
		return baseAmount * (actualDatedConversionRates.get(outputCurrency) / actualDatedConversionRates.get(baseCurrency));
	}

}