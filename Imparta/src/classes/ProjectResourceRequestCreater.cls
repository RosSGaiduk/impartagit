public class ProjectResourceRequestCreater {
    
    
    @AuraEnabled
    public static project_cloud__Work_Type__c[] getWorkTypesByProjectId(Id projectId) {
        
        List<project_cloud__Project_Task__c> projectTasks = [
            SELECT
            	Id, project_cloud__Work_Type__c
            FROM
            	project_cloud__Project_Task__c
            WHERE
            	project_cloud__Project__c =: projectId
            	AND Resource_Request__c = null
        ];
        
        Set<Id> workTypesIds = new Set<Id>();
        for (project_cloud__Project_Task__c projectTask : projectTasks) {
            if (projectTask.project_cloud__Work_Type__c != null) {
            	workTypesIds.add(projectTask.project_cloud__Work_Type__c);
            }
        }
        
        System.debug('Work types ids: '+workTypesIds);
        
        return [
            SELECT 
                Id, Name, project_cloud__Type__c,
            	(SELECT Id FROM project_cloud__Estimates__r)
            FROM 
                project_cloud__Work_Type__c
            //WHERE
            //	Id in: workTypesIds
        ];
    }

}