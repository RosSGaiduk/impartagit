<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>true</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This object records individual remittance payment transactions.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Payment_Date__c</fullName>
        <description>Enter the date that this payment has been debited from the bank</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter the date that this payment has been debited from the bank</inlineHelpText>
        <label>Payment Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>RAN__c</fullName>
        <description>This is the parent Remittance Advice Number of this payment transaction</description>
        <externalId>false</externalId>
        <inlineHelpText>This is the parent Remittance Advice Number of this payment transaction</inlineHelpText>
        <label>RAN</label>
        <referenceTo>Remittances__c</referenceTo>
        <relationshipLabel>A/C Payable Transactions</relationshipLabel>
        <relationshipName>A_C_Payable_Transactions</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>true</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Transaction_Amount__c</fullName>
        <description>Enter the amount paid to the creditor account in this transaction</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter the amount paid to the creditor account in this transaction</inlineHelpText>
        <label>Transaction Amount</label>
        <precision>9</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <label>A/C Payable Transaction</label>
    <nameField>
        <displayFormat>AC-Pay_{0000}</displayFormat>
        <label>A/C Payable Transaction Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>A/C Payable Transactions</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
