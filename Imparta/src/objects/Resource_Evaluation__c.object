<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>Resource_Evaluation_Record_Page</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>Resource_Evaluation_Compact_Layout</compactLayoutAssignment>
    <compactLayouts>
        <fullName>Resource_Evaluation_Compact_Layout</fullName>
        <fields>Resource__c</fields>
        <fields>Status__c</fields>
        <label>Resource Evaluation Compact Layout</label>
    </compactLayouts>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Used by the Resource Request process</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Comments__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Enter any pertinent comments here</inlineHelpText>
        <label>Comments</label>
        <length>131072</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Html</type>
        <visibleLines>40</visibleLines>
    </fields>
    <fields>
        <fullName>Not_Selected_Reason__c</fullName>
        <description>If resource is not slected, why?</description>
        <externalId>false</externalId>
        <inlineHelpText>If resource not selected, why?</inlineHelpText>
        <label>Not Selected Reason</label>
        <picklist>
            <controllingField>Status__c</controllingField>
            <picklistValues>
                <fullName>Alertnative Chosen</fullName>
                <default>false</default>
                <controllingFieldValues>Not Selected</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Not Available</fullName>
                <default>false</default>
                <controllingFieldValues>Not Selected</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Not Interested</fullName>
                <default>false</default>
                <controllingFieldValues>Not Selected</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Not Suitable</fullName>
                <default>false</default>
                <controllingFieldValues>Not Selected</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Other</fullName>
                <default>false</default>
                <controllingFieldValues>Not Selected</controllingFieldValues>
            </picklistValues>
            <restrictedPicklist>true</restrictedPicklist>
            <sorted>true</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Resource_Request__c</fullName>
        <externalId>false</externalId>
        <label>Resource Request</label>
        <referenceTo>project_cloud__Resource_Request__c</referenceTo>
        <relationshipLabel>Resource Evaluations</relationshipLabel>
        <relationshipName>Resource_Evaluations</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Resource__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Used to capture the name of the resource being considered</description>
        <externalId>false</externalId>
        <inlineHelpText>Name of Resource being Evaluated/Considered</inlineHelpText>
        <label>Resource</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Resource_Evaluations</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <description>Used to capture the status of the Resource Evaluation</description>
        <externalId>false</externalId>
        <label>Status</label>
        <picklist>
            <picklistValues>
                <fullName>Option</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Interview Scheduled</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>On Hold</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Order Confirmation Sent</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Booked</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Not Selected</fullName>
                <default>false</default>
            </picklistValues>
            <restrictedPicklist>true</restrictedPicklist>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <label>Resource Evaluation</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>RE-{0000000}</displayFormat>
        <label>Resource Evaluation Number</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Resource Evaluations</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Status__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Resource__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Resource_Request__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>CREATEDBY_USER</customTabListAdditionalFields>
        <customTabListAdditionalFields>CREATED_DATE</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Status__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Resource__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Resource_Request__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>CREATEDBY_USER</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>CREATED_DATE</lookupDialogsAdditionalFields>
        <searchResultsAdditionalFields>Status__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Resource__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Resource_Request__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>CREATEDBY_USER</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>CREATED_DATE</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
